module Server.WebServer

open Giraffe
open Microsoft.AspNetCore.Hosting
open Microsoft.AspNetCore.Http
open Microsoft.AspNetCore.Builder
open Microsoft.Extensions.DependencyInjection
open Server.RequestHandler
open Infrastructure.SqlOperations
open Infrastructure.EventStore.Access

let private ensureAccessKey accessKey (ctx : HttpContext) =
  ctx.GetFormValue "accessKey"
  |> Option.exists (fun key -> key = accessKey)

let private sqlCommandHandler databaseConfiguration handler : Result<UserSettings.Intention option, string> =
  async {
    try
      use! connection = openConnection databaseConfiguration
      let! intention = handler connection
      return Ok intention

    with ex ->
      return Error ex.Message
  }
  |> Async.RunSynchronously

let private sqlQueryHandler databaseConfiguration handler : Result<string, string> =
  async {
    try
      use! connection = openConnection databaseConfiguration
      let! result = handler connection
      return Ok result

    with ex ->
      return Error ex.Message
  }
  |> Async.RunSynchronously

let private configureServices (services : IServiceCollection) =
  services.AddGiraffe() |> ignore

let private accessDenied = setStatusCode 401 >=> text "Access Denied"

let run (config : ConfigLoader.Configuration) (reloadCustomChatColors : unit -> unit) eventStore =
  let tryCommand handler =
    match handler with
    | Ok handler ->
        eventStore |> tryEffect handler

    | Error err ->
        Some err
    |> Utils.asResponse

  let sqlCommand handler =
    match handler with
    | Ok handler ->
        match sqlCommandHandler config.database handler with
        | Ok (Some UserSettings.ReloadCustomChatColors) ->
            reloadCustomChatColors()
            None

        | Ok None ->
            None

        | Error err ->
            Some err

    | Error err ->
        Some err
    |> Utils.asResponse

  let query handler =
    getEvents eventStore
    |> handler
    |> setBodyFromString
    |> Successful.ok

  let sqlQuery handlerResult =
    match handlerResult with
    | Ok handler ->
        match sqlQueryHandler config.database handler with
        | Ok result ->
          Successful.ok (setBodyFromString result)

        | Error error ->
            ServerErrors.internalError (setBodyFromString error)

    | Error error ->
        RequestErrors.badRequest (setBodyFromString error)

  let configureApp (app : IApplicationBuilder) =
    setHttpHeader "Content-Type" "application/json; charset=utf-8"
    >=> choose [
      GET >=> choose [
        route "/getVIPs" >=> warbler (fun _ -> query (VIPs.getAll System.DateTime.UtcNow))
        route "/getMembers" >=> warbler (fun _ -> query Members.getAll)
        route "/getRoleOf" >=> warbler (fun (_, ctx) -> query (Members.getRole System.DateTime.UtcNow ctx))

        route "/joinsoundOf" >=> warbler (snd>>UserSettings.getJoinsoundOf>>sqlQuery)
        route "/definedJoinsounds" >=> warbler (snd>>UserSettings.getDefinedJoinsounds>>sqlQuery)
        route "/customChatColorsOf" >=> warbler (snd>>UserSettings.getCustomChatColorsOf>>sqlQuery)

        route "/serverStatus" >=> warbler (GameServerInfo.serverStatus config.gameserver)
      ]
      POST
      >=> authorizeRequest (ensureAccessKey config.accessKey) accessDenied
      >=> choose [
          route "/addMember" >=> warbler (snd>>Members.add>>tryCommand)
          route "/setMemberRank" >=> warbler (snd>>Members.setRank>>tryCommand)
          route "/removeMember" >=> warbler (snd>>Members.remove>>tryCommand)

          route "/giveUserVIP" >=> warbler (snd>>VIPs.giveToUser>>tryCommand)
          route "/correctVIPDuration" >=> warbler (snd>>VIPs.correctDuration>>tryCommand)
          route "/extendVIPDuration" >=> warbler (snd>>VIPs.extendDuration>>tryCommand)
          route "/extendTimeForAllVIPs" >=> warbler (snd>>VIPs.extendTimeForAllVIPs>>tryCommand)
          route "/transferTime" >=> warbler (snd>>VIPs.transferTime>>tryCommand)
          route "/removeVIPDueToRuleBreaking" >=> warbler (snd>>VIPs.removeDueToRuleBreaking>>tryCommand)
          route "/pauseVIP" >=> warbler (snd>>VIPs.pause>>tryCommand)
          route "/resumeVIP" >=> warbler (snd>>VIPs.resume>>tryCommand)

          route "/setCustomChatColors" >=> warbler (snd>>UserSettings.setCustomChatColors>>sqlCommand)
          route "/setJoinsound" >=> warbler (snd>>UserSettings.setJoinsound>>sqlCommand)
          route "/useNoJoinsound" >=> warbler (snd>>UserSettings.useNoJoinsound>>sqlCommand)
        ]
    ]
    |> app.UseGiraffe

  WebHostBuilder()
    .UseKestrel()
    .Configure(System.Action<IApplicationBuilder> configureApp)
    .ConfigureServices(configureServices)
    .UseUrls(sprintf "http://%s:%i" config.ip config.port)
    .Build()
    .Run()