module Server.GameServer

open CoreRCON
open System.Net
open System.Threading
open System.Threading.Tasks

type Configuration =
  { ip : string
    port : int
    rconPassword : string
  }

type Player =
  { name : string
    duration : float32
    score : int16
  }

type Players = Player array

type ServerInfo =
  { players : Players
    address : string
    maxPlayers : int
    map : string
  }

type ServerStatus =
  | Offline of string
  | Online of ServerInfo

let private awaitTask (timeout: int) task =
  async {
    use cts = new CancellationTokenSource()
    use timer = Task.Delay (timeout, cts.Token)
    let! completed = Async.AwaitTask <| Task.WhenAny(task, timer)
    if completed = (task :> Task) then
      cts.Cancel ()
      let! result = Async.AwaitTask task
      return Some result
    else return None
  }

let private awaitSideEffectTask (timeout: int) (task : Task) =
  async {
    use cts = new CancellationTokenSource()
    use timer = Task.Delay (timeout, cts.Token)
    let! completed = Async.AwaitTask <| Task.WhenAny(task, timer)
    if completed = task then
      cts.Cancel ()
      let! result = Async.AwaitTask task
      return Some result
    else return None
  }

let private ipEndpointFrom (config : Configuration) =
  IPEndPoint(IPAddress.Parse config.ip, config.port)

let private serverConnectionTimeout = 250

let private openRcon (config : Configuration) : Result<RCON, string> =
  try
    let rcon = new RCON(ipEndpointFrom config, config.rconPassword)
    rcon.ConnectAsync()
    |> awaitSideEffectTask serverConnectionTimeout
    |> Async.RunSynchronously
    |> Option.map (fun () -> Ok rcon)
    |> Option.defaultWith (fun () -> rcon.Dispose(); Error "Timeout while connecting to server via RCON")

  with ex ->
    Error ex.Message

let private runCommand (command : string) (rcon : RCON) : Result<string, string> =
  try
    rcon.SendCommandAsync command
    |> Async.AwaitTask
    |> Async.RunSynchronously
    |> Ok

  with ex ->
    Error ex.Message

let private delayToWaitForAdminsHaveBeenRefreshed = 750

let reloadAdminCacheAndCustomChatColors config =
  match openRcon config with
  | Ok rcon ->
      using rcon
        (fun r ->
          runCommand "sm_reloadadmins" r
          |> Result.bind (fun _ ->
            Async.Sleep delayToWaitForAdminsHaveBeenRefreshed |> Async.RunSynchronously
            runCommand "sm_reloadccc" r
          )
        )

  | Error err ->
      Error err

let reloadCustomChatColors config =
  match openRcon config with
  | Ok rcon ->
      using rcon (runCommand "sm_reloadccc")

  | Error err ->
      Error err

let private infoAsStatus address (status : Result<PacketFormats.IQueryInfo * Players, string>) =
  match status with
  | Ok (response, players) ->
      let info = response :?> PacketFormats.SourceQueryInfo
      { players = players
        address = address
        maxPlayers = info.MaxPlayers |> System.Convert.ToInt32
        map = info.Map
      }
      |> Online

  | Error info ->
      Offline info

let private serverQueryTimeout = 10000

let private asPlayer (player : PacketFormats.ServerQueryPlayer) : Player =
  { name = player.Name
    duration = player.Duration
    score = player.Score
  }

let private playersOnServer config : Result<Players, string> =
  try
    ServerQuery.Players(ipEndpointFrom config)
    |> awaitTask serverQueryTimeout
    |> Async.RunSynchronously
    |> Option.map (Array.map asPlayer >> Ok)
    |> Option.defaultValue (Error "Timeout")

  with ex ->
    Error ex.Message

let private statusOfServer config =
  try
    ServerQuery.Info(ipEndpointFrom config, ServerQuery.ServerType.Source)
    |> awaitTask serverQueryTimeout
    |> Async.RunSynchronously
    |> function | Some o -> Ok o | None -> Error "Timeout"

  with ex ->
    Error ex.Message

let statusWithPlayers config : ServerStatus =
  statusOfServer config
  |> Result.bind (fun status -> playersOnServer config |> Result.map (fun players -> (status, players)))
  |> infoAsStatus (sprintf "%s:%i" config.ip config.port)