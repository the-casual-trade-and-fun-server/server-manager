module Server.DiscordWebhook

open System.Text
open System.Net.Http
open Newtonsoft.Json

type DiscordConfig =
  {
    webhook : string
    mention : string
  }

type Message =
  {
    content : string
  }

let private message text : Message =
  {
    content = text
  }

let private send (url : string) (msg : Message) =
  let rec tryToSendWithRetries tries count =
    async {
      try
        use client = new HttpClient()
        use content = new StringContent(JsonConvert.SerializeObject msg, Encoding.UTF8, "application/json")
        let! res = client.PostAsync(url, content) |> Async.AwaitTask

        if not res.IsSuccessStatusCode then
          let! err = res.Content.ReadAsStringAsync() |> Async.AwaitTask
          if count = tries then
            printfn "Failed to send discord webhook: %s\nRespone: %s" res.ReasonPhrase err
          else
            do! Async.Sleep 100
            do! tryToSendWithRetries tries (count+1)

      with e ->
        if count = tries then
          printfn "Failed to send discord webhook: %s" e.Message
        else
          do! Async.Sleep 100
          do! tryToSendWithRetries tries (count+1)
    }

  tryToSendWithRetries 5 0
  |> Async.RunSynchronously

let private linkToSteamUser (steamId : string) =
  steamId
    .Replace("STEAM_", "")
    .Split(":")
  |> function
    [|_ ; y ; z|] ->
      z |> int
      |> (*) 2
      |> (+) (int y)
      |> sprintf "https://steamcommunity.com/profiles/[U:1:%i]"
    | _ -> sprintf "https://rep.tf/%s" steamId
  |> sprintf "[%s](%s)" steamId

let private expiredVipMessage vip =
  vip
  |> linkToSteamUser
  |> sprintf "Removed player with ID %s from the VIP list."

let private addedVipMessage vip =
  vip
  |> linkToSteamUser
  |> sprintf "Added player with ID %s to the VIP list."

let private pausedVipMessage vip =
  vip
  |> linkToSteamUser
  |> sprintf "Player with ID %s has paused VIP and will be removed from the VIP list until resuming."

let private resumedVipMessage vip =
  vip
  |> linkToSteamUser
  |> sprintf "Player with ID %s has resumed VIP and was added to the VIP list again."

let private removedVipMessage vip =
  vip
  |> linkToSteamUser
  |> sprintf "Removed player with ID %s from the VIP list due to rule breaking."

let notifyExpiration (config : DiscordConfig) expiredVip =
  sprintf "%s %s" config.mention (expiredVipMessage expiredVip)
  |> message
  |> send config.webhook

let notifyVipAddition (config : DiscordConfig) vip =
  sprintf "%s %s" config.mention (addedVipMessage vip)
  |> message
  |> send config.webhook

let notifyVipPaused (config : DiscordConfig) vip =
  sprintf "%s %s" config.mention (pausedVipMessage vip)
  |> message
  |> send config.webhook

let notifyVipResumed (config : DiscordConfig) vip =
  sprintf "%s %s" config.mention (resumedVipMessage vip)
  |> message
  |> send config.webhook

let notifyRemoveDueToRuleBreaking (config : DiscordConfig) vip =
  sprintf "%s %s" config.mention (removedVipMessage vip)
  |> message
  |> send config.webhook