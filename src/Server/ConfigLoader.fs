module Server.ConfigLoader

open Infrastructure.Configuration
open DiscordWebhook
open System.IO
open Newtonsoft.Json

type Configuration =
  {
    ip : string
    port : int
    accessKey : string
    hoursBetweenCheckOfExpiredVIPs : float
    database : DatabaseConnectionInformation
    adminGroups : SourceModAdminGroups
    discord : DiscordConfig
    gameserver : GameServer.Configuration
  }

let loadConfig (file : string) : Result<Configuration, string> =
  try
    let config =
      File.ReadAllText file
      |> JsonConvert.DeserializeObject<Configuration>

    if config.hoursBetweenCheckOfExpiredVIPs > 0. then
      Ok config
    else
      Error "hoursBetweenCheckOfExpiredVIPs must be higher than 0"

  with e ->
    Error e.Message