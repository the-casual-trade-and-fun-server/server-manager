module Server.RequestHandler.GameServerInfo

open Server.GameServer
open Giraffe

type PlayerDTO =
  { name : string
    score : int
    duration : float32
  }

type StatusDTO =
  { online : bool
    reason : string
    players : PlayerDTO array
    address : string
    maxPlayers : int
    map : string
  }

let private asPlayerDTO (player : Player) : PlayerDTO =
  { name = player.name
    score = player.score |> int32
    duration = player.duration
  }

let private statusAsDTO (status : ServerStatus) : StatusDTO =
  match status with
  | Online info ->
      { online = true
        reason = ""
        players = info.players |> Array.map asPlayerDTO
        address = info.address
        maxPlayers = info.maxPlayers
        map = info.map
      }

  | Offline reason ->
      { online = false
        reason = reason
        players = [||]
        address = ""
        maxPlayers = 0
        map = ""
      }

let serverStatus (config : Configuration) _ =
  statusWithPlayers config
  |> statusAsDTO
  |> Newtonsoft.Json.JsonConvert.SerializeObject
  |> setBodyFromString
  |> Successful.ok