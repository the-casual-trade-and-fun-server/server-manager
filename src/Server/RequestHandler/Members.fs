module Server.RequestHandler.Members

open Domain.Members.Types
open Domain.Members.Events
open Domain.Members.Projections
open Domain.Members.Commands
open Domain.Members.Behavior
open Domain.Members.RoleOfProjection
open Microsoft.AspNetCore.Http
open Utils

type SerializedMember =
  {
    id : string
    rank : string
  }

let private serializeRole (role : Role) : string =
  match role with
  | ServerOwner -> "server-owner"
  | TeamMember -> "team-member"
  | VIP -> "vip"
  | PausedVIP -> "paused-vip"
  | Player -> "player"

let getRole dateOfView (ctx : HttpContext) (history : Events) : string =
  ctx
  |> queryParameter "steamid" parseSteamId
  |> Result.map (fun steamId -> roleOf steamId dateOfView history)
  |> Result.map serializeRole
  |> function
  | Ok role ->
      sprintf "{ \"success\":true, \"role\":\"%s\" }" role

  | Error err ->
      err
      |> argumentParsingErrorAsString
      |> asError

let private serializeRank (rank : Rank) =
  match rank with
  | Owner -> "owner"
  | Admin -> "admin"
  | Moderator -> "moderator"
  | Mapper -> "mapper"
  | PermanentVIP -> "permanent-vip"

let private serializeMember (id : SteamId, rank : Rank) : SerializedMember =
  {
    id = steamIdString id
    rank = serializeRank rank
  }

let getAll (history : Events) =
  history
  |> projectMembers
  |> Map.toSeq
  |> Seq.map serializeMember
  |> Newtonsoft.Json.JsonConvert.SerializeObject

let private addMemberErrorAsString (error : AddMemberError) =
  match error with
  | AlreadyMemberWithAnotherRank -> "AlreadyMemberWithAnotherRank"
  | IsAlreadyAdded -> "IsAlreadyAdded"

let private addMemberCommand user rank : AddMember =
  {
    user = user
    rank = rank
  }

let asAddMemberCommand data : Result<AddMember, ArgumentParsingError> =
  Ok addMemberCommand
  |> applyArgument (parseArg "user" parseSteamId data)
  |> applyArgument (parseArg "rank" parseRank data)

let add (ctx : HttpContext) =
  ctx |> handleCommandWithFormData
    asAddMemberCommand
    handleAddMember
    addMemberErrorAsString

let private setRankOfMemberErrorAsString (error : SetRankOfMemberError) =
  match error with
  | SetRankOfMemberError.NotAMember -> "NotAMember"
  | RankAlreadySetted -> "RankAlreadySetted"

let private setRankOfMemberCommand user rank : SetRankOfMemberTo =
  {
    user = user
    newRank = rank
  }

let asSetRankOfMemberCommand data : Result<SetRankOfMemberTo, ArgumentParsingError> =
  Ok setRankOfMemberCommand
  |> applyArgument (parseArg "user" parseSteamId data)
  |> applyArgument (parseArg "newRank" parseRank data)

let setRank (ctx : HttpContext) =
  ctx |> handleCommandWithFormData
    asSetRankOfMemberCommand
    handleSetRankOfMember
    setRankOfMemberErrorAsString

let asRemoveMemberCommand data: Result<RemoveMember, ArgumentParsingError> =
  parseArg "user" parseSteamId data
  |> Result.map (fun user -> { user = user })

let private removeMemberErrorAsString (error : RemoveMemberError) =
  match error with
  | RemoveMemberError.NotAMember -> "NotAMember"

let remove req =
  req |> handleCommandWithFormData
    asRemoveMemberCommand
    handleRemoveMember
    removeMemberErrorAsString