module Server.RequestHandler.Utils

open Giraffe
open Microsoft.AspNetCore.Http
open System.Globalization
open Domain.Members.Types

type ArgumentParsingError =
  | Missing of string
  | WrongFormat of string

let dateTimeToUnixTimestamp (date : System.DateTime) : int64 =
  (System.DateTimeOffset date).ToUnixTimeMilliseconds()

let parseArg arg (parser : string -> Result<'T, ArgumentParsingError>) data : Result<'T, ArgumentParsingError> =
  data
  |> Map.tryFind arg
  |> Option.flatten
  |> Option.map parser
  |> Option.defaultValue (Error <| Missing arg)

let parseOptionalArg arg (parser : string -> Result<'T, ArgumentParsingError>) data : Result<'T option, ArgumentParsingError> =
  data
  |> Map.tryFind arg
  |> Option.map (Option.map (parser >> Result.map Some) >> Option.defaultValue (Ok None))
  |> Option.defaultValue (Error <| Missing arg)

let queryParameter param (parser : string -> Result<'T, ArgumentParsingError>) (ctx : HttpContext) =
  ctx.TryGetQueryStringValue param
  |> Option.map parser
  |> Option.defaultValue (Error <| Missing param)

let parseRank (input : string) : Result<Rank, ArgumentParsingError> =
  match input.ToLowerInvariant() with
  | "owner" -> Ok Owner
  | "admin" -> Ok Admin
  | "moderator" -> Ok Moderator
  | "mapper" -> Ok Mapper
  | "permanent-vip" -> Ok PermanentVIP
  | _ -> sprintf "Rank '%s' does not exists" input |> WrongFormat |> Error

let parseSteamId input : Result<SteamId, ArgumentParsingError> =
  // TODO: Validate input
  input |> SteamId |> Ok

let parseString input : Result<string, ArgumentParsingError> =
  Ok input

let parseDurationInDays (input : string) : Result<uint16, ArgumentParsingError> =
  try
    input |> uint16 |> Ok

  with _ ->
    Error <| WrongFormat "duration"

let parseDuration (input : string) : Result<uint32, ArgumentParsingError> =
  try
    input |> uint32 |> Ok

  with _ ->
    Error <| WrongFormat "duration"

let parseDateTime (input: string) : Result<System.DateTime, ArgumentParsingError> =
  match System.DateTime.TryParseExact(input, "dd.MM.yyyy HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None) with
  | (true, date) -> Ok <| date.ToUniversalTime()
  | (false, _) -> Error <| WrongFormat "date"

let parseDateTimeFromTimestamp (input: string) : Result<System.DateTime, ArgumentParsingError> =
  try
    input
    |> int64
    |> System.DateTimeOffset.FromUnixTimeSeconds
    |> fun offset -> offset.UtcDateTime
    |> Ok

  with err ->
    Error <| WrongFormat err.Message

let asError msg =
  sprintf "{ \"success\" : false, \"error\" : \"%s\" }" msg

let applyArgument argument result =
  result |> Result.bind (fun a -> argument |> Result.map a)

let asResponse (error : string option) =
  error
  |> Option.map asError
  |> Option.map setBodyFromString
  |> Option.map ServerErrors.internalError
  |> Option.defaultValue (Successful.ok (setBodyFromString "{ \"success\" : true }"))

let argumentParsingErrorAsString error =
  match error with
  | Missing argument -> sprintf "Missing argument '%s'" argument
  | WrongFormat info -> sprintf "Wrong format: %s" info

let formOfRequest (req : HttpRequest) : Map<string, string option> =
  req.Form
  |> Seq.map (fun a -> (a.Key, if a.Value.Count > 0 then Some (a.Value.Item 0) else None))
  |> Map.ofSeq

let handleCommandWithFormData cmdParser cmdHandler errorMapper (ctx : HttpContext) =
  ctx.Request
  |> formOfRequest
  |> cmdParser
  |> Result.mapError argumentParsingErrorAsString
  |> Result.map (fun cmd -> fun history -> cmdHandler cmd history |> Result.mapError errorMapper)

let andThen (func : 'a -> 'b) (task : Async<'a>) : Async<'b> =
  async {
    let! a = task
    return func a
  }