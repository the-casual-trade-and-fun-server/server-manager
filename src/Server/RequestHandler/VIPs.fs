module Server.RequestHandler.VIPs

open Domain.Members.Events
open Domain.Members.AllVIPsProjection
open Domain.Members.Commands
open Domain.Members.Behavior
open Utils

type SerializedVIP =
  {
    id : string
    date : int64
    duration : uint32
    pausedSince : int64 array
    durationFromPauses : uint32
    countOfPauses : int
  }

let private serializeDate (date : System.DateTime) =
  date.ToLocalTime().ToString()

let private serializeVIP (vip : Domain.Members.AllVIPsProjection.VIPInfo) : SerializedVIP =
  {
    id = (vip.id |> Domain.Members.Types.steamIdString)
    date = dateTimeToUnixTimestamp vip.date
    duration = vip.duration
    pausedSince = vip.pausedSince |> Option.map dateTimeToUnixTimestamp |> Option.toArray
    durationFromPauses = vip.durationFromPauses
    countOfPauses = vip.countOfPauses
  }

let getAll projectionDate (history : Events) =
  history
  |> allVIPs projectionDate
  |> Array.map serializeVIP
  |> Newtonsoft.Json.JsonConvert.SerializeObject

let private giveUserVIPCommand user date duration : GiveUserVIP =
  {
    user = user
    date = date
    duration = duration
  }

let asGiveUserVIPCommand data : Result<GiveUserVIP, ArgumentParsingError> =
  (Ok giveUserVIPCommand)
  |> applyArgument (parseArg "user" parseSteamId data)
  |> applyArgument (parseArg "date" parseDateTimeFromTimestamp data)
  |> applyArgument (parseArg "duration" parseDuration data)

let private giveUserVIPErrorAsString (error : GiveUserVIPError) =
  match error with
  | UserIsAlreadyVIP -> "UserIsAlreadyVIP"
  | UserIsCurrentlyPaused -> "UserIsCurrentlyPaused"
  | UserIsARuleBreaker -> "UserIsARuleBreaker"
  | UserIsAMember -> "UserIsAMember"
  | GiveUserVIPError.TimezoneIsNotUTC -> "TimezoneIsNotUTC"
  | GiveUserVIPError.DurationOfZero -> "DurationOfZero"

let giveToUser req =
  req |> handleCommandWithFormData
    asGiveUserVIPCommand
    handleGiveUserVIP
    giveUserVIPErrorAsString

let private correctDurationCommand user duration : CorrectDurationOfVIP =
  {
    user = user
    newDuration = duration
  }

let asCorrectVIPDurationCommand data : Result<CorrectDurationOfVIP, ArgumentParsingError> =
  Ok correctDurationCommand
  |> applyArgument (parseArg "user" parseSteamId data)
  |> applyArgument (parseArg "newDuration" parseDuration data)

let private correctDurationOfVIPErrorAsString (error : CorrectDurationOfVIPError) =
  match error with
  | CorrectDurationOfVIPError.UserIsNotAVIP -> "user is not a VIP"
  | CorrectDurationOfVIPError.NoDifferenceInDuration -> "NoDifferenceInDuration"
  | CorrectDurationOfVIPError.DurationOfZero -> "GiveUserVIPError"

let correctDuration req =
  req |> handleCommandWithFormData
    asCorrectVIPDurationCommand
    handleCorrectDurationOfVIP
    correctDurationOfVIPErrorAsString

let private extendDurationCommand user date duration : ExtendDurationOfVIP =
  {
    user = user
    dateOfExtension = date
    additionalDuration = duration
  }

let asExtendVIPDurationCommand data : Result<ExtendDurationOfVIP, ArgumentParsingError> =
  Ok extendDurationCommand
  |> applyArgument (parseArg "user" parseSteamId data)
  |> applyArgument (parseArg "dateOfExtension" parseDateTimeFromTimestamp data)
  |> applyArgument (parseArg "additionalDuration" parseDuration data)

let private extendDurationOfVIPErrorAsString (error : ExtendDurationOfVIPError) =
  match error with
  | ExtendDurationOfVIPError.UserIsNotAVIP -> "UserIsNotAVIP"
  | ExtendDurationOfVIPError.VIPIsExpired -> "VIPIsExpired"
  | ExtendDurationOfVIPError.TimezoneIsNotUTC -> "TimezoneIsNotUTC"
  | ExtendDurationOfVIPError.DurationOfZero -> "DurationOfZero"

let extendDuration req =
  req |> handleCommandWithFormData
    asExtendVIPDurationCommand
    handleExtendDurationOfVIP
    extendDurationOfVIPErrorAsString

let private extendTimeForAllVIPsCommand date duration : ExtendTimeForAllVIPs =
  {
    date = date
    duration = duration
  }

let asExtendTimeForAllVIPsCommand data : Result<ExtendTimeForAllVIPs, ArgumentParsingError> =
  Ok extendTimeForAllVIPsCommand
  |> applyArgument (parseArg "date" parseDateTimeFromTimestamp data)
  |> applyArgument (parseArg "duration" parseDurationInDays data)

let private extendTimeForAllVIPsErrorAsString error : string =
  match error with
  | ExtendTimeForAllVIPsError.DurationOfZero -> "DurationOfZero"

let extendTimeForAllVIPs req =
  req |> handleCommandWithFormData
    asExtendTimeForAllVIPsCommand
    handleExtendTimeForAllVIPs
    extendTimeForAllVIPsErrorAsString

let private transferTimeErrorAsString error =
  match error with
  | TransferTimeError.DurationIsNotAvailable -> "DurationIsNotAvailable"
  | TransferTimeError.UserIsNotAVIP -> "UserIsNotAVIP"
  | TransferTimeError.VIPIsExpired -> "VIPIsExpired"
  | TransferTimeError.DurationOfZero -> "DurationOfZero"
  | TransferTimeError.TimezoneIsNotUTC -> "TimezoneIsNotUTC"

let private transferTimeCommand user target date duration : TransferTime =
  {
    user = user
    target = target
    date = date
    duration = duration
  }

let asTransferTimeCommand data =
  data
  |> parseArg "user" parseSteamId
  |> Result.map transferTimeCommand
  |> applyArgument (data |> parseArg "target" parseSteamId)
  |> applyArgument (data |> parseArg "date" parseDateTimeFromTimestamp)
  |> applyArgument (data |> parseArg "duration" parseDurationInDays)

let transferTime req =
  req |> handleCommandWithFormData
    asTransferTimeCommand
    handleTransferTime
    transferTimeErrorAsString

let asRemoveVIPDueToRuleBreakingCommand data : Result<RemoveVIPDueToRuleBreaking, ArgumentParsingError> =
  parseArg "user" parseSteamId data
  |> Result.map (fun user -> { user = user })

let private removeVIPDueToRuleBreakingErrorAsString (error : RemoveVIPDueToRuleBreakingError) =
  match error with
  | RemoveVIPDueToRuleBreakingError.UserIsNotAVIP -> "UserIsNotAVIP"

let removeDueToRuleBreaking req =
  req |> handleCommandWithFormData
    asRemoveVIPDueToRuleBreakingCommand
    handleRemoveVIPDueToRuleBreaking
    removeVIPDueToRuleBreakingErrorAsString

let private pauseVIPCommand user date : PauseVIP =
  {
    user = user
    pauseDate = date
  }

let asPauseVIPCommand data : Result<PauseVIP, ArgumentParsingError> =
  Ok pauseVIPCommand
  |> applyArgument (parseArg "user" parseSteamId data)
  |> applyArgument (parseArg "pauseDate" parseDateTimeFromTimestamp data)

let private pauseVIPErrorAsString (error : PauseVIPError) =
  match error with
  | PauseVIPError.UserIsNotAVIP -> "UserIsNotAVIP"
  | PauseVIPError.PauseBeforeStartDate -> "PauseBeforeStartDate"
  | PauseVIPError.PauseAfterExpiration -> "PauseAfterExpiration"
  | PauseVIPError.AlreadyPaused -> "AlreadyPaused"
  | PauseVIPError.TimezoneIsNotUTC -> "TimezoneIsNotUTC"

let pause req =
  req |> handleCommandWithFormData
    asPauseVIPCommand
    handlePauseVIP
    pauseVIPErrorAsString

let private resumeVIPCommand user date : ResumeVIP =
  {
    user = user
    resumeDate = date
  }

let asResumeVIPCommand data : Result<ResumeVIP, ArgumentParsingError> =
  Ok resumeVIPCommand
  |> applyArgument (parseArg "user" parseSteamId data)
  |> applyArgument (parseArg "resumeDate" parseDateTimeFromTimestamp data)

let private resumeVIPErrorAsString (error : ResumeVIPError) =
  match error with
  | ResumeVIPError.TimezoneIsNotUTC -> "TimezoneIsNotUTC"
  | ResumeVIPError.IsNotPaused -> "IsNotPaused"

let resume req =
  req |> handleCommandWithFormData
    asResumeVIPCommand
    handleResumeVIP
    resumeVIPErrorAsString