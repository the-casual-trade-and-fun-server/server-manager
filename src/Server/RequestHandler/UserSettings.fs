module Server.RequestHandler.UserSettings

open Domain.ACL.UserSettings
open Microsoft.AspNetCore.Http
open Utils

type CustomChatColorsDTO =
  { Tag : string array
    Name : string array
    Chat : string array
  }

type Intention =
  | ReloadCustomChatColors

let private handleSqlCommandWithFormData cmdParser (cmdHandler : 'cmd -> 'sqlConnection -> Async<unit>) (intention : Intention option) (ctx : HttpContext) : Result<'sqlConnection -> Async<Intention option>, string> =
  ctx.Request
  |> formOfRequest
  |> cmdParser
  |> Result.mapError argumentParsingErrorAsString
  |> Result.map (fun cmd connection -> cmdHandler cmd connection |> andThen (fun () -> intention))

let private queryJoinsoundOf user connection =
  joinsoundOf user connection
  |> andThen (Option.toArray>>Newtonsoft.Json.JsonConvert.SerializeObject)

let getJoinsoundOf ctx =
  ctx
  |> queryParameter "user" parseSteamId
  |> Result.map queryJoinsoundOf
  |> Result.mapError (argumentParsingErrorAsString>>asError)

let getDefinedJoinsounds _ =
  Ok (fun connection ->
    definedJoinsounds connection
    |> andThen Newtonsoft.Json.JsonConvert.SerializeObject
  )

let private asColorsDTO (colors : CustomChatColors) : CustomChatColorsDTO =
  { Tag = colors.Tag |> Option.map ((+) "#") |> Option.toArray
    Name = colors.Name |> Option.map ((+) "#") |> Option.toArray
    Chat = colors.Chat |> Option.map ((+) "#") |> Option.toArray
  }

let private queryCustomChatColorsOf user connection =
  customChatColorsOf user connection
  |> andThen (asColorsDTO>>Newtonsoft.Json.JsonConvert.SerializeObject)

let getCustomChatColorsOf request =
  request
  |> queryParameter "user" parseSteamId
  |> Result.map queryCustomChatColorsOf
  |> Result.mapError (argumentParsingErrorAsString>>asError)

let private setCustomChatColorsCommand user tag name chat =
  { User = user
    Colors =
      { Tag = tag
        Name = name
        Chat = chat
      }
  }

let asSetCustomChatColorsCommand data : Result<SetCustomChatColorsCommand, ArgumentParsingError> =
  Ok setCustomChatColorsCommand
  |> applyArgument (parseArg "user" parseSteamId data)
  |> applyArgument (parseOptionalArg "tag" parseString data)
  |> applyArgument (parseOptionalArg "name" parseString data)
  |> applyArgument (parseOptionalArg "chat" parseString data)

let setCustomChatColors ctx =
  handleSqlCommandWithFormData
    asSetCustomChatColorsCommand
    handleSetCustomChatColors
    (Some ReloadCustomChatColors)
    ctx

let private setJoinsoundCommand user sound =
  { User = user
    Sound = sound
  }

let asSetJoinsosundCommand data : Result<SetJoinsoundCommand, ArgumentParsingError> =
  Ok setJoinsoundCommand
  |> applyArgument (parseArg "user" parseSteamId data)
  |> applyArgument (parseArg "sound" parseString data)

let setJoinsound req =
  handleSqlCommandWithFormData
    asSetJoinsosundCommand
    handleSetJoinsound
    None
    req

let private useNoJoinsoundCommand user : UseNoJoinsoundCommand =
  { User = user }

let asUseNoJoinsoundCommand data : Result<UseNoJoinsoundCommand, ArgumentParsingError> =
  data
  |> parseArg "user" parseSteamId
  |> Result.map useNoJoinsoundCommand

let useNoJoinsound req =
  handleSqlCommandWithFormData
    asUseNoJoinsoundCommand
    handleUseNoJoinsound
    None
    req