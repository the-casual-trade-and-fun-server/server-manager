﻿module Server.Main

open Infrastructure.EventStore.Access
open Infrastructure.SqlOperations
open Domain.ACL
open Domain.Members.Events

let private eventStoreWithConfig (config : ConfigLoader.Configuration) =
  startEventStore config.database
  |> Result.map (fun (store, observer) -> (config, store, observer))

let private hoursAsMilliseconds (hours: float) =
  System.TimeSpan.FromHours(hours).TotalMilliseconds

let private restoreUserSettingsFor vip config : string option =
  async {
    try
      use! connection = openConnection config
      do! Domain.ACL.UserSettings.restoreUserSettingsFor vip connection
      return None

    with ex ->
      return Some (sprintf "Failed to restore user settings: %s" ex.Message)
  }
  |> Async.RunSynchronously

let private reloadAdminCacheAndCustomChatColors (config : GameServer.Configuration) =
  match GameServer.reloadAdminCacheAndCustomChatColors config with
  | Ok response ->
      printfn "[Info] Reloaded admin cache and custom chat colors - %s" response

  | Error error ->
      printfn "[Error] Failed to reload admin cache and custom chat colors: %s" error

let private reloadCustomChatColors (config : GameServer.Configuration) =
  match GameServer.reloadCustomChatColors config with
  | Ok response ->
      printfn "[Info] Reloaded custom chat colors - %s" response

  | Error error ->
      printfn "[Error] Failed to reload custom chat colors: %s" error

let private onVipAdded (config : ConfigLoader.Configuration) steamId =
  let vip = Domain.Members.Types.steamIdString steamId
  DiscordWebhook.notifyVipAddition config.discord vip
  restoreUserSettingsFor vip config.database |> Option.iter (printfn "%s")

let private rememberUserSettingsFor vip config : string option =
  async {
    try
      use! connection = openConnection config

      let! rememberError =
        async {
          try
            do! UserSettings.rememberUserSettingsFor vip connection
            return None
          with ex ->
            return Some (sprintf "Failed to remember user settings: %s" ex.Message)
        }

      let! removeError =
        async {
          try
            do! UserSettings.removeUserSettingsFor vip connection
            return None
          with ex ->
            return Some (sprintf "Failed to remove user settings: %s" ex.Message)
        }

      return
        rememberError
        |> Option.map (fun err -> sprintf "%s%s" err (removeError |> Option.defaultValue ""))

    with ex ->
      return Some (sprintf "Failed to open connection for remebering and removing user settings: %s" ex.Message)
  }
  |> Async.RunSynchronously

let private onVipExpired (config : ConfigLoader.Configuration) vip =
  let user = Domain.Members.Types.steamIdString vip
  DiscordWebhook.notifyExpiration config.discord user
  rememberUserSettingsFor user config.database |> Option.iter (printfn "%s")

let private onVipPaused (config : ConfigLoader.Configuration) vip =
  let user = Domain.Members.Types.steamIdString vip
  DiscordWebhook.notifyVipPaused config.discord user
  rememberUserSettingsFor user config.database |> Option.iter (printfn "%s")

let private onVipResumed (config : ConfigLoader.Configuration) vip =
  let user = Domain.Members.Types.steamIdString vip
  DiscordWebhook.notifyVipResumed config.discord user
  restoreUserSettingsFor user config.database |> Option.iter (printfn "%s")

let private removeUserSettingsFor vip config : string option =
  async {
    try
      use! connection = openConnection config
      do! UserSettings.removeUserSettingsFor vip connection
      return None

    with ex ->
      return Some (sprintf "Failed to remove user settings: %s" ex.Message)
  }
  |> Async.RunSynchronously

let private onVipRemoved (config : ConfigLoader.Configuration) vip =
  let user = Domain.Members.Types.steamIdString vip
  DiscordWebhook.notifyRemoveDueToRuleBreaking config.discord user
  removeUserSettingsFor user config.database |> Option.iter (printfn "%s")

let private removeExpiredVips eventStore date =
  Infrastructure.EventStore.Access.effect
    (Domain.Members.Behavior.handleRemoveExpiredVips { date = date })
    eventStore
  |> Option.iter (printfn "[Error] Failed to remove expired VIPs: %s")

let private needsToReloadAdminCache event =
  match event with
  | MemberWasAdded _
  | MemberGotNewRank _
  | MemberWasRemoved _
  | UserRecievedVIP _
  | VIPWasPaused _
  | VIPWasResumed _
  | VIPHasExpired _
  | VIPWasRemovedDueToRuleBreaking _ ->
      true

  | VIPTransferredTimeTo _
  | VIPDurationWasCorrected _
  | VIPDurationWasExtended _ ->
      false

let private afterSourcemodAdminsChanged (config : ConfigLoader.Configuration) event =
  match event with
  | UserRecievedVIP (steamId, _, _) ->
      onVipAdded config steamId

  | VIPHasExpired steamId ->
      onVipExpired config steamId

  | VIPWasPaused (steamId, _) ->
      onVipPaused config steamId

  | VIPWasResumed (steamId, _) ->
      onVipResumed config steamId

  | VIPWasRemovedDueToRuleBreaking steamId ->
      onVipRemoved config steamId

  | _ ->
      ()

let private updateSourcemodAdmins (config : ConfigLoader.Configuration) events =
  match Domain.ACL.MembersForSourceModAdmins.updateReadmodel config.database config.adminGroups events |> Async.RunSynchronously with
  | None ->
      events |> List.iter (afterSourcemodAdminsChanged config)

      if events |> List.exists (needsToReloadAdminCache) then
        reloadAdminCacheAndCustomChatColors config.gameserver

  | Some error ->
      printfn "%s" error

let private initSourcemodAdminsReadmodel (config : ConfigLoader.Configuration) eventStore =
  eventStore
  |> Infrastructure.EventStore.Access.getEvents
  |> Domain.ACL.MembersForSourceModAdmins.initReadmodel config.database config.adminGroups
  |> Async.Start

let run config eventStore (eventObserver: IEvent<Domain.Members.Events.Events>) =
  use __ =
    eventObserver
    |> Observable.subscribe (updateSourcemodAdmins config)

  use timer = new System.Timers.Timer()
  timer.Interval <- hoursAsMilliseconds config.hoursBetweenCheckOfExpiredVIPs
  timer.Start()
  use __ =
    timer.Elapsed
    |> Observable.subscribe (fun e -> removeExpiredVips eventStore e.SignalTime)

  removeExpiredVips eventStore System.DateTime.Now
  initSourcemodAdminsReadmodel config eventStore

  WebServer.run config (fun () -> reloadCustomChatColors config.gameserver) eventStore

[<EntryPoint>]
let main _ =
  ConfigLoader.loadConfig "./config.json"
  |> Result.mapError (sprintf "Failed to load config: %s")
  |> Result.bind eventStoreWithConfig
  |> function
    | Ok (config, eventStore, eventObserver) ->
        run config eventStore eventObserver
        0

    | Error error ->
        printfn "Failed to load events: %s" error
        1