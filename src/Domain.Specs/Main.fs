module Domain.Specs.Main

open Expecto.Tests

let tests =
  testList "Members"
    [ Members.Behavior.Tests.tests
      Members.Projections.Tests.tests
    ]

[<EntryPoint>]
let main args =
  runTestsWithCLIArgs
    []
    args
    tests
