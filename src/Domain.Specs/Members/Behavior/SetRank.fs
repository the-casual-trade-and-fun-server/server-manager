module Domain.Specs.Members.Behavior.SetRank

open Expecto
open Domain.Members.Commands
open Domain.Members.Behavior
open Domain.Specs.TestBase
open Domain.Specs.Members.TestHelpers

let private setRankTo rank user : SetRankOfMemberTo =
  {
    user = user
    newRank = rank
  }

let private whenSetRank user rank =
  handleSetRankOfMember (user |> setRankTo rank)

let private rankAlreadySetted : SetRankOfMemberError =
  SetRankOfMemberError.RankAlreadySetted

let private isNotAMember : SetRankOfMemberError =
  SetRankOfMemberError.NotAMember

let private testUser = steamId "STEAM_1:0:173628"
let tests =
  [
    test "Set rank of member from 'Admin' to 'Owner'" {
      Given [ testUser |> wasAddedAsMemberWithRank adminRank ]
      |> whenSetRank testUser ownerRank
      |> Then [ testUser |> rankWasSettedTo ownerRank ]
    }

    test "Set rank of member will fail if the new rank equals old rank" {
      Given [ testUser |> wasAddedAsMemberWithRank adminRank ]
      |> whenSetRank testUser adminRank
      |> ThenFailWith rankAlreadySetted
    }

    test "Set rank of member will fail if user is not a member" {
      Given noEvents
      |> whenSetRank testUser adminRank
      |> ThenFailWith isNotAMember
    }
  ]
  |> testList "Set rank of member"