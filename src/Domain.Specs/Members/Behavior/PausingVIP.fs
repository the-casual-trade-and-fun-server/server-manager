module Domain.Specs.Members.Behavior.PausingVIP

open Expecto
open Domain.Members.Behavior
open Domain.Members.Commands
open Domain.Specs.Members.TestHelpers
open Domain.Specs.TestBase

let private pauseVIPOn date user : PauseVIP =
  {
    user = user
    pauseDate = date
  }

let private whenPausing user pauseDate =
  handlePauseVIP (user |> pauseVIPOn pauseDate)

let private timezoneIsNotUTC : PauseVIPError =
  PauseVIPError.TimezoneIsNotUTC

let private userIsNotAVIP : PauseVIPError =
  PauseVIPError.UserIsNotAVIP

let private pauseAfterVIPExpiration : PauseVIPError =
  PauseAfterExpiration

let private pauseBeforeStartDate : PauseVIPError =
  PauseBeforeStartDate

let private alreadyPaused : PauseVIPError =
  AlreadyPaused

let private testUser = steamId "STEAM_0:0:12341111"
let tests =
  [
    test "Pause VIP duration" {
      let startDate = date 01 01 2018
      let duration = durationFromDays 30
      let pauseDate = date 16 01 2018

      Given [ testUser |> receivedVIP startDate duration ]
      |> whenPausing testUser pauseDate
      |> Then [ testUser |> pausedVIPOn pauseDate ]
    }

    test "Pause VIP will fail if timezone is not UTC" {
      Given [ testUser |> receivedVIP (date 01 01 2018) (durationFromDays 50) ]
      |> whenPausing testUser (date 10 01 2018 |> time 18 00 00 |> withLocalTimeZone)
      |> ThenFailWith timezoneIsNotUTC
    }

    test "Pause VIP duration will fail user is not a VIP" {
      let pauseDate = date 16 01 2018

      Given noEvents
      |> whenPausing testUser pauseDate
      |> ThenFailWith userIsNotAVIP
    }

    test "Pause VIP duration will fail if VIP is already Paused" {
      let startDate = date 01 01 2018
      let duration = durationFromDays 30
      let pauseDate = date 16 01 2018
      let nextPauseDate = date 18 01 2018

      Given
        [
          testUser |> receivedVIP startDate duration
          testUser |> pausedVIPOn pauseDate
        ]
      |> whenPausing testUser nextPauseDate
      |> ThenFailWith alreadyPaused
    }

    test "Pause VIP duration will fail if a pause is scheduled" {
      let startDate = date 01 01 2018
      let duration = durationFromDays 30
      let scheduledPauseDate = date 16 01 2018
      let earlierPauseDate = date 18 01 2018

      Given
        [
          testUser |> receivedVIP startDate duration
          testUser |> pausedVIPOn scheduledPauseDate
        ]
      |> whenPausing testUser earlierPauseDate
      |> ThenFailWith alreadyPaused
    }

    test "Pause VIP duration will fail if pause date is after VIP is expired" {
      let startDate = date 01 01 2018
      let duration = durationFromDays 15
      let pauseDateAfterVIPDuration = date 10 02 2018

      Given
        [ testUser |> receivedVIP startDate duration
          testUser |> hasExpired
        ]
      |> whenPausing testUser pauseDateAfterVIPDuration
      |> ThenFailWith pauseAfterVIPExpiration
    }

    test "Pause VIP duration will fail if the pause date is before the start date" {
      let startDate = date 31 01 2018
      let duration = durationFromDays 10
      let pauseDateBeforeStart = date 15 01 2018

      Given [ testUser |> receivedVIP startDate duration ]
      |> whenPausing testUser pauseDateBeforeStart
      |> ThenFailWith pauseBeforeStartDate
    }
  ]
  |> testList "Pausing VIP"