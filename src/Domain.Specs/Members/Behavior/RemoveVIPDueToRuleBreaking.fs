module Domain.Specs.Members.Behavior.RemoveVIPDueToRuleBreaking

open Expecto
open Domain.Members.Commands
open Domain.Members.Behavior
open Domain.Specs.TestBase
open Domain.Specs.Members.TestHelpers

let private removeVIPDueToRuleBreaking user : RemoveVIPDueToRuleBreaking =
  {
    user = user
  }

let private whenRemovingDueToRuleBreaking user =
  handleRemoveVIPDueToRuleBreaking (removeVIPDueToRuleBreaking user)

let private userIsNotAVIP : RemoveVIPDueToRuleBreakingError =
  RemoveVIPDueToRuleBreakingError.UserIsNotAVIP

let private testUser = steamId "STEAM_1:0:123495"
let tests =
  [
    test "Remove VIP due to rule breaking" {
      let startDate = date 01 01 2018
      let duration = durationFromDays 10

      Given [ testUser |> receivedVIP startDate duration ]
      |> whenRemovingDueToRuleBreaking testUser
      |> Then [ testUser |> wasRemovedDueToRuleBreaking ]
    }

    test "Remove VIP due to rule breaking will fail if user is not a VIP" {
      Given noEvents
      |> whenRemovingDueToRuleBreaking testUser
      |> ThenFailWith userIsNotAVIP
    }

    test "Remove a paused VIP" {
      Given
        [
          testUser |> receivedVIP (date 01 01 2019) (durationFromDays 90)
          testUser |> pausedVIPOn (date 01 01 2019)
        ]
      |> whenRemovingDueToRuleBreaking testUser
      |> Then [ testUser |> wasRemovedDueToRuleBreaking ]
    }

    test "Block a expired VIP" {
      Given
        [ testUser |> receivedVIP (date 01 01 2019) (durationFromDays 20)
          testUser |> hasExpired
        ]
      |> whenRemovingDueToRuleBreaking testUser
      |> Then [ testUser |> wasRemovedDueToRuleBreaking ]
    }
  ]
  |> testList "Remove VIP due to rule breaking"