module Domain.Specs.Members.Behavior.RemoveExpiredVips

open Expecto
open Domain.Specs.TestBase
open Domain.Members.Behavior
open Domain.Members.Commands
open Domain.Specs.Members.TestHelpers

let private thenExpect events actualEvents =
  Expect.equal actualEvents events "Events are not the same"

let private removeExpiredVipsCommand date : RemoveExpiredVips =
  { date = date }

let private whenRemovingExpiredVipsOn date history =
  handleRemoveExpiredVips (removeExpiredVipsCommand date) history

let private testUser = steamId "STEAM_0:0:12345"
let private secondUser = steamId "STEAM_0:0:54321"
let private noExpiredVips = []

let tests =
  [ test "removes expired vip" {
      Given [ testUser |> receivedVIP (date 01 01 2019) (durationFromDays 20) ]
      |> whenRemovingExpiredVipsOn (date 21 01 2019)
      |> thenExpect [ testUser |> hasExpired ]
    }

    test "removes expired vip - time got extended" {
      Given
        [ testUser |> receivedVIP (date 01 01 2019) (durationFromDays 10)
          testUser |> durationWasExtendedBy (durationFromDays 5)
        ]
      |> whenRemovingExpiredVipsOn (date 16 01 2019)
      |> thenExpect [ testUser |> hasExpired ]
    }

    test "removes expired vip - time got corrected" {
      Given
        [ testUser |> receivedVIP (date 01 01 2019) (durationFromDays 30)
          testUser |> durationWasCorrectedTo (durationFromDays 20)
        ]
      |> whenRemovingExpiredVipsOn (date 21 01 2019)
      |> thenExpect [ testUser |> hasExpired ]
    }

    test "removes expired vip - time was transfered" {
      Given
        [ testUser |> receivedVIP (date 01 01 2019) (durationFromDays 30)
          testUser |> transferredTimeTo secondUser (durationFromDays 20)
        ]
      |> whenRemovingExpiredVipsOn (date 11 01 2019)
      |> thenExpect [ testUser |> hasExpired ]
    }

    test "removes expired vip - resumed pause" {
      Given
        [ testUser |> receivedVIP (date 01 01 2019) (durationFromDays 10)
          testUser |> pausedVIPOn (date 05 01 2019)
          testUser |> resumedVIPOn (date 10 01 2019)
        ]
      |> whenRemovingExpiredVipsOn (date 21 01 2019)
      |> thenExpect [ testUser |> hasExpired ]
    }

    test "removes expired vips" {
      Given
        [ testUser |> receivedVIP (date 01 01 2019) (durationFromDays 10)
          secondUser |> receivedVIP (date 02 01 2019) (durationFromDays 9)
        ]
      |> whenRemovingExpiredVipsOn (date 11 01 2019)
      |> thenExpect
        [ testUser |> hasExpired
          secondUser |> hasExpired
        ]
    }

    test "not started vip is not expired" {
      Given [ testUser |> receivedVIP (date 20 01 2019) (durationFromDays 20) ]
      |> whenRemovingExpiredVipsOn (date 10 01 2019)
      |> thenExpect noExpiredVips
    }

    test "active vip is not expired" {
      Given [ testUser |> receivedVIP (date 01 01 2019) (durationFromDays 10) ]
      |> whenRemovingExpiredVipsOn (date 05 01 2019)
      |> thenExpect noExpiredVips
    }

    test "paused vip is not expired" {
      Given
        [ testUser |> receivedVIP (date 01 01 2019) (durationFromDays 5)
          testUser |> pausedVIPOn (date 03 01 2019)
        ]
      |> whenRemovingExpiredVipsOn (date 15 01 2019)
      |> thenExpect noExpiredVips
    }

    test "paused vip is not expired - check at begin of pause" {
      Given
        [ testUser |> receivedVIP (date 01 01 2019) (durationFromDays 5)
          testUser |> pausedVIPOn (date 03 01 2019)
        ]
      |> whenRemovingExpiredVipsOn (date 03 01 2019)
      |> thenExpect noExpiredVips
    }

    test "vip is not expired due to extra duration from pause" {
      Given
        [ testUser |> receivedVIP (date 01 01 2019) (durationFromDays 5)
          testUser |> pausedVIPOn (date 03 01 2019)
          testUser |> resumedVIPOn (date 24 01 2019)
        ]
      |> whenRemovingExpiredVipsOn (date 25 01 2019)
      |> thenExpect noExpiredVips
    }

    test "vip is not expired due to previous expiration" {
      Given
        [ testUser |> receivedVIP (date 01 01 2019) (durationFromDays 10)
          testUser |> hasExpired
        ]
      |> whenRemovingExpiredVipsOn (date 11 01 2019)
      |> thenExpect noExpiredVips
    }
  ]
  |> testList "Remove expired vips"