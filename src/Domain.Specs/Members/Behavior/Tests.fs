module Domain.Specs.Members.Behavior.Tests

open Expecto

let tests =
  [ AddMember.tests
    SetRank.tests
    RemoveMember.tests
    GiveUserVIP.tests
    CorrectDuration.tests
    ExtendDuration.tests
    ExtendTimeForAll.tests
    TransferTime.tests
    RemoveVIPDueToRuleBreaking.tests
    PausingVIP.tests
    ResumeVIP.tests
    RemoveExpiredVips.tests
  ]
  |> testList "Behavior"