module Domain.Specs.Members.Behavior.TransferTime

open Expecto
open Domain.Members.Commands
open Domain.Members.Behavior
open Domain.Specs.Members.TestHelpers
open Domain.Specs.TestBase

let private transferTimeTo target date duration user : TransferTime =
  {
    user = user
    target = target
    date = date
    duration = duration
  }

let private whenTransferingTime user target date duration =
  handleTransferTime (user |> transferTimeTo target date duration)

let private durationOfZero : TransferTimeError =
  TransferTimeError.DurationOfZero

let private timezoneIsNotUTC : TransferTimeError =
  TransferTimeError.TimezoneIsNotUTC

let private notAVIP : TransferTimeError =
  TransferTimeError.UserIsNotAVIP

let private isExpired : TransferTimeError =
  TransferTimeError.VIPIsExpired

let private durationNotAvailable : TransferTimeError =
  TransferTimeError.DurationIsNotAvailable

let private testUser = steamId "STEAM_0:0:1234567"
let private targetUser = steamId "STEAM_0:0:7654321"
let tests =
  [
    test "Transfer time from VIP to user which becomes a VIP" {
      Given [ testUser |> receivedVIP (date 01 01 2019) (durationFromDays 50) ]
      |> whenTransferingTime testUser targetUser (date 05 01 2019) (durationInDays 30)
      |> Then
        [
          testUser |> transferredTimeTo targetUser (durationFromDays 30)
          targetUser |> receivedVIP (date 05 01 2019) (durationFromDays 30)
        ]
    }

    test "Transfer time from VIP to VIP will extend time of target" {
      Given
        [
          testUser |> receivedVIP (date 01 01 2019) (durationFromDays 190)
          targetUser |> receivedVIP (date 15 01 2019) (durationFromDays 20)
        ]
      |> whenTransferingTime testUser targetUser (date 20 01 2019) (durationInDays 50)
      |> Then
        [
          testUser |> transferredTimeTo targetUser (durationFromDays 50)
          targetUser |> durationWasExtendedBy (durationFromDays 50)
        ]
    }

    test "Transfer time from VIP to expired VIP will give target VIP" {
      Given
        [
          testUser |> receivedVIP (date 01 02 2019) (durationFromDays 80)
          targetUser |> receivedVIP (date 15 01 2019) (durationFromDays 20)
          targetUser |> hasExpired
        ]
      |> whenTransferingTime testUser targetUser (date 20 02 2019) (durationInDays 40)
      |> Then
        [
          testUser |> transferredTimeTo targetUser (durationFromDays 40)
          targetUser |> receivedVIP (date 20 02 2019) (durationFromDays 40)
        ]
    }

    test "Transfer time after VIP time was extenden will give target VIP" {
      Given
        [
          testUser |> receivedVIP (date 01 01 2019) (durationFromDays 10)
          testUser |> durationWasExtendedBy (durationFromDays 50)
        ]
      |> whenTransferingTime testUser targetUser (date 09 01 2019) (durationInDays 40)
      |> Then
        [
          testUser |> transferredTimeTo targetUser (durationFromDays 40)
          targetUser |> receivedVIP (date 09 01 2019) (durationFromDays 40)
        ]
    }

    test "Transfer time after VIP paused and resumed will give target VIP" {
      Given
        [
          testUser |> receivedVIP (date 01 01 2019) (durationFromDays 50)
          testUser |> pausedVIPOn (date 02 01 2019)
          testUser |> resumedVIPOn (date 15 02 2019)
        ]
      |> whenTransferingTime testUser targetUser (date 20 02 2019) (durationInDays 40)
      |> Then
        [
          testUser |> transferredTimeTo targetUser (durationFromDays 40)
          targetUser |> receivedVIP (date 20 02 2019) (durationFromDays 40)
        ]
    }

    test "Transfer time after VIP paused, transferred time and resumed will give target VIP" {
      Given
        [
          testUser |> receivedVIP (date 01 01 2019) (durationFromDays 52)
          testUser |> pausedVIPOn (date 02 01 2019)
          testUser |> transferredTimeTo (targetUser) (durationFromDays 5)
          targetUser |> hasExpired
          testUser |> resumedVIPOn (date 15 02 2019)
        ]
      |> whenTransferingTime testUser targetUser (date 20 02 2019) (durationInDays 40)
      |> Then
        [
          testUser |> transferredTimeTo targetUser (durationFromDays 40)
          targetUser |> receivedVIP (date 20 02 2019) (durationFromDays 40)
        ]
    }

    test "Transfer time will fail when user is not a VIP" {
      Given noEvents
      |> whenTransferingTime testUser targetUser (date 01 01 2019) (durationInDays 30)
      |> ThenFailWith notAVIP
    }

    test "Transfer time will fail when VIP has not started" {
      Given [ testUser |> receivedVIP (date 19 02 2019) (durationFromDays 50) ]
      |> whenTransferingTime testUser targetUser (date 01 01 2019) (durationInDays 30)
      |> ThenFailWith notAVIP
    }

    test "Transfer time will fail when duration is zero" {
      Given [ testUser |> receivedVIP (date 01 01 2019) (durationFromDays 50) ]
      |> whenTransferingTime testUser targetUser (date 05 08 2019) (durationInDays 0)
      |> ThenFailWith durationOfZero
    }

    test "Transfer time will fail when datetime is not UTC" {
      Given [ testUser |> receivedVIP (date 01 01 2019) (durationFromDays 50) ]
      |> whenTransferingTime testUser targetUser (date 05 08 2019 |> withLocalTimeZone) (durationInDays 0)
      |> ThenFailWith timezoneIsNotUTC
    }

    test "Transfer time will fail when user is an expired VIP" {
      Given
        [ testUser |> receivedVIP (date 01 01 2019) (durationFromDays 50)
          testUser |> hasExpired
        ]
      |> whenTransferingTime testUser targetUser (date 05 08 2019) (durationInDays 50)
      |> ThenFailWith isExpired
    }

    test "Transfer time will fail when transferred duration is more than available" {
      Given [ testUser |> receivedVIP (date 01 01 2019) (durationFromDays 20) ]
      |> whenTransferingTime testUser targetUser (date 01 01 2019) (durationInDays 50)
      |> ThenFailWith durationNotAvailable
    }

    test "Transfer time will fail when transferred duration is more than remaining time" {
      Given [ testUser |> receivedVIP (date 01 01 2019) (durationFromDays 60) ]
      |> whenTransferingTime testUser targetUser (date 24 02 2019) (durationInDays 30)
      |> ThenFailWith durationNotAvailable
    }
  ]
  |> testList "Transfer time between VIPs or from VIP to user"