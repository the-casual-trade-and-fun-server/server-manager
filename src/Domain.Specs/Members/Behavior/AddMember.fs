module Domain.Specs.Members.Behavior.AddMember

open Expecto
open Domain.Members.Commands
open Domain.Members.Behavior
open Domain.Specs.TestBase
open Domain.Specs.Members.TestHelpers

let private addToMembersWithRank rank user : AddMember =
  {
    user = user
    rank = rank
  }

let private whenAddingToMembers user rank =
  handleAddMember (user |> addToMembersWithRank rank)

let private isAlreadyAMemberWithAnotherRank : AddMemberError =
  AddMemberError.AlreadyMemberWithAnotherRank

let private isAlreadyAddedToMembers : AddMemberError =
  AddMemberError.IsAlreadyAdded

let private testUser = steamId "STEAM_0:0:8123123"
let tests =
  [
    test "Add user to members with rank 'Admin'" {
      Given noEvents
      |> whenAddingToMembers testUser adminRank
      |> Then [ testUser |> wasAddedAsMemberWithRank adminRank ]
    }

    test "Add user to members with rank 'Admin' when user is already an admin will fail" {
      Given [ testUser |> wasAddedAsMemberWithRank adminRank ]
      |> whenAddingToMembers testUser adminRank
      |> ThenFailWith isAlreadyAddedToMembers
    }

    test "Add user to members with rank 'Owner' will fail if user is already an admin" {
      Given [ testUser |> wasAddedAsMemberWithRank adminRank ]
      |> whenAddingToMembers testUser ownerRank
      |> ThenFailWith isAlreadyAMemberWithAnotherRank
    }

    test "Add user to members with rank 'Admin' when user was removed before" {
      Given
        [
          testUser |> wasAddedAsMemberWithRank ownerRank
          testUser |> wasRemovedFromMembers
        ]
      |> whenAddingToMembers testUser adminRank
      |> Then [ testUser |> wasAddedAsMemberWithRank adminRank ]
    }
  ]
  |> testList "Add user as member"