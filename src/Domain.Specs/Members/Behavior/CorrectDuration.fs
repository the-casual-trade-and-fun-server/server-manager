module Domain.Specs.Members.Behavior.CorrectDuration

open Expecto
open Domain.Members.Commands
open Domain.Members.Behavior
open Domain.Specs.TestBase
open Domain.Specs.Members.TestHelpers

let private correctDurationOfVIPTo duration user : CorrectDurationOfVIP =
  {
    user = user
    newDuration = duration
  }

let private whenCorrectingDurationTo user duration =
  handleCorrectDurationOfVIP (user |> correctDurationOfVIPTo duration)

let private userIsNotAVIP : CorrectDurationOfVIPError =
  CorrectDurationOfVIPError.UserIsNotAVIP

let private noDifferenceInDuration : CorrectDurationOfVIPError =
  CorrectDurationOfVIPError.NoDifferenceInDuration

let private durationOfZero : CorrectDurationOfVIPError =
  CorrectDurationOfVIPError.DurationOfZero

let private testUser = steamId "STEAM_1:0:192313"
let tests =
  [
    test "Correct the duration of a VIP" {
      let startDate = date 01 01 2018
      let duration = durationFromDays 5
      let newDuration = durationFromDays 3

      Given [ testUser |> receivedVIP startDate duration ]
      |> whenCorrectingDurationTo testUser newDuration
      |> Then [ testUser |> durationWasCorrectedTo newDuration ]
    }

    test "Correct duration of a VIP fails if user is not a VIP" {
      let newDuration = durationFromDays 4

      Given noEvents
      |> whenCorrectingDurationTo testUser newDuration
      |> ThenFailWith userIsNotAVIP
    }

    test "Correct duration fails when new duration equals initial duration" {
      Given [ testUser |> receivedVIP (date 01 01 2019) (durationFromDays 10) ]
      |> whenCorrectingDurationTo testUser (durationFromDays 10)
      |> ThenFailWith noDifferenceInDuration
    }

    test "Correct duration fails when new duration equals corrected duration" {
      Given
        [
          testUser |> receivedVIP (date 01 01 2019) (durationFromDays 20)
          testUser |> durationWasCorrectedTo (durationFromDays 10)
        ]
      |> whenCorrectingDurationTo testUser (durationFromDays 10)
      |> ThenFailWith noDifferenceInDuration
    }

    test "Correct duration fails when new duration equals duration after extending" {
      Given
        [
          testUser |> receivedVIP (date 01 01 2019) (durationFromDays 20)
          testUser |> durationWasExtendedBy (durationFromDays 30)
        ]
      |> whenCorrectingDurationTo testUser (durationFromDays 50)
      |> ThenFailWith noDifferenceInDuration
    }

    test "Correct duration fails when duration equals zero" {
      Given [ testUser |> receivedVIP (date 01 01 2019) (durationFromDays 10) ]
      |> whenCorrectingDurationTo testUser 0u
      |> ThenFailWith durationOfZero
    }

    test "Correct duration of a paused VIP" {
      Given
        [
          testUser |> receivedVIP (date 18 03 2019) (durationFromDays 90)
          testUser |> pausedVIPOn (date 24 03 2019)
        ]
      |> whenCorrectingDurationTo testUser (durationFromDays 20)
      |> Then [ testUser |> durationWasCorrectedTo (durationFromDays 20) ]
    }

    test "Correct duration fails when VIP is removed due to rule breaking" {
      Given
        [
          testUser |> receivedVIP (date 18 03 2019) (durationFromDays 90)
          testUser |> wasRemovedDueToRuleBreaking
        ]
      |> whenCorrectingDurationTo testUser (durationFromDays 10)
      |> ThenFailWith userIsNotAVIP
    }
  ]
  |> testList "Correct duration"