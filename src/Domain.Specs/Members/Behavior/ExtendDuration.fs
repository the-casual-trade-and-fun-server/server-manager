module Domain.Specs.Members.Behavior.ExtendDuration

open Expecto
open Domain.Members.Commands
open Domain.Members.Behavior
open Domain.Specs.TestBase
open Domain.Specs.Members.TestHelpers

let private extendDurationBy duration date user : ExtendDurationOfVIP =
  {
    user = user
    dateOfExtension = date
    additionalDuration = duration
  }

let private whenExtendingDuration user date duration =
  handleExtendDurationOfVIP (user |> extendDurationBy duration date)

let private userIsNotAVIP : ExtendDurationOfVIPError =
  ExtendDurationOfVIPError.UserIsNotAVIP

let private userIsExpired : ExtendDurationOfVIPError =
  ExtendDurationOfVIPError.VIPIsExpired

let private timezoneIsNotUTC : ExtendDurationOfVIPError =
  ExtendDurationOfVIPError.TimezoneIsNotUTC

let private durationOfZero : ExtendDurationOfVIPError =
  ExtendDurationOfVIPError.DurationOfZero

let private testUser = steamId "STEAM_0:0:123941"
let tests =
  [
    test "Extend duration of a VIP" {
      let startDate = date 01 01 2018
      let duration = durationFromDays 5
      let dateOfExtension = date 02 01 2018
      let extraDuration = durationFromDays 3

      Given [ testUser |> receivedVIP startDate duration ]
      |> whenExtendingDuration testUser dateOfExtension extraDuration
      |> Then [ testUser |> durationWasExtendedBy extraDuration ]
    }

    test "Extend duration of a VIP will fail if user is not a VIP" {
      let dateOfExtension = date 01 01 2018
      let extraDuration = durationFromDays 3

      Given noEvents
      |> whenExtendingDuration testUser dateOfExtension extraDuration
      |> ThenFailWith userIsNotAVIP
    }

    test "Extend duration of a VIP will fail if user is expired" {
      let startDate = date 01 01 2018
      let duration = durationFromDays 5
      let dateOfExtension = date 12 01 2018
      let extraDuration = durationFromDays 3

      Given
        [ testUser |> receivedVIP startDate duration
          testUser |> hasExpired
        ]
      |> whenExtendingDuration testUser dateOfExtension extraDuration
      |> ThenFailWith userIsExpired
    }

    test "Extend duration fails when VIP is removed due to rule breaking" {
      Given
        [
          testUser |> receivedVIP (date 01 01 2018) (durationFromDays 5)
          testUser |> wasRemovedDueToRuleBreaking
        ]
      |> whenExtendingDuration testUser (date 12 01 2018) (durationFromDays 3)
      |> ThenFailWith userIsNotAVIP
    }

    test "Extend duration of a VIP will fail if timezone is not UTC" {
      let dateOfExtension = date 05 01 2018 |> time 15 30 00 |> withLocalTimeZone

      Given [ testUser |> receivedVIP (date 01 01 2018) (durationFromDays 10) ]
      |> whenExtendingDuration testUser dateOfExtension (durationFromDays 50)
      |> ThenFailWith timezoneIsNotUTC
    }

    test "Extend duration fails when duration equals zero" {
      Given [ testUser |> receivedVIP (date 01 01 2018) (durationFromDays 40) ]
      |> whenExtendingDuration testUser (date 04 01 2018) 0u
      |> ThenFailWith durationOfZero
    }

    test "Extend duration of a paused VIP" {
      Given
        [
          testUser |> receivedVIP (date 01 01 2018) (durationFromDays 40)
          testUser |> pausedVIPOn (date 05 02 2018)
        ]
      |> whenExtendingDuration testUser (date 08 02 2018) (durationFromDays 20)
      |> Then [ testUser |> durationWasExtendedBy (durationFromDays 20) ]
    }
  ]
  |> testList "Extend duration"