module Domain.Specs.Members.Behavior.ResumeVIP

open Expecto
open Domain.Members.Commands
open Domain.Members.Behavior
open Domain.Specs.Members.TestHelpers
open Domain.Specs.TestBase

let private resumeVIPOn date user : ResumeVIP =
  {
    user = user
    resumeDate = date
  }

let private whenResume user resumeDate =
  handleResumeVIP (user |> resumeVIPOn resumeDate)

let private timezoneIsNotUTC : ResumeVIPError =
  ResumeVIPError.TimezoneIsNotUTC

let private isNotPaused : ResumeVIPError =
  ResumeVIPError.IsNotPaused

let private testUser = steamId "STEAM_1:0:12344321"

let tests =
  [
    test "Resume VIP after pause" {
      let startDate = date 10 01 2018
      let duration = durationFromDays 30
      let pauseDate = date 15 01 2018
      let resumeDate = date 20 01 2018

      Given
        [
          testUser |> receivedVIP startDate duration
          testUser |> pausedVIPOn pauseDate
        ]
      |> whenResume testUser resumeDate
      |> Then [ testUser |> resumedVIPOn resumeDate ]
    }

    test "Resume VIP will fail if timezone is not UTC" {
      Given
        [
          testUser |> receivedVIP (date 01 10 2018) (durationFromDays 40)
          testUser |> pausedVIPOn (date 20 10 2018)
        ]
        |> whenResume testUser (date 30 10 2018 |> time 14 45 00 |> withLocalTimeZone)
        |> ThenFailWith timezoneIsNotUTC
    }

    test "Resume VIP after pause when VIP would be expired if not paused before" {
      let startDate = date 10 01 2018
      let duration = durationFromDays 30
      let pauseDate = date 15 01 2018
      let resumeDate = date 29 03 2018

      Given
        [
          testUser |> receivedVIP startDate duration
          testUser |> pausedVIPOn pauseDate
        ]
      |> whenResume testUser resumeDate
      |> Then [ testUser |> resumedVIPOn resumeDate ]
    }

    test "Resume VIP will fail if user is not a VIP" {
      let resumeDate = date 01 01 2018

      Given noEvents
      |> whenResume testUser resumeDate
      |> ThenFailWith isNotPaused
    }

    test "Resume VIP will fail if user has not paused VIP" {
      let startDate = date 01 01 2018
      let duration = durationFromDays 10
      let resumeDate = date 05 01 2018

      Given [ testUser |> receivedVIP startDate duration ]
      |> whenResume testUser resumeDate
      |> ThenFailWith isNotPaused
    }

    test "Resume VIP will fail if user has resumed VIP before" {
      let startDate = date 01 01 2018
      let duration = durationFromDays 10
      let pauseDate = date 02 01 2018
      let resumeDate = date 05 01 2018
      let secondResumeDate = date 06 01 2018

      Given
        [
          testUser |> receivedVIP startDate duration
          testUser |> pausedVIPOn pauseDate
          testUser |> resumedVIPOn resumeDate
        ]
      |> whenResume testUser secondResumeDate
      |> ThenFailWith isNotPaused
    }

    test "Resume VIP will fail if user is an expired VIP" {
      let startDate = date 01 01 2018
      let duration = durationFromDays 10
      let resumeDate = date 24 01 2018

      Given
        [ testUser |> receivedVIP startDate duration
          testUser |> hasExpired
        ]
      |> whenResume testUser resumeDate
      |> ThenFailWith isNotPaused
    }

    test "Resume VIP will fail when resume date is before pause date" {
      Given
        [
          testUser |> receivedVIP (date 01 01 2018) (durationFromDays 30)
          testUser |> pausedVIPOn (date 20 01 2018)
        ]
      |> whenResume testUser (date 15 01 2018)
      |> ThenFailWith isNotPaused
    }
  ]
  |> testList "Resume VIP"