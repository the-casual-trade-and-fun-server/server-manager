module Domain.Specs.Members.Behavior.GiveUserVIP

open Expecto
open Domain.Specs.TestBase
open Domain.Specs.Members.TestHelpers
open Domain.Members.Behavior
open Domain.Members.Commands

let private giveVIP user date duration : GiveUserVIP =
  {
    user = user
    date = date
    duration = duration
  }

let private whenGivingUserVIP user date duration =
  handleGiveUserVIP (giveVIP user date duration)

let private timezoneIsNotUTC : GiveUserVIPError =
  GiveUserVIPError.TimezoneIsNotUTC

let private userIsAlreadyVIP : GiveUserVIPError =
  GiveUserVIPError.UserIsAlreadyVIP

let private userIsARuleBreaker : GiveUserVIPError =
  GiveUserVIPError.UserIsARuleBreaker

let private userIsAMember : GiveUserVIPError =
  GiveUserVIPError.UserIsAMember

let private durationOfZero : GiveUserVIPError =
  GiveUserVIPError.DurationOfZero

let private currentlyPaused : GiveUserVIPError =
  GiveUserVIPError.UserIsCurrentlyPaused

let private testUser = steamId "STEAM_1:0:123456"
let tests =
  [
    test "Give user VIP" {
      let startDate = date 01 01 2018
      let duration = durationFromDays 5

      Given noEvents
      |> whenGivingUserVIP testUser startDate duration
      |> Then [ testUser |> receivedVIP startDate duration ]
    }

    test "Give user VIP fails if timezone is not UTC" {
      let startDate = date 01 01 2018 |> time 18 00 00 |> withLocalTimeZone
      let duration = durationFromDays 5

      Given noEvents
      |> whenGivingUserVIP testUser startDate duration
      |> ThenFailWith timezoneIsNotUTC
    }

    test "Give user VIP if user was removed due to expiration" {
      let oldStartDate = date 01 12 2017
      let oldDuration = durationFromDays 10
      let startDate = date 01 01 2018
      let duration = durationFromDays 5

      Given
        [ testUser |> receivedVIP oldStartDate oldDuration
          testUser |> hasExpired
        ]
      |> whenGivingUserVIP testUser startDate duration
      |> Then [ testUser |> receivedVIP startDate duration ]
    }

    test "Give user VIP will fail if user is already a VIP" {
      let startDate = date 01 01 2018
      let duration = durationFromDays 5

      Given [ testUser |> receivedVIP startDate duration ]
      |> whenGivingUserVIP testUser startDate duration
      |> ThenFailWith userIsAlreadyVIP
    }

    test "Give user VIP will fail if user was removed due to rule breaking" {
      let oldStartDate = date 01 01 2018
      let oldDuration = durationFromDays 15
      let startDate = date 01 02 2018
      let duration = durationFromDays 10

      Given
        [
          testUser |> receivedVIP oldStartDate oldDuration
          testUser |> wasRemovedDueToRuleBreaking
        ]
      |> whenGivingUserVIP testUser startDate duration
      |> ThenFailWith userIsARuleBreaker
    }

    test "Give user VIP will fail if user is a member" {
      let startDate = date 01 01 2018
      let duration = durationFromDays 5

      Given [ testUser |> wasAddedAsMemberWithRank adminRank ]
      |> whenGivingUserVIP testUser startDate duration
      |> ThenFailWith userIsAMember
    }

    test "Give user VIP will fail if duration equals zero" {
      Given noEvents
      |> whenGivingUserVIP testUser (date 01 01 2018) 0u
      |> ThenFailWith durationOfZero
    }

    test "Give user VIP will fail if VIP is paused" {
      Given
        [
          testUser |> receivedVIP (date 01 01 2019) (durationFromDays 120)
          testUser |> pausedVIPOn (date 05 02 2019)
        ]
      |> whenGivingUserVIP testUser (date 10 02 2019) (durationFromDays 10)
      |> ThenFailWith currentlyPaused
    }
  ]
  |> testList "Give user VIP"