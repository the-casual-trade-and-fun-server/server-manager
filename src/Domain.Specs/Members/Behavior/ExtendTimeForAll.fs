module Domain.Specs.Members.Behavior.ExtendTimeForAll

open Expecto
open Domain.Members.Commands
open Domain.Members.Behavior
open Domain.Specs.TestBase
open Domain.Specs.Members.TestHelpers

let private extendTimeForAllVIPs date duration : ExtendTimeForAllVIPs =
  {
    date = date
    duration = duration
  }

let private whenExtendingTimeForAllBy duration date events =
  handleExtendTimeForAllVIPs (extendTimeForAllVIPs date duration) events

let private durationOfZero : ExtendTimeForAllVIPsError =
  ExtendTimeForAllVIPsError.DurationOfZero

let private testUser = steamId "STEAM_0:0:123941"
let private testUser2 = steamId "STEAM_0:0:123984"
let tests =
  [
    test "Will extend time of all VIPs by given duration" {
      Given
        [
          testUser |> receivedVIP (date 01 01 2019) (durationFromDays 5)
          testUser2 |> receivedVIP (date 04 01 2019) (durationFromDays 10)
        ]
      |> whenExtendingTimeForAllBy (durationInDays 20) (date 04 01 2019)
      |> Then [
        testUser |> durationWasExtendedBy (durationFromDays 20)
        testUser2 |> durationWasExtendedBy (durationFromDays 20)
      ]
    }

    test "Will extend time of paused VIPs by given duration" {
      Given
        [
          testUser |> receivedVIP (date 01 01 2019) (durationFromDays 5)
          testUser2 |> receivedVIP (date 04 01 2019) (durationFromDays 10)
          testUser |> pausedVIPOn (date 05 01 2019)
        ]
      |> whenExtendingTimeForAllBy (durationInDays 20) (date 12 01 2019)
      |> Then [
        testUser |> durationWasExtendedBy (durationFromDays 20)
        testUser2 |> durationWasExtendedBy (durationFromDays 20)
      ]
    }

    test "Will extend time of soon to be paused VIPs by given duration" {
      Given
        [
          testUser |> receivedVIP (date 01 01 2019) (durationFromDays 5)
          testUser2 |> receivedVIP (date 04 01 2019) (durationFromDays 10)
          testUser |> pausedVIPOn (date 05 01 2019)
        ]
      |> whenExtendingTimeForAllBy (durationInDays 20) (date 04 01 2019)
      |> Then [
        testUser |> durationWasExtendedBy (durationFromDays 20)
        testUser2 |> durationWasExtendedBy (durationFromDays 20)
      ]
    }

    test "Will extend time of resumed VIPs by given duration" {
      Given
        [
          testUser |> receivedVIP (date 01 01 2019) (durationFromDays 30)
          testUser2 |> receivedVIP (date 04 01 2019) (durationFromDays 10)
          testUser2 |> pausedVIPOn (date 05 01 2019)
          testUser2 |> resumedVIPOn (date 10 01 2019)
        ]
      |> whenExtendingTimeForAllBy (durationInDays 20) (date 10 01 2019)
      |> Then [
        testUser |> durationWasExtendedBy (durationFromDays 20)
        testUser2 |> durationWasExtendedBy (durationFromDays 20)
      ]
    }

    test "ignores VIPs which time not started" {
      Given
        [
          testUser |> receivedVIP (date 01 01 2019) (durationFromDays 5)
          testUser2 |> receivedVIP (date 04 01 2019) (durationFromDays 10)
        ]
      |> whenExtendingTimeForAllBy (durationInDays 20) (date 02 01 2019)
      |> Then [
        testUser |> durationWasExtendedBy (durationFromDays 20)
      ]
    }

    test "ignores expired VIPs" {
      Given
        [
          testUser |> receivedVIP (date 01 01 2019) (durationFromDays 5)
          testUser2 |> receivedVIP (date 04 01 2019) (durationFromDays 10)
          testUser |> hasExpired
        ]
      |> whenExtendingTimeForAllBy (durationInDays 20) (date 11 01 2019)
      |> Then [
        testUser2 |> durationWasExtendedBy (durationFromDays 20)
      ]
    }

    test "ignores removed VIPs due to rule breaking" {
      Given
        [
          testUser |> receivedVIP (date 01 01 2019) (durationFromDays 5)
          testUser2 |> receivedVIP (date 04 01 2019) (durationFromDays 10)
          testUser2 |> wasRemovedDueToRuleBreaking
        ]
      |> whenExtendingTimeForAllBy (durationInDays 20) (date 04 01 2019)
      |> Then [
        testUser |> durationWasExtendedBy (durationFromDays 20)
      ]
    }

    test "fails when given duration is zero" {
      Given
        [
          testUser |> receivedVIP (date 01 01 2019) (durationFromDays 5)
          testUser2 |> receivedVIP (date 04 01 2019) (durationFromDays 10)
        ]
      |> whenExtendingTimeForAllBy (durationInDays 0) (date 04 01 2019)
      |> ThenFailWith durationOfZero
    }
  ]
  |> testList "Extend time for all VIPs"