module Domain.Specs.Members.Behavior.RemoveMember

open Expecto
open Domain.Members.Commands
open Domain.Members.Behavior
open Domain.Specs.TestBase
open Domain.Specs.Members.TestHelpers

let private removeFromMembers user : RemoveMember =
  { user = user }

let private whenRemoving user =
  handleRemoveMember (removeFromMembers user)

let private isNotAMember : RemoveMemberError =
  NotAMember

let private testUser = steamId "STEAM_0:0:7123828"
let tests =
  [
    test "Remove member" {
      Given [ testUser |> wasAddedAsMemberWithRank adminRank ]
      |> whenRemoving testUser
      |> Then [ testUser |> wasRemovedFromMembers ]
    }

    test "Remove member will fail if member was not added before" {
      Given noEvents
      |> whenRemoving testUser
      |> ThenFailWith isNotAMember
    }

    test "Remove member will fail if member was removed before" {
      Given
        [
          testUser |> wasAddedAsMemberWithRank adminRank
          testUser |> wasRemovedFromMembers
        ]
      |> whenRemoving testUser
      |> ThenFailWith isNotAMember
    }
  ]
  |> testList "Remove member"