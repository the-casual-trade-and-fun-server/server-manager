module Domain.Specs.Members.Projections.Tests

open Expecto

let tests =
  [
    AllVIPs.tests
    RoleOf.tests
  ]
  |> testList "Projections"