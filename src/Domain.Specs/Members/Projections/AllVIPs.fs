module Domain.Specs.Members.Projections.AllVIPs

open Expecto
open Domain.Members.AllVIPsProjection
open Domain.Specs.Members.TestHelpers

let private given = id
let private thenExpect expected result =
  Expect.equal result expected "VIPs are not as expected"

let private vip user date duration : VIPInfo =
  {
    id = user
    date = date
    duration = duration
    pausedSince = None
    durationFromPauses = 0u
    countOfPauses = 0
  }

let private withPauseDuration duration vip : VIPInfo =
  { vip with durationFromPauses = vip.durationFromPauses + duration }

let private withCountOfPauses count vip : VIPInfo =
  { vip with countOfPauses = count }

let private pausedSince date vip : VIPInfo =
  { vip with pausedSince = Some date }

let private testUser = steamId "STEAM_1:0:123456"
let tests =
  [
    test "VIP is shown" {
      given [ testUser |> receivedVIP (date 01 01 2019) (durationFromDays 140) ]
      |> allVIPs (date 01 01 2019)
      |> thenExpect [| vip testUser (date 01 01 2019) (durationFromDays 140) |]
    }

    test "In later starting VIP is not shown" {
      given [ testUser |> receivedVIP (date 20 03 2019) (durationFromDays 50) ]
      |> allVIPs (date 01 02 2019)
      |> thenExpect [||]
    }

    test "Expried VIP is not shown" {
      given
        [ testUser |> receivedVIP (date 01 01 2019) (durationFromDays 140)
          testUser |> hasExpired
        ]
      |> allVIPs (date 21 05 2019)
      |> thenExpect [||]
    }

    test "Expried VIP is not shown - missing expired event" {
      given [ testUser |> receivedVIP (date 01 01 2019) (durationFromDays 140) ]
      |> allVIPs (date 21 05 2019)
      |> thenExpect [||]
    }

    test "VIP removed due to rule breaking is not shown" {
      given
        [
          testUser |> receivedVIP (date 01 01 2019) (durationFromDays 140)
          testUser |> wasRemovedDueToRuleBreaking
        ]
      |> allVIPs (date 01 01 2019)
      |> thenExpect [||]
    }

    test "Corrected duration is used as duration" {
      given
        [
          testUser |> receivedVIP (date 01 01 2019) (durationFromDays 30)
          testUser |> durationWasCorrectedTo (durationFromDays 60)
        ]
      |> allVIPs (date 01 01 2019)
      |> thenExpect [| vip testUser (date 01 01 2019) (durationFromDays 60) |]
    }

    test "Extended duration is added to duration" {
      given
        [
          testUser |> receivedVIP (date 01 01 2019) (durationFromDays 30)
          testUser |> durationWasExtendedBy (durationFromDays 100)
        ]
      |> allVIPs (date 01 01 2019)
      |> thenExpect [| vip testUser (date 01 01 2019) (durationFromDays 130) |]
    }

    test "Transferred time is subtracted from duration" {
      given
        [
          testUser |> receivedVIP (date 01 01 2019) (durationFromDays 30)
          testUser |> transferredTimeTo (steamId "STEAM_0:0:71241824") (durationFromDays 10)
        ]
      |> allVIPs (date 01 01 2019)
      |> thenExpect [| vip testUser (date 01 01 2019) (durationFromDays 20) |]
    }

    test "Paused VIP is shown when query after pause with date since pause" {
      given
        [
          testUser |> receivedVIP (date 01 01 2019) (durationFromDays 50)
          testUser |> pausedVIPOn (date 05 01 2019)
        ]
      |> allVIPs (date 10 02 2019)
      |> thenExpect
        [|
          vip testUser (date 01 01 2019) (durationFromDays 50)
          |> pausedSince (date 05 01 2019)
        |]
    }

    test "Paused VIP is shown on pause date with date since pause" {
      given
        [
          testUser |> receivedVIP (date 01 01 2019) (durationFromDays 50)
          testUser |> pausedVIPOn (date 05 01 2019)
        ]
      |> allVIPs (date 05 01 2019)
      |> thenExpect
        [|
          vip testUser (date 01 01 2019) (durationFromDays 50)
          |> pausedSince (date 05 01 2019)
        |]
    }

    test "In later paused VIP is shown" {
      given
        [
          testUser |> receivedVIP (date 28 01 2019) (durationFromDays 90)
          testUser |> pausedVIPOn (date 15 02 2019)
        ]
      |> allVIPs (date 30 01 2019)
      |> thenExpect [| vip testUser (date 28 01 2019) (durationFromDays 90) |]
    }

    test "In later paused VIP is shown with duration from previous pauses" {
      given
        [
          testUser |> receivedVIP (date 28 01 2019) (durationFromDays 90)
          testUser |> pausedVIPOn (date 15 02 2019)
          testUser |> resumedVIPOn (date 10 03 2019)
          testUser |> pausedVIPOn (date 02 04 2019)
          testUser |> resumedVIPOn (date 08 04 2019)
          testUser |> pausedVIPOn (date 20 04 2019)
        ]
      |> allVIPs (date 19 04 2019)
      |> thenExpect
        [|
          vip testUser (date 28 01 2019) (durationFromDays 90)
          |> withPauseDuration (durationFromDays 29)
          |> withCountOfPauses 2
        |]
    }

    test "Paused VIP is shown at time when VIP would be expired" {
      given
        [
          testUser |> receivedVIP (date 10 02 2019) (durationFromDays 30)
          testUser |> pausedVIPOn (date 20 02 2019)
        ]
      |> allVIPs (date 24 04 2019)
      |> thenExpect
        [|
          vip testUser (date 10 02 2019) (durationFromDays 30)
          |> pausedSince (date 20 02 2019)
        |]
    }

    test "Resumed VIP is show with duration from pausing" {
      given
        [
          testUser |> receivedVIP (date 01 01 2019) (durationFromDays 50)
          testUser |> pausedVIPOn (date 05 01 2019)
          testUser |> resumedVIPOn (date 10 01 2019)
        ]
      |> allVIPs (date 10 01 2019)
      |> thenExpect
        [|
          vip testUser (date 01 01 2019) (durationFromDays 50)
          |> withPauseDuration (durationFromDays 5)
          |> withCountOfPauses 1
        |]
    }

    test "VIP is shown after resuming a pause which would be expired" {
      given
        [
          testUser |> receivedVIP (date 01 01 2019) (durationFromDays 5)
          testUser |> pausedVIPOn (date 02 01 2019)
          testUser |> resumedVIPOn (date 01 02 2019)
        ]
      |> allVIPs (date 01 02 2019)
      |> thenExpect
        [|
          vip testUser (date 01 01 2019) (durationFromDays 5)
          |> withPauseDuration (durationFromDays 30)
          |> withCountOfPauses 1
        |]
    }

    test "Corrected duration during pause is shown after resuming" {
      given
        [
          testUser |> receivedVIP (date 01 01 2019) (durationFromDays 30)
          testUser |> pausedVIPOn (date 20 01 2019)
          testUser |> durationWasCorrectedTo (durationFromDays 35)
          testUser |>resumedVIPOn (date 01 02 2019)
        ]
      |> allVIPs (date 10 02 2019)
      |> thenExpect
        [|
          vip testUser (date 01 01 2019) (durationFromDays 35)
          |> withPauseDuration (durationFromDays 12)
          |> withCountOfPauses 1
        |]
    }
  ]
  |> testList "All VIPs"