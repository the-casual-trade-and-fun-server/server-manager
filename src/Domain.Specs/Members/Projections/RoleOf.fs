module Domain.Specs.Members.Projections.RoleOf

open Expecto
open Domain.Specs.Members.TestHelpers
open Domain.Members.RoleOfProjection

let private given = id
let private expect expected result =
  Expect.equal result expected "role is not what was expected"

let private serverOwnerRole =
  Role.ServerOwner

let private teamMemberRole =
  Role.TeamMember

let private vipRole =
  Role.VIP

let private pausedVipRole =
  Role.PausedVIP

let private playerRole =
  Role.Player

let private testUser = steamId "STEAM_0:0:1724124"
let private viewDate = date 01 01 2019

let tests =
  [
    test "member with rank 'owner' is an server owner" {
      given [ testUser |> wasAddedAsMemberWithRank ownerRank ]
      |> roleOf testUser viewDate
      |> expect serverOwnerRole
    }

    test "member with rank 'admin' is a team member" {
      given [ testUser |> wasAddedAsMemberWithRank adminRank ]
      |> roleOf testUser viewDate
      |> expect teamMemberRole
    }

    test "member with rank 'moderator' is a team member" {
      given [ testUser |> wasAddedAsMemberWithRank moderatorRank ]
      |> roleOf testUser viewDate
      |> expect teamMemberRole
    }

    test "member with rank 'mapper' is a player" {
      given [ testUser |> wasAddedAsMemberWithRank mapperRank ]
      |> roleOf testUser viewDate
      |> expect playerRole
    }

    test "member with rank 'permanent VIP' is a VIP" {
      given [ testUser |> wasAddedAsMemberWithRank permanentVIPRank ]
      |> roleOf testUser viewDate
      |> expect vipRole
    }

    test "valid VIP" {
      given [ testUser |> receivedVIP (date 01 01 2019) (durationFromDays 20) ]
      |> roleOf testUser (date 01 01 2019)
      |> expect vipRole
    }

    test "not started VIP is a player" {
      given [ testUser |> receivedVIP (date 10 01 2019) (durationFromDays 20) ]
      |> roleOf testUser (date 01 01 2019)
      |> expect playerRole
    }

    test "expired VIP is a player" {
      given
        [ testUser |> receivedVIP (date 01 01 2019) (durationFromDays 15)
          testUser |> hasExpired
        ]
      |> roleOf testUser (date 20 01 2019)
      |> expect playerRole
    }

    test "expired VIP is a player - missing expired event" {
      given [ testUser |> receivedVIP (date 01 01 2019) (durationFromDays 15) ]
      |> roleOf testUser (date 20 01 2019)
      |> expect playerRole
    }

    test "paused VIP" {
      given
        [
          testUser |> receivedVIP (date 01 01 2019) (durationFromDays 20)
          testUser |> pausedVIPOn (date 10 01 2019)
        ]
      |> roleOf testUser (date 10 01 2019)
      |> expect pausedVipRole
    }

    test "user who is not a member or a VIP is a player" {
      given
        [
          testUser |> wasAddedAsMemberWithRank ownerRank
          (steamId "STEAM_0:0:6149124") |> receivedVIP (date 01 01 2019) (durationFromDays 20)
        ]
      |> roleOf (steamId "STEAM_0:0:8241941") viewDate
      |> expect playerRole
    }
  ]
  |> testList "Role of"