module Domain.Specs.Members.TestHelpers

open System
open Domain.Members.Types
open Domain.Members.Commands
open Domain.Members.Events

let steamId id = SteamId id

let permanentVIPRank = Rank.PermanentVIP
let mapperRank = Rank.Mapper
let moderatorRank = Rank.Moderator
let adminRank = Rank.Admin
let ownerRank = Rank.Owner

let wasAddedAsMemberWithRank rank user : Event =
  Event.MemberWasAdded (user, rank)

let setRankTo rank user : SetRankOfMemberTo =
  {
    user = user
    newRank = rank
  }

let rankWasSettedTo rank user : Event =
  Event.MemberGotNewRank (user, rank)

let wasRemovedFromMembers user : Event =
  Event.MemberWasRemoved user

let date day month year =
  DateTime (year, month, day, 0, 0, 0, DateTimeKind.Utc)

let time hour minute second (date: DateTime) =
  DateTime (date.Year, date.Month, date.Day, hour, minute, second, DateTimeKind.Utc)

let withLocalTimeZone datetime =
  DateTime.SpecifyKind(datetime, DateTimeKind.Local)

let durationFromDays days =
  days * 1440 |> uint32

let durationInDays days =
  days |> uint16

let receivedVIP date duration user : Event =
  Event.UserRecievedVIP (user, date, duration)

let durationWasCorrectedTo duration user : Event =
  Event.VIPDurationWasCorrected (user, duration)

let durationWasExtendedBy duration user : Event =
  Event.VIPDurationWasExtended (user, duration)

let transferredTimeTo target duration user : Event =
  Event.VIPTransferredTimeTo (user, target, duration)

let wasRemovedDueToRuleBreaking user : Event =
  Event.VIPWasRemovedDueToRuleBreaking user

let pausedVIPOn date user : Event =
  Event.VIPWasPaused (user, date)

let resumedVIPOn date user : Event =
  Event.VIPWasResumed (user, date)

let hasExpired user : Event =
  Event.VIPHasExpired user