module Domain.Specs.TestBase

open Expecto.Expect
open Expecto.Tests

let Given = id

let noEvents = []

let Then expectedEvents result =
  match result with
  | Ok events ->
      equal events expectedEvents "Evenst should equal expected events"

  | Error err ->
      failtest (sprintf "should succeed but failed with %A" err)

let ThenFailWith (expectedError : 'error) (result : Result<'a, 'error>) =
  match result with
  | Ok _ ->
      failtest "should fail but succeeded"

  | Error error ->
      equal error expectedError ""