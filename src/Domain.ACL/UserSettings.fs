module Domain.ACL.UserSettings

open Domain.Members.Types
open Infrastructure.SqlOperations

type DefinedJoinsound =
  {
    Name : string
    File : string
    Used : bool
  }

type CustomChatColors =
  {
    Tag : string option
    Name : string option
    Chat : string option
  }

type SetCustomChatColorsCommand =
  {
    User : SteamId
    Colors : CustomChatColors
  }

type SetJoinsoundCommand =
  {
    User : SteamId
    Sound : string
  }

type UseNoJoinsoundCommand =
  { User : SteamId }

let private sqlArgsWithUser (user : string) (args : MySqlConnector.MySqlParameterCollection) =
  args.AddWithValue ("@user", user) |> ignore

let private usersWithCustomChatColor connection =
  connection |> queryData "SELECT auth FROM cccm_users" (fun r -> r.GetString 0)

let private valueOrNull (value : 'T option) : obj =
  value |> Option.map box |> Option.defaultValue (box System.DBNull.Value)

let private trimHash (str : string) : string =
  str.Replace ("#", "")

let handleSetCustomChatColors (command: SetCustomChatColorsCommand) connection =
  async {
    let user = steamIdString command.User
    let! users = usersWithCustomChatColor connection
    let sql =
      if users |> List.contains user then
        "UPDATE cccm_users SET tagcolor = @tag, namecolor = @name, chatcolor = @chat WHERE auth = @user"
      else
        "INSERT INTO cccm_users (auth, hidetag, tagcolor, namecolor, chatcolor) VALUES (@user, '0', @tag, @name, @chat)"

    do!
      connection
      |> executeSqlWithArgs sql
        (fun args ->
          args |> sqlArgsWithUser user
          args.AddWithValue ("@tag", command.Colors.Tag |> Option.map trimHash |> valueOrNull) |> ignore
          args.AddWithValue ("@name", command.Colors.Name |> Option.map trimHash |> valueOrNull) |> ignore
          args.AddWithValue ("@chat", command.Colors.Chat |> Option.map trimHash |> valueOrNull) |> ignore
        )
  }

let joinsoundOf (user : SteamId) connection =
  async {
    let steamId = steamIdString user
    let! sound =
      connection |> queryDataWithArgs
        "SELECT file FROM joinsounds WHERE steamid = @user"
        (sqlArgsWithUser steamId)
        (fun r -> r.GetString 0)

    return sound |> List.tryHead
  }

let private readDefinedJoinsound (reader : System.Data.Common.DbDataReader) : DefinedJoinsound =
  {
    Name = reader.GetString 0
    File = reader.GetString 1
    Used = reader.GetBoolean 2
  }

let definedJoinsounds connection =
  async {
    return!
      connection |> queryData
        "SELECT name, file, used FROM joinsounds_list"
        readDefinedJoinsound
  }

let private readStringOrNone index (reader : System.Data.Common.DbDataReader) : string option =
  if reader.IsDBNull index then None else Some (reader.GetString index)

let private readCustomChatColors (reader : System.Data.Common.DbDataReader) : CustomChatColors =
  {
    Tag = reader |> readStringOrNone 0
    Name = reader |> readStringOrNone 1
    Chat = reader |> readStringOrNone 2
  }

let customChatColorsOf (user : SteamId) connection =
  async {
    let steamId = steamIdString user
    let! colors =
      connection |> queryDataWithArgs
        "SELECT tagcolor, namecolor, chatcolor FROM cccm_users WHERE auth = @user"
        (sqlArgsWithUser steamId)
        readCustomChatColors

    return colors
      |> List.tryHead
      |> Option.defaultValue { Tag = None; Name = None; Chat = None }
  }

let private abletoUse (state : bool option) =
  match state with
  | Some used -> not used
  | None -> false

let handleSetJoinsound (command: SetJoinsoundCommand) connection =
  async {
    let steamId = steamIdString command.User
    let! previousSounds =
      joinsoundOf command.User connection

    let! stateOfSound =
      connection |> queryDataWithArgs
        "SELECT used FROM joinsounds_list WHERE file = @sound"
        (fun args -> args.AddWithValue ("@sound", command.Sound) |> ignore)
        (fun r -> r.GetBoolean 0)

    if stateOfSound |> List.tryHead |> abletoUse then
      match previousSounds with
      | Some previous ->
          do! connection
            |> executeSqlWithArgs
              "UPDATE joinsounds SET file = @sound WHERE steamid = @user"
              (fun args ->
                args.AddWithValue ("@user", steamId) |> ignore
                args.AddWithValue ("@sound", command.Sound) |> ignore)

          do! connection
            |> executeSqlWithArgs
              "UPDATE joinsounds_list SET used = 0 WHERE file = @previousSound"
              (fun args -> args.AddWithValue ("@previousSound", previous) |> ignore)

          do! connection
            |> executeSqlWithArgs
              "UPDATE joinsounds_list SET used = 1 WHERE file = @sound"
              (fun args -> args.AddWithValue ("@sound", command.Sound) |> ignore)

      | None ->
          do! connection
            |> executeSqlWithArgs
              "INSERT INTO joinsounds (name, steamid, file) VALUES ('', @user, @sound)"
              (fun args ->
                args.AddWithValue ("@user", steamId) |> ignore
                args.AddWithValue ("@sound", command.Sound) |> ignore)

          do! connection
            |> executeSqlWithArgs
              "UPDATE joinsounds_list SET used = 1 WHERE file = @sound"
              (fun args -> args.AddWithValue ("@sound", command.Sound) |> ignore)
  }

let private sqlArgsWithFile (file : string) (args : MySqlConnector.MySqlParameterCollection) =
  args.AddWithValue ("@file", file) |> ignore

let handleUseNoJoinsound (command : UseNoJoinsoundCommand) connection =
  async {
    let! oldSound = joinsoundOf command.User connection

    match oldSound with
    | Some sound ->
        do! connection
          |> executeSqlWithArgs "DELETE FROM joinsounds WHERE steamid = @user"
            (command.User |> steamIdString |> sqlArgsWithUser)

        do! connection
          |> executeSqlWithArgs "UPDATE joinsounds_list SET used = 0 WHERE file = @file"
            (sqlArgsWithFile sound)

    | None ->
        ()
  }

let removeUserSettingsFor user connection =
  async {
    do!
      connection
      |> executeSqlWithArgs
        "DELETE FROM cccm_users WHERE auth = @user"
        (sqlArgsWithUser user)

    let! joinsound =
      connection
      |> queryDataWithArgs
        "SELECT file FROM joinsounds WHERE steamid = @user"
        (sqlArgsWithUser user)
        (fun r -> r.GetString 0)

    match joinsound |> List.tryHead with
    | Some file ->
        do!
          connection
          |> executeSqlWithArgs
            "DELETE FROM joinsounds WHERE file = @file"
            (sqlArgsWithFile file)

        do!
          connection
          |> executeSqlWithArgs
            "UPDATE joinsounds_list SET used = 0 WHERE file = @file"
            (sqlArgsWithFile file)

    | None ->
        ()
  }

let rememberUserSettingsFor user connection =
  let sql =
    "INSERT INTO remembered_user_settings
    SELECT cccm_users.auth, tagcolor, namecolor, chatcolor, file
    FROM cccm_users
    LEFT JOIN joinsounds ON joinsounds.steamid = cccm_users.auth
    WHERE auth = @user
    ON DUPLICATE KEY UPDATE tag = tagcolor, name = namecolor, chat = chatcolor, joinsound = file"
  async {
    do! connection |> executeSqlWithArgs sql (fun args -> args.AddWithValue ("@user", user) |> ignore)
  }

let restoreUserSettingsFor user connection =
  async {
    do! connection
      |> executeSqlWithArgs
        "INSERT INTO cccm_users (auth, hidetag, tagcolor, namecolor, chatcolor)
        SELECT auth, '0', tag, name, chat
        FROM remembered_user_settings
        WHERE auth = @user"
        (sqlArgsWithUser user)

    let! joinsound =
      connection
      |> queryDataWithArgs
        "SELECT joinsound FROM remembered_user_settings
        JOIN joinsounds_list ON joinsounds_list.file = joinsound AND joinsounds_list.used = 0
        WHERE auth = @user"
        (sqlArgsWithUser user)
        (fun r -> if r.IsDBNull 0 then None else Some (r.GetString 0))

    match joinsound |> List.choose id |> List.tryHead with
    | Some sound ->
        do! connection
          |> executeSqlWithArgs
            "INSERT INTO joinsounds (name, steamid, file) VALUES ('', @user, @sound)"
            (fun args ->
              args.AddWithValue ("@user", user) |> ignore
              args.AddWithValue ("@sound", sound) |> ignore
            )

        do! connection
          |> executeSqlWithArgs
            "UPDATE joinsounds_list SET used = 1 WHERE file = @sound"
            (fun args -> args.AddWithValue ("@sound", sound) |> ignore)

    | None -> ()

    do! connection
      |> executeSqlWithArgs
        "DELETE FROM remembered_user_settings WHERE auth = @user"
        (sqlArgsWithUser user)
  }