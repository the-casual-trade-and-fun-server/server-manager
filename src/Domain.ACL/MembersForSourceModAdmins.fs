module Domain.ACL.MembersForSourceModAdmins

open Infrastructure.Configuration
open Infrastructure.SqlOperations
open Domain.Members.Types
open Domain.Members.Events

let private adminGroupOf rank (adminGroups : SourceModAdminGroups) =
  match rank with
  | Owner -> adminGroups.Owner
  | Admin -> adminGroups.Admin
  | Moderator -> adminGroups.Moderator
  | Mapper -> adminGroups.Mapper
  | PermanentVIP -> adminGroups.VIP

let private vipImmunity = 0

let private immunityOf rank =
  match rank with
  | Owner -> 99
  | Admin -> 20
  | Moderator -> 10
  | Mapper -> 0
  | PermanentVIP -> vipImmunity

let private insertMember user rank adminGroups =
  "INSERT INTO members (identity, immunity, group_id) VALUES (@user, @immunity, @group)",
  (fun (args : MySqlConnector.MySqlParameterCollection) ->
    args.AddWithValue("@user", steamIdString user) |> ignore
    args.AddWithValue("@immunity", immunityOf rank ) |> ignore
    args.AddWithValue("@group", adminGroupOf rank adminGroups) |> ignore
  )

let private updateRank user rank adminGroups =
  "UPDATE members SET immunity = @immunity, group_id = @group WHERE identity = @user",
  (fun (args : MySqlConnector.MySqlParameterCollection) ->
    args.AddWithValue("@user", steamIdString user) |> ignore
    args.AddWithValue("@immunity", immunityOf rank ) |> ignore
    args.AddWithValue("@group", adminGroupOf rank adminGroups) |> ignore
  )

let private removeUser user =
  "DELETE FROM members WHERE identity = @user",
  (fun (args : MySqlConnector.MySqlParameterCollection) ->
    args.AddWithValue("@user", steamIdString user) |> ignore
  )

let private insertVip user group (receivedOn : Date) (duration : VIPDuration) =
  "DELETE FROM members WHERE identity = @user; INSERT INTO members (identity, immunity, group_id, receivedOn, duration, expiresAt) VALUES (@user, @immunity, @group, @receivedOn, @duration, @expiresAt)",
  (fun (args : MySqlConnector.MySqlParameterCollection) ->
    args.AddWithValue("@user", steamIdString user) |> ignore
    args.AddWithValue("@immunity", vipImmunity) |> ignore
    args.AddWithValue("@group", group) |> ignore
    args.AddWithValue("@receivedOn", receivedOn) |> ignore
    args.AddWithValue("@duration", duration) |> ignore
    args.AddWithValue("@expiresAt", receivedOn.AddMinutes (float duration)) |> ignore
  )

let private correctDuration user (duration : VIPDuration) =
  "UPDATE members SET duration = @duration, expiresAt = DATE_ADD(receivedOn, INTERVAL @duration MINUTE) WHERE identity = @user",
  (fun (args : MySqlConnector.MySqlParameterCollection) ->
    args.AddWithValue("@user", steamIdString user) |> ignore
    args.AddWithValue("@duration", duration) |> ignore
  )

let private extendDuration user (duration : VIPDuration) =
  "UPDATE members SET duration = duration + @duration, expiresAt = DATE_ADD(receivedOn, INTERVAL duration MINUTE) WHERE identity = @user",
  (fun (args : MySqlConnector.MySqlParameterCollection) ->
    args.AddWithValue("@user", steamIdString user) |> ignore
    args.AddWithValue("@duration", duration) |> ignore
  )

let private substractDuration user (duration : VIPDuration) =
  "UPDATE members SET duration = duration - @duration, expiresAt = DATE_ADD(receivedOn, INTERVAL duration MINUTE) WHERE identity = @user",
  (fun (args : MySqlConnector.MySqlParameterCollection) ->
    args.AddWithValue("@user", steamIdString user) |> ignore
    args.AddWithValue("@duration", duration) |> ignore
  )

let private pause user (date : Date) =
  "UPDATE members SET expiresAt = NULL, pausedSince = @date WHERE identity = @user",
  (fun (args : MySqlConnector.MySqlParameterCollection) ->
    args.AddWithValue("@user", steamIdString user) |> ignore
    args.AddWithValue("@date", date) |> ignore
  )

let private resume user date =
  "UPDATE members SET duration = duration + TIMESTAMPDIFF(minute, pausedSince, @date), expiresAt = DATE_ADD(receivedOn, INTERVAL duration MINUTE), pausedSince = NULL WHERE identity = @user",
  (fun (args : MySqlConnector.MySqlParameterCollection) ->
    args.AddWithValue("@user", steamIdString user) |> ignore
    args.AddWithValue("@date", date) |> ignore
  )

let private sqlCommand adminGroups event =
  match event with
  | MemberWasAdded (user, rank) ->
      insertMember user rank adminGroups

  | MemberGotNewRank (user, rank) ->
      updateRank user rank adminGroups

  | UserRecievedVIP (user, date, duration) ->
      insertVip user adminGroups.VIP date duration

  | VIPDurationWasCorrected (user, duration) ->
      correctDuration user duration

  | VIPDurationWasExtended (user, duration) ->
      extendDuration user duration

  | VIPTransferredTimeTo (user, _, duration) ->
      substractDuration user duration

  | VIPWasPaused (user, date) ->
      pause user date

  | VIPWasResumed (user, date) ->
      resume user date

  | VIPHasExpired user
  | VIPWasRemovedDueToRuleBreaking user
  | MemberWasRemoved user ->
      removeUser user

let projectMembersIntoDatabase connection adminGroups (events : Events) =
  async {
    try
      events
      |> List.map (sqlCommand adminGroups >> fun (cmd, args) -> connection |> executeSqlWithArgs cmd args)
      |> List.iter (Async.RunSynchronously)

      return None

    with e ->
      return Some e.Message
  }

let initReadmodel databaseConfiguration adminGroups history =
  async {
    try
      use! connection = openConnection databaseConfiguration
      do! connection |> executeSql "TRUNCATE members"

      let! error = projectMembersIntoDatabase connection adminGroups history
      error |> Option.iter (printfn "[ERROR] Failed to init SourcemodAdmins readmodel: %s")

    with ex ->
      printfn "[ERROR] Failed to init SourcemodAdmins readmodel: %s" ex.Message
  }

let updateReadmodel databaseConfiguration adminGroups events =
  async {
    try
      use! connection = openConnection databaseConfiguration

      let! error = projectMembersIntoDatabase connection adminGroups events
      return error |> Option.map (sprintf "[ERROR] Failed to update SourcemodAdmins readmodel: %s")

    with ex ->
      return Some (sprintf "[ERROR] Failed to update SourcemodAdmins readmodel: %s" ex.Message)
  }