module Infrastructure.Specs.UserSettings.DefinedJoinsounds

open Expecto
open Domain.ACL.UserSettings
open Infrastructure.SqlOperations
open SqlTestUtils

let private teardown connection =
  async {
    do! connection |> executeSql "TRUNCATE joinsounds_list"
  }

let private whenRetrievingDefinedJoinsounds (given : string list) connection =
  async {
    if not given.IsEmpty then
      do! connection |> executeSql (System.String.Join(";", given))

    return! definedJoinsounds connection
  }

let private thenExpectDefinedJoinsounds expectedJoinsounds actual connection =
  async {
    let! definedJoinsounds = actual connection
    Expect.equal definedJoinsounds (List.rev expectedJoinsounds) "Defined joinsounds are not the same"
  }

let private file = id
let private joinsound (name : string) (file : string) = (name, file)
let private used (name, file) =
  sprintf "INSERT INTO joinsounds_list (name, file, used) VALUES ('%s', '%s', 1)" name file
let private unused (name, file) =
  sprintf "INSERT INTO joinsounds_list (name, file, used) VALUES ('%s', '%s', 0)" name file

let private definedJoinsound used name file : DefinedJoinsound =
  { Name = name
    File = file
    Used = used
  }
let private definedUsedJoinsound = definedJoinsound true
let private definedUnusedJoinsound = definedJoinsound false

let tests databaseConfiguration =
  [
    testCaseAsync "retrieves all defined joinsounds" (
      given
        [ unused <| joinsound "Test" (file "test.mp3")
          used <| joinsound "Bla" (file "bla.mp3")
          used <| joinsound "Whenever you gooooo oh oh" (file "go.mp3")
          unused <| joinsound "Stretch" (file "subdir/go_on.mp3")
        ]
      |> whenRetrievingDefinedJoinsounds
      |> thenExpectDefinedJoinsounds
        [ definedUnusedJoinsound "Test" "test.mp3"
          definedUsedJoinsound "Bla" "bla.mp3"
          definedUsedJoinsound "Whenever you gooooo oh oh" "go.mp3"
          definedUnusedJoinsound "Stretch" "subdir/go_on.mp3"
        ]
      |> testSql databaseConfiguration teardown
    )

    testCaseAsync "empty list when no joinsound is defined" (
      given []
      |> whenRetrievingDefinedJoinsounds
      |> thenExpectDefinedJoinsounds []
      |> testSql databaseConfiguration teardown
    )
  ]
  |> testList "Defined joinsounds"
  |> testSequenced