module Infrastructure.Specs.UserSettings.CustomChatColorsOf

open Expecto
open Domain.ACL.UserSettings
open Infrastructure.SqlOperations
open SqlTestUtils

let private teardown connection =
  async {
    do! connection |> executeSql "TRUNCATE cccm_users"
  }

let private whenRetrieveCustomChatColorsOf user (givenData : string list) connection =
  async {
    if not givenData.IsEmpty then
      do! connection |> executeSql (System.String.Join (";", givenData))

    return! customChatColorsOf (Domain.Members.Types.SteamId user) connection
  }

let private thenExpect expectedColors actual connection =
  async {
    let! colors = actual connection
    Expect.equal colors expectedColors "Custom chat colors are not the same"
  }

let private customColors tag name chat : CustomChatColors =
  {
    Tag = Some tag
    Name = Some name
    Chat = Some chat
  }

let private emptyColors : CustomChatColors =
  {
    Tag = None
    Name = None
    Chat = None
  }

let private withTagColor color colors : CustomChatColors =
  { colors with Tag = Some color }
let private withNameColor color colors : CustomChatColors =
  { colors with Name = Some color }
let private withChatColor color colors : CustomChatColors =
  { colors with Chat = Some color }

let private testUser = "STEAM_0:0:123"
let private secondUser = "STEAM_0:0:124"

let tests databaseConfiguration =
  [
    testCaseAsync "Returns colors of given user" (
      given
        [ testUser |> hasCustomColors "#aabbcc" "#121212" "#223344"
          secondUser |> hasCustomColors "#ccddee" "#998877" "#008800"
        ]
      |> whenRetrieveCustomChatColorsOf testUser
      |> thenExpect (customColors "#aabbcc" "#121212" "#223344")
      |> testSql databaseConfiguration teardown
    )

    testCaseAsync "Returns no colors when user has no colors" (
      given noColors
      |> whenRetrieveCustomChatColorsOf testUser
      |> thenExpect emptyColors
      |> testSql databaseConfiguration teardown
    )

    testCaseAsync "user has only tag color" (
      given [ testUser |> hasCustomTagColor "#121212" ]
      |> whenRetrieveCustomChatColorsOf testUser
      |> thenExpect (emptyColors |> withTagColor "#121212")
      |> testSql databaseConfiguration teardown
    )

    testCaseAsync "user has only name color" (
      given [ testUser |> hasCustomNameColor "#aabbcc" ]
      |> whenRetrieveCustomChatColorsOf testUser
      |> thenExpect (emptyColors |> withNameColor "#aabbcc")
      |> testSql databaseConfiguration teardown
    )

    testCaseAsync "user has only chat color" (
      given [ testUser |> hasCustomChatColor "#00ff00" ]
      |> whenRetrieveCustomChatColorsOf testUser
      |> thenExpect (emptyColors |> withChatColor "#00ff00")
      |> testSql databaseConfiguration teardown
    )
  ]
  |> testList "custom chat colors of user"
  |> testSequenced