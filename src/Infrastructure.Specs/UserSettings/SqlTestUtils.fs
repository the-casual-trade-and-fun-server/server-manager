module Infrastructure.Specs.UserSettings.SqlTestUtils

open Infrastructure.SqlOperations

let given = id

let hasDefaultColors user =
  sprintf "INSERT INTO cccm_users (auth, hidetag, tagcolor, namecolor, chatcolor) VALUES ('%s', '0', NULL, NULL, NULL)" user
let hasCustomTagColor color user =
  sprintf "INSERT INTO cccm_users (auth, hidetag, tagcolor, namecolor, chatcolor) VALUES ('%s', '0', '%s', NULL, NULL)" user color
let hasCustomNameColor color user =
  sprintf "INSERT INTO cccm_users (auth, hidetag, tagcolor, namecolor, chatcolor) VALUES ('%s', '0', NULL, '%s', NULL)" user color
let hasCustomChatColor color user =
  sprintf "INSERT INTO cccm_users (auth, hidetag, tagcolor, namecolor, chatcolor) VALUES ('%s', '0', NULL, NULL, '%s')" user color
let hasCustomColors tag name chat user =
  sprintf "INSERT INTO cccm_users (auth, hidetag, tagcolor, namecolor, chatcolor) VALUES ('%s', '0', '%s', '%s', '%s')" user tag name chat

let hasJoinsound sound user =
  sprintf "INSERT INTO joinsounds (name, steamid, file) VALUES ('test', '%s', '%s')" user sound

let private joinsound sound used =
  sprintf "INSERT INTO joinsounds_list (name, file, used) VALUES ('test sound', '%s', %i)" sound used
let usedJoinsound sound =
  joinsound sound 1
let unusedJoinsound sound =
  joinsound sound 0

let hasRememberedColors tag name chat user =
  sprintf
    "INSERT INTO remembered_user_settings (auth, tag, name, chat) VALUES ('%s', '%s', '%s', '%s')"
    user
    tag
    name
    chat
let andRememberedJoinsound sound user =
  sprintf "UPDATE remembered_user_settings SET joinsound = '%s' WHERE auth = '%s'" sound user

let withNoColors user =
  (user, "0", None, None, None)
let withTagColor color user =
  (user, "0", Some color, None, None)
let andNameColor color (user, hidetag, tag, _, chat) =
  (user, hidetag, tag, Some color, chat)
let andChatColor color (user, hidetag, tag, name, _) =
  (user, hidetag, tag, name, Some color)

let withDefaultRememberedColors user =
  (user, None, None, None, None)
let withRememberedColors tag name chat user =
  (user, Some tag, Some name, Some chat, None)
let andJoinsound sound (user, tag, name, chat, _) =
  (user, tag, name, chat, Some sound)

let withJoinsound sound user = (user, sound)
let used sound = (sound, true)
let unused sound = (sound, false)

let noColors = []
let noRememberedSettings = []
let noJoinsounds = []
let noDefinedJoinsounds = []

let testSql databaseConfiguration teardown testCase =
  async {
    use! connection = openConnection databaseConfiguration
    try
      do! testCase connection

    finally
      teardown connection |> Async.RunSynchronously
  }

let private readStringOrNone index (reader : System.Data.Common.DbDataReader) =
  if reader.IsDBNull index then None else Some (reader.GetString index)

let retrieveCustomChatColors connection =
  connection
  |> queryData
    "SELECT auth, hidetag, tagcolor, namecolor, chatcolor FROM cccm_users ORDER BY auth DESC"
    (fun r -> (r.GetString 0, r.GetString 1, r |> readStringOrNone 2, r |> readStringOrNone 3, r |> readStringOrNone 4))

let retrieveJoinsounds connection =
  connection
  |> queryData
    "SELECT steamid, file FROM joinsounds ORDER BY steamid DESC"
    (fun r -> (r.GetString 0, r.GetString 1))

let retrieveDefinedJoinsounds connection =
  connection
  |> queryData
    "SELECT file, used FROM joinsounds_list ORDER BY file DESC"
    (fun r -> (r.GetString 0, r.GetBoolean 1))

let retrieveRememberedSettings connection =
  connection
  |> queryData
    "SELECT auth, tag, name, chat, joinsound FROM remembered_user_settings ORDER BY auth DESC"
    (fun r -> (r.GetString 0, r |> readStringOrNone 1, r |> readStringOrNone 2, r |> readStringOrNone 3, r |> readStringOrNone 4))