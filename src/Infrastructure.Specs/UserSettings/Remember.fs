module Infrastructure.Specs.UserSettings.Remember

open Expecto
open SqlTestUtils
open Infrastructure.SqlOperations
open Domain.ACL.UserSettings

let private teardown connection =
  async {
    do! connection |> executeSql "TRUNCATE cccm_users"
    do! connection |> executeSql "TRUNCATE joinsounds"
    do! connection |> executeSql "TRUNCATE remembered_user_settings"
  }

let private whenRememberUserSettingsFor user (given : string list) connection =
  async {
    if not given.IsEmpty then
      do! executeSql (System.String.Join(';', given |> Seq.ofList)) connection

    do! rememberUserSettingsFor user connection

    return! connection |> retrieveRememberedSettings
  }

let private thenExpectRememberedSettings expectedSettings actualSettings connection =
  async {
    let! settings = actualSettings connection
    Expect.equal settings expectedSettings "remembered user settings are not the same"
  }

let private testUser = "STEAM_0:0:123"
let private secondUser = "STEAM_0:0:124"

let tests databaseConfiguration =
  [
    testCaseAsync "remembers all colors" (
      given [ testUser |> hasCustomColors "00aaef" "12ffee" "cc0055" ]
      |> whenRememberUserSettingsFor testUser
      |> thenExpectRememberedSettings [ testUser |> withRememberedColors "00aaef" "12ffee" "cc0055" ]
      |> testSql databaseConfiguration teardown
    )

    testCaseAsync "remembers only given user" (
      given
        [ testUser |> hasCustomColors "00aaef" "12ffee" "cc0055"
          secondUser |> hasCustomColors "ffee00" "ab2fc4" "0fad50"
        ]
      |> whenRememberUserSettingsFor testUser
      |> thenExpectRememberedSettings [ testUser |> withRememberedColors "00aaef" "12ffee" "cc0055" ]
      |> testSql databaseConfiguration teardown
    )

    testCaseAsync "does nothing if user has no colors" (
      given []
      |> whenRememberUserSettingsFor testUser
      |> thenExpectRememberedSettings noRememberedSettings
      |> testSql databaseConfiguration teardown
    )

    testCaseAsync "does nothing if user has no colors but rembered colors" (
      given [ testUser |> hasRememberedColors "00aaef" "12ffee" "cc0055" ]
      |> whenRememberUserSettingsFor testUser
      |> thenExpectRememberedSettings [ testUser |> withRememberedColors "00aaef" "12ffee" "cc0055" ]
      |> testSql databaseConfiguration teardown
    )

    testCaseAsync "updates previous remembered colors" (
      given
        [ testUser |> hasCustomColors "00aaef" "12ffee" "cc0055"
          testUser |> hasRememberedColors "11f0ac" "80de12" "110022"
        ]
      |> whenRememberUserSettingsFor testUser
      |> thenExpectRememberedSettings [ testUser |> withRememberedColors "00aaef" "12ffee" "cc0055" ]
      |> testSql databaseConfiguration teardown
    )

    testCaseAsync "leaves remembered colors from other users untouched" (
      given
        [ testUser |> hasCustomColors "00aaef" "12ffee" "cc0055"
          secondUser |> hasRememberedColors "11f0ac" "80de12" "110022"
        ]
      |> whenRememberUserSettingsFor testUser
      |> thenExpectRememberedSettings
        [ testUser |> withRememberedColors "00aaef" "12ffee" "cc0055"
          secondUser |> withRememberedColors "11f0ac" "80de12" "110022"
        ]
      |> testSql databaseConfiguration teardown
    )

    testCaseAsync "remembers joinsound" (
      given
        [ testUser |> hasDefaultColors
          testUser |> hasJoinsound "test.mp3"
        ]
      |> whenRememberUserSettingsFor testUser
      |> thenExpectRememberedSettings
        [ testUser
          |> withDefaultRememberedColors
          |> andJoinsound "test.mp3"
        ]
      |> testSql databaseConfiguration teardown
    )

    testCaseAsync "updates previous remembered joinsound" (
      given
        [ testUser |> hasCustomColors "#22ee44" "#121212" "#ff33ee"
          testUser |> hasJoinsound "blub.mp3"

          testUser |> hasRememberedColors "#000000" "#000000" "#000000"
          testUser |> andRememberedJoinsound "test.mp3"
        ]
      |> whenRememberUserSettingsFor testUser
      |> thenExpectRememberedSettings
        [ testUser
          |> withRememberedColors "#22ee44" "#121212" "#ff33ee"
          |> andJoinsound "blub.mp3"
        ]
      |> testSql databaseConfiguration teardown
    )
  ]
  |> testList "Remember user settings for"
  |> testSequenced