module Infrastructure.Specs.UserSettings.JoinsoundOf

open Expecto
open SqlTestUtils
open Domain.ACL.UserSettings
open Infrastructure.SqlOperations

let private teardown connection =
  async {
    do! connection |> executeSql "TRUNCATE joinsounds"
  }

let private whenRetrieveJoinsoundOf user (given : string list) connection =
  async {
    if not given.IsEmpty then
      do! connection |> executeSql (System.String.Join (";", given))

    return! joinsoundOf (Domain.Members.Types.SteamId user) connection
  }

let private thenExpectJoinsound (expected : string option) actual connection =
  async {
    let! sound = actual connection
    Expect.equal sound expected "Joinsounds are not equal"
  }

let private testUser = "STEAM_0:0:123"

let tests databaseConfiguration =
  [
    testCaseAsync "returns file of user" (
      given [ testUser |> hasJoinsound "test.mp3"]
      |> whenRetrieveJoinsoundOf testUser
      |> thenExpectJoinsound (Some "test.mp3")
      |> testSql databaseConfiguration teardown
    )

    testCaseAsync "returns nothing if user has no joinsound" (
      given []
      |> whenRetrieveJoinsoundOf testUser
      |> thenExpectJoinsound None
      |> testSql databaseConfiguration teardown
    )
  ]
  |> testList "Joinsound of user"
  |> testSequenced