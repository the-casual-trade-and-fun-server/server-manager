module Infrastructure.Specs.UserSettings.Remove

open Expecto
open Infrastructure.SqlOperations
open Domain.ACL.UserSettings
open SqlTestUtils

let private teardown connection =
  async {
    do! connection |> executeSql "TRUNCATE cccm_users"
    do! connection |> executeSql "TRUNCATE joinsounds"
    do! connection |> executeSql "TRUNCATE joinsounds_list"
  }

let private whenRemovingUserSettingsFor user (givenSettings : string list) connection =
  async {
    if not givenSettings.IsEmpty then
      do! connection |> executeSql (System.String.Join (";", givenSettings))

    do! connection |> removeUserSettingsFor user

    let! colors = connection |> retrieveCustomChatColors
    let! joinsounds = connection |> retrieveJoinsounds
    let! definedJoinsounds = connection |> retrieveDefinedJoinsounds

    return (colors, joinsounds, definedJoinsounds)
  }

let private thenExpectUserSettings expectedCustomChatColors expectedJoinsounds expectedDefinedJoinsounds actual connection =
  async {
    let! (colors, joinsounds, definedJoinsounds) = actual connection

    Expect.equal colors expectedCustomChatColors "Custom chat colors are not the same"
    Expect.equal joinsounds expectedJoinsounds "Joinsounds are not the same"
    Expect.equal definedJoinsounds expectedDefinedJoinsounds "Defined joinsounds are not the same"
  }

let testUser = "STEAM_0:0:123"
let secondUser = "STEAM_0:0:124"

let tests databaseConfiguration =
  [
    testCaseAsync "Removes custom chat colors" (
      given [ testUser |> hasCustomColors "#123123" "#123123" "#123123" ]
      |> whenRemovingUserSettingsFor testUser
      |> thenExpectUserSettings
        noColors
        noJoinsounds
        noDefinedJoinsounds
      |> testSql databaseConfiguration teardown
    )

    testCaseAsync "Removes joinsounds and updates defined joinsound" (
      given
        [ testUser |> hasJoinsound "test.mp3"
          usedJoinsound "test.mp3"
        ]
      |> whenRemovingUserSettingsFor testUser
      |> thenExpectUserSettings
        noColors
        noJoinsounds
        [ unused "test.mp3" ]
      |> testSql databaseConfiguration teardown
    )

    testCaseAsync "Does nothing if user has no settings" (
      given []
      |> whenRemovingUserSettingsFor testUser
      |> thenExpectUserSettings
        noColors
        noJoinsounds
        noDefinedJoinsounds
      |> testSql databaseConfiguration teardown
    )

    testCaseAsync "Leaves other settings untouched" (
      given
        [ secondUser |> hasCustomColors "#123123" "#aabbcc" "#00ff00"
          secondUser |> hasJoinsound "bla.mp3"
          usedJoinsound "bla.mp3"

          testUser |> hasCustomColors "#123123" "#123123" "#123123"
          testUser |> hasJoinsound "test.mp3"
          usedJoinsound "test.mp3"
        ]
      |> whenRemovingUserSettingsFor testUser
      |> thenExpectUserSettings
        [ secondUser |> withTagColor "#123123" |> andNameColor "#aabbcc" |> andChatColor "#00ff00" ]
        [ secondUser |> withJoinsound "bla.mp3" ]
        [ used "bla.mp3"
          unused "test.mp3"
        ]
      |> testSql databaseConfiguration teardown
    )
  ]
  |> testList "Remove user settings"
  |> testSequenced