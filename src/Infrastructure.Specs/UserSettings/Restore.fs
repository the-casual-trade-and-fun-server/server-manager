module Infrastructure.Specs.UserSettings.Restore

open Expecto
open SqlTestUtils
open Infrastructure.SqlOperations
open Domain.ACL.UserSettings

let private teardown connection =
  async {
    do! connection |> executeSql "TRUNCATE cccm_users"
    do! connection |> executeSql "TRUNCATE joinsounds"
    do! connection |> executeSql "TRUNCATE joinsounds_list"
    do! connection |> executeSql "TRUNCATE remembered_user_settings"
  }

let private whenRestoreUserSettingsFor user (givenColors : string list) connection =
  async {
    if not givenColors.IsEmpty then
      do! executeSql (System.String.Join(';', givenColors |> Seq.ofList)) connection

    do! restoreUserSettingsFor user connection

    let! rememberedSettings = connection |> retrieveRememberedSettings
    let! colors = connection |> retrieveCustomChatColors
    let! joinsounds = connection |> retrieveJoinsounds
    let! definedJoinsounds = connection |> retrieveDefinedJoinsounds

    return (colors, rememberedSettings, joinsounds, definedJoinsounds)
  }

let private thenExpect expectedColors expectedJoinsounds expectedJoinsoundList expectedRememberedSettings actualSettings connection =
  async {
    let! (colors, rememberedSettings, joinsounds, joinsoundList) = actualSettings connection
    Expect.equal colors expectedColors "custom chat colors are not the same"
    Expect.equal joinsounds expectedJoinsounds "joinsounds are not the same"
    Expect.equal joinsoundList expectedJoinsoundList "joinsound list are not the same"
    Expect.equal rememberedSettings expectedRememberedSettings "remembered user settings are not the same"
  }

let private testUser = "STEAM_0:0:123"
let private secondUser = "STEAM_0:0:124"

let tests databaseConfiguration =
  [
    testCaseAsync "restores all colors and removes remembered settings" (
      given [ testUser |> hasRememberedColors "#00aaef" "#12ffee" "#cc0055" ]
      |> whenRestoreUserSettingsFor testUser
      |> thenExpect
        [ testUser
          |> withTagColor "#00aaef"
          |> andNameColor "#12ffee"
          |> andChatColor "#cc0055"
        ]
        noJoinsounds
        noDefinedJoinsounds
        noRememberedSettings
      |> testSql databaseConfiguration teardown
    )

    testCaseAsync "restores all colors and removes only remembered settings of given user" (
      given
        [ testUser |> hasRememberedColors "#00aaef" "#12ffee" "#cc0055"
          secondUser |> hasRememberedColors "#aabbcc" "#001122" "#33aa44"
        ]
      |> whenRestoreUserSettingsFor testUser
      |> thenExpect
        [ testUser
          |> withTagColor "#00aaef"
          |> andNameColor "#12ffee"
          |> andChatColor "#cc0055"
        ]
        noJoinsounds
        noDefinedJoinsounds
        [ secondUser |> withRememberedColors "#aabbcc" "#001122" "#33aa44" ]
      |> testSql databaseConfiguration teardown
    )

    testCaseAsync "restores colors only for given user" (
      given
        [ testUser |> hasCustomColors "#00aaef" "#12ffee" "#cc0055"
          secondUser |> hasRememberedColors "#aabbcc" "#001122" "#33aa44"
        ]
      |> whenRestoreUserSettingsFor secondUser
      |> thenExpect
        [ testUser
          |> withTagColor "#00aaef"
          |> andNameColor "#12ffee"
          |> andChatColor "#cc0055"

          secondUser
          |> withTagColor "#aabbcc"
          |> andNameColor "#001122"
          |> andChatColor "#33aa44"
        ]
        noJoinsounds
        noDefinedJoinsounds
        noRememberedSettings
      |> testSql databaseConfiguration teardown
    )

    testCaseAsync "restores joinsound, sets joinsound to used and removes remembered settings" (
      given
        [ testUser |> hasRememberedColors "#1133ee" "#aabbcc" "#00ff00"
          testUser |> andRememberedJoinsound "test.mp3"
          unusedJoinsound "test.mp3"
        ]
      |> whenRestoreUserSettingsFor testUser
      |> thenExpect
        [ testUser
          |> withTagColor "#1133ee"
          |> andNameColor "#aabbcc"
          |> andChatColor "#00ff00"
        ]
        [ testUser |> withJoinsound "test.mp3" ]
        [ used "test.mp3" ]
        noRememberedSettings
      |> testSql databaseConfiguration teardown
    )

    testCaseAsync "does not restores joinsound when joinsound is used, but restores colors" (
      given
        [ testUser |> hasRememberedColors "#1133ee" "#aabbcc" "#00ff00"
          testUser |> andRememberedJoinsound "test.mp3"

          secondUser |> hasJoinsound "test.mp3"
          usedJoinsound "test.mp3"
        ]
      |> whenRestoreUserSettingsFor testUser
      |> thenExpect
        [ testUser
          |> withTagColor "#1133ee"
          |> andNameColor "#aabbcc"
          |> andChatColor "#00ff00"
        ]
        [ secondUser |> withJoinsound "test.mp3" ]
        [ used "test.mp3" ]
        noRememberedSettings
      |> testSql databaseConfiguration teardown
    )

    testCaseAsync "does not restores joinsound when joinsound is not defined, but restores colors" (
      given
        [ testUser |> hasRememberedColors "#1133ee" "#aabbcc" "#00ff00"
          testUser |> andRememberedJoinsound "test.mp3"
        ]
      |> whenRestoreUserSettingsFor testUser
      |> thenExpect
        [ testUser
          |> withTagColor "#1133ee"
          |> andNameColor "#aabbcc"
          |> andChatColor "#00ff00"
        ]
        noJoinsounds
        noDefinedJoinsounds
        noRememberedSettings
      |> testSql databaseConfiguration teardown
    )

    testCaseAsync "leaves other joinsounds untouched" (
      given
        [ testUser |> hasRememberedColors "#1133ee" "#aabbcc" "#00ff00"
          testUser |> andRememberedJoinsound "test.mp3"
          unusedJoinsound "test.mp3"

          secondUser |> hasJoinsound "bla.mp3"
          usedJoinsound "bla.mp3"

          unusedJoinsound "blub.mp3"
        ]
      |> whenRestoreUserSettingsFor testUser
      |> thenExpect
        [ testUser
          |> withTagColor "#1133ee"
          |> andNameColor "#aabbcc"
          |> andChatColor "#00ff00"
        ]
        [ testUser |> withJoinsound "test.mp3"
          secondUser |> withJoinsound "bla.mp3"
        ]
        [ used "bla.mp3"
          unused "blub.mp3"
          used "test.mp3"
        ]
        noRememberedSettings
      |> testSql databaseConfiguration teardown
    )

    testCaseAsync "does nothing is given user has no remembered settings" (
      given [ secondUser |> hasRememberedColors "#aabbcc" "#001122" "#33aa44" ]
      |> whenRestoreUserSettingsFor testUser
      |> thenExpect
        noColors
        noJoinsounds
        noDefinedJoinsounds
        [ secondUser |> withRememberedColors "#aabbcc" "#001122" "#33aa44" ]
      |> testSql databaseConfiguration teardown
    )
  ]
  |> testList "Restore user settings for"
  |> testSequenced