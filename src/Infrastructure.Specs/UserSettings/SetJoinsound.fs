module Infrastructure.Specs.UserSettings.SetJoinsound

open Expecto
open SqlTestUtils
open Domain.Members.Types
open Domain.ACL.UserSettings
open Infrastructure.SqlOperations

let private teardown connection =
  async {
    do! connection |> executeSql "TRUNCATE joinsounds"
    do! connection |> executeSql "TRUNCATE joinsounds_list"
  }

let private setJoinsoundCommand user sound : SetJoinsoundCommand =
  { User = SteamId user
    Sound = sound
  }

let private whenSettingJoinsound sound user (givenJoinsounds : string list) connection =
  async {
    if not givenJoinsounds.IsEmpty then
      do! connection |> executeSql (System.String.Join (";", givenJoinsounds))

    do! handleSetJoinsound (setJoinsoundCommand user sound) connection

    let! joinsounds = connection |> retrieveJoinsounds
    let! definedJoinsounds = connection |> retrieveDefinedJoinsounds

    return (joinsounds, definedJoinsounds)
  }

let private thenExpectJoinsounds expectedJoinsounds expectedDefinedJoinsounds actualSounds connection =
  async {
    let! (joinsounds, definedJoinsounds) = actualSounds connection

    Expect.equal joinsounds expectedJoinsounds "Joinsounds are not the same"
    Expect.equal definedJoinsounds expectedDefinedJoinsounds "Defined joinsounds are not the same"
  }

let private testUser = "STEAM_0:0:123"
let private secondTestUser = "STEAM_0:0:124"

let tests databaseConfiguration =
  [
    testCaseAsync "Sets joinsound for user and change sound to used" (
      given [ unusedJoinsound "test.mp3" ]
      |> whenSettingJoinsound "test.mp3" testUser
      |> thenExpectJoinsounds
        [ testUser |> withJoinsound "test.mp3" ]
        [ used "test.mp3" ]
      |> testSql databaseConfiguration teardown
    )

    testCaseAsync "don't sets joinsound if sound is not defined" (
      given []
      |> whenSettingJoinsound "test.mp3" testUser
      |> thenExpectJoinsounds
        noJoinsounds
        noDefinedJoinsounds
      |> testSql databaseConfiguration teardown
    )

    testCaseAsync "don't sets joinsound if sound is already used" (
      given [ usedJoinsound "test.mp3" ]
      |> whenSettingJoinsound "test.mp3" testUser
      |> thenExpectJoinsounds
        noJoinsounds
        [ used "test.mp3" ]
      |> testSql databaseConfiguration teardown
    )

    testCaseAsync "overrides joinsounds and sets old sound to unused" (
      given
        [ testUser |> hasJoinsound "test.mp3"
          usedJoinsound "test.mp3"
          unusedJoinsound "bla.mp3"
        ]
      |> whenSettingJoinsound "bla.mp3" testUser
      |> thenExpectJoinsounds
        [ testUser |> withJoinsound "bla.mp3" ]
        [ used "bla.mp3"
          unused "test.mp3"
        ]
      |> testSql databaseConfiguration teardown
    )

    testCaseAsync "leaves other joinsounds untouched" (
      given
        [ secondTestUser |> hasJoinsound "test.mp3"
          usedJoinsound "test.mp3"
          unusedJoinsound "bla.mp3"
          unusedJoinsound "blub.mp3"
        ]
      |> whenSettingJoinsound "bla.mp3" testUser
      |> thenExpectJoinsounds
        [ testUser |> withJoinsound "bla.mp3"
          secondTestUser |> withJoinsound "test.mp3"
        ]
        [ used "bla.mp3"
          unused "blub.mp3"
          used "test.mp3"
        ]
      |> testSql databaseConfiguration teardown
    )
  ]
  |> testList "Set joinsound for vip"
  |> testSequenced