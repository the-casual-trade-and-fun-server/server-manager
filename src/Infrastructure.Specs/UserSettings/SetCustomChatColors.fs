module Infrastructure.Specs.UserSettings.SetCustomChatColor

open Expecto
open SqlTestUtils
open Infrastructure.SqlOperations
open Domain.Specs.Members.TestHelpers
open Domain.ACL.UserSettings

let private teardown connection =
  async {
    do! connection |> executeSql "TRUNCATE cccm_users"
  }

let private customTagColor color : CustomChatColors =
  { Tag = Some color
    Name = None
    Chat = None
  }
let private defaultColors : CustomChatColors =
  { Tag = None
    Name = None
    Chat = None
  }
let private withNameColor color customChatColors : CustomChatColors =
  { customChatColors with Name = Some color }
let private withChatColor color customChatColors : CustomChatColors =
  { customChatColors with Chat = Some color }

let private setCustomChatColorsCommand user colors : SetCustomChatColorsCommand =
  { User = steamId user
    Colors = colors
  }

let private whenSettingCustomChatColors user colors (givenColors : string list) connection =
  async {
    if not givenColors.IsEmpty then
      do! executeSql (System.String.Join(';', givenColors |> Seq.ofList)) connection

    do! handleSetCustomChatColors (setCustomChatColorsCommand user colors) connection

    return! connection |> retrieveCustomChatColors
  }

let private thenExpect expectedColors actualColors connection =
  async {
    let! colors = actualColors connection
    Expect.equal colors expectedColors "Custom Chat Colors differ"
  }

let private testUser = "STEAM_0:0:1"
let private secondTestUser = "STEAM_0:0:2"

let tests databaseConfiguration =
  [
    testCaseAsync "saves custom chat colors and removes '#' - without previous setting" (
      given []
      |> whenSettingCustomChatColors testUser
        (customTagColor "#aaaaaa"
        |> withNameColor "#bbbbbb"
        |> withChatColor "#cccccc")
      |> thenExpect
        [ testUser
          |> withTagColor "aaaaaa"
          |> andNameColor "bbbbbb"
          |> andChatColor "cccccc"
        ]
      |> testSql databaseConfiguration teardown
    )

    testCaseAsync "saves custom chat colors and removes '#' - overrides previous setting" (
      given
        [ testUser |> hasCustomColors "111111" "222222" "333333" ]
      |> whenSettingCustomChatColors testUser
        (customTagColor "#aaaaaa"
        |> withNameColor "#bbbbbb"
        |> withChatColor "#cccccc")
      |> thenExpect
        [ testUser
          |> withTagColor "aaaaaa"
          |> andNameColor "bbbbbb"
          |> andChatColor "cccccc"
        ]
      |> testSql databaseConfiguration teardown
    )

    testCaseAsync "without previous setting - only tag color" (
      given noColors
      |> whenSettingCustomChatColors testUser (customTagColor "#aaaaaa")
      |> thenExpect [ testUser |> withTagColor "aaaaaa" ]
      |> testSql databaseConfiguration teardown
    )

    testCaseAsync "overrides previous setting - only tag color" (
      given [ testUser |> hasCustomTagColor "cfcfcf" ]
      |> whenSettingCustomChatColors testUser (customTagColor "#bbaabb")
      |> thenExpect [ testUser |> withTagColor "bbaabb" ]
      |> testSql databaseConfiguration teardown
    )

    testCaseAsync "without previous setting - only name color" (
      given noColors
      |> whenSettingCustomChatColors testUser (defaultColors |> withNameColor "#aaaaaa")
      |> thenExpect [ testUser |> withNoColors |> andNameColor "aaaaaa" ]
      |> testSql databaseConfiguration teardown
    )

    testCaseAsync "overrides previous setting - only name color" (
      given [ testUser |> hasCustomNameColor "cfcfcf" ]
      |> whenSettingCustomChatColors testUser (defaultColors |> withNameColor "#bbaabb")
      |> thenExpect [ testUser |> withNoColors |> andNameColor "bbaabb" ]
      |> testSql databaseConfiguration teardown
    )

    testCaseAsync "without previous setting - only chat color" (
      given noColors
      |> whenSettingCustomChatColors testUser (defaultColors |> withChatColor "#aaaaaa")
      |> thenExpect [ testUser |> withNoColors |> andChatColor "aaaaaa" ]
      |> testSql databaseConfiguration teardown
    )

    testCaseAsync "overrides previous setting - only chat color" (
      given [ testUser |> hasCustomChatColor "cfcfcf" ]
      |> whenSettingCustomChatColors testUser (defaultColors |> withChatColor "#bbaabb")
      |> thenExpect [ testUser |> withNoColors |> andChatColor "bbaabb" ]
      |> testSql databaseConfiguration teardown
    )

    testCaseAsync "has no tag color if no color is given" (
      given [ testUser |> hasCustomTagColor "ababab" ]
      |> whenSettingCustomChatColors testUser (defaultColors)
      |> thenExpect [ testUser |> withNoColors ]
      |> testSql databaseConfiguration teardown
    )

    testCaseAsync "has no name color if no color is given" (
      given [ testUser |> hasCustomChatColor "ababab" ]
      |> whenSettingCustomChatColors testUser defaultColors
      |> thenExpect [ testUser |> withNoColors ]
      |> testSql databaseConfiguration teardown
    )

    testCaseAsync "has no chat color if no color is given" (
      given [ testUser |> hasCustomChatColor "#ababab" ]
      |> whenSettingCustomChatColors testUser defaultColors
      |> thenExpect [ testUser |> withNoColors ]
      |> testSql databaseConfiguration teardown
    )

    testCaseAsync "leaves other settings untouched" (
      given
        [ testUser |> hasCustomChatColor "ffffff"
          secondTestUser |> hasCustomColors "22fe11" "ff00aa" "dedede"
        ]
        |> whenSettingCustomChatColors testUser defaultColors
        |> thenExpect
          [ testUser |> withNoColors
            secondTestUser
            |> withTagColor "22fe11"
            |> andNameColor "ff00aa"
            |> andChatColor "dedede"
          ]
        |> testSql databaseConfiguration teardown
    )
  ]
  |> testList "Set custom-chat-colors"
  |> testSequenced