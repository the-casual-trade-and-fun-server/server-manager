module Infrastructure.Specs.UserSettings.UseNoJoinsound

open Expecto
open Domain.ACL.UserSettings
open Infrastructure.SqlOperations
open SqlTestUtils

let private teardown connection =
  async {
    do! connection |> executeSql "TRUNCATE joinsounds"
    do! connection |> executeSql "TRUNCATE joinsounds_list"
  }

let private useNoJoinsoundCommand user : UseNoJoinsoundCommand =
  { User = Domain.Members.Types.SteamId user }

let private whenUsingNoJoinsound user (givenSql : string list) connection =
  async {
    if not givenSql.IsEmpty then
      do! connection |> executeSql (System.String.Join (";", givenSql))

    do! handleUseNoJoinsound (useNoJoinsoundCommand user) connection

    let! joinsounds = retrieveJoinsounds connection
    let! sounds = retrieveDefinedJoinsounds connection
    return (joinsounds, sounds)
  }

let private thenExpect expectedJoinsounds expectedSounds actual connection =
  async {
    let! (joinsounds, sounds) = actual connection

    Expect.equal joinsounds expectedJoinsounds "Joinsounds are not equal"
    Expect.equal sounds expectedSounds "Defined sounds are not equal"
  }

let private testUser = "STEAM_0:0:123"
let private secondUser = "STEAM_0:0:124"

let tests databaseConfiguration =
  [
    testCaseAsync "Remove joinsound and set sound to unused" (
      given
        [ testUser |> hasJoinsound "test.mp3"
          usedJoinsound "test.mp3"
        ]
      |> whenUsingNoJoinsound testUser
      |> thenExpect
        []
        [ unused "test.mp3" ]
      |> testSql databaseConfiguration teardown
    )

    testCaseAsync "Does nothing if user has no joinsound" (
      given
        [ secondUser |> hasJoinsound "bla.mp3"
          usedJoinsound "bla.mp3"
        ]
      |> whenUsingNoJoinsound testUser
      |> thenExpect
        [secondUser |> withJoinsound "bla.mp3" ]
        [ used "bla.mp3"]
      |> testSql databaseConfiguration teardown
    )
  ]
  |> testList "Use no joinsound"
  |> testSequenced