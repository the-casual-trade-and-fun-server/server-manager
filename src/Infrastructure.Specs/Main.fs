﻿module Infrastructure.Specs.Main

open Infrastructure.Configuration
open Infrastructure.SqlOperations
open Expecto.Tests

let tests databaseConfiguration =
  testList "Infrastructure"
    [ testList "EventStore"
        [ EventStore.Access.tests databaseConfiguration
          EventStore.Persistor.tests databaseConfiguration
        ]

      testList "Projection"
        [ Projection.MembersForSourceModAdmins.tests databaseConfiguration ]

      testList "User settings"
        [ UserSettings.JoinsoundOf.tests databaseConfiguration
          UserSettings.DefinedJoinsounds.tests databaseConfiguration
          UserSettings.CustomChatColorsOf.tests databaseConfiguration
          UserSettings.SetCustomChatColor.tests databaseConfiguration
          UserSettings.SetJoinsound.tests databaseConfiguration
          UserSettings.UseNoJoinsound.tests databaseConfiguration
          UserSettings.Remember.tests databaseConfiguration
          UserSettings.Restore.tests databaseConfiguration
          UserSettings.Remove.tests databaseConfiguration
        ]
    ]

let private setup databaseConfiguration =
  async {
    use! connection = openConnection databaseConfiguration
    do!
      connection
      |> executeSql "CREATE TABLE IF NOT EXISTS EventStore (recordDate datetime, recordOrder tinyint(4), event text)"

    do!
      connection
      |> executeSql
        "CREATE TABLE IF NOT EXISTS cccm_users (
          id int(64) NOT NULL PRIMARY KEY AUTO_INCREMENT,
          auth varchar(60) UNIQUE,
          hidetag varchar(1),
          tagcolor varchar(7) NULL,
          namecolor varchar(7) NULL,
          chatcolor varchar(7) NULL)"

    do!
      connection
      |> executeSql
        "CREATE TABLE IF NOT EXISTS remembered_user_settings (
          auth varchar(60) NOT NULL PRIMARY KEY,
          tag varchar(7) NULL,
          name varchar(7) NULL,
          chat varchar(7) NULL,
          joinsound varchar(60) NULL)"

    do!
      connection
      |> executeSql
        "CREATE TABLE IF NOT EXISTS joinsounds (
          id int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
          name varchar(256) NOT NULL,
          steamid varchar(256) NOT NULL,
          file varchar(256) UNIQUE NOT NULL)"

    do!
      connection
      |> executeSql
        "CREATE TABLE IF NOT EXISTS joinsounds_list (
          id int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
          name varchar(256) NOT NULL,
          file varchar(60) UNIQUE NOT NULL,
          used tinyint(1) NOT NULL)"

    do!
      connection
      |> executeSql
        "CREATE TABLE IF NOT EXISTS members (
          id int(10) unsigned NOT NULL AUTO_INCREMENT KEY,
          identity varchar(65) NOT NULL,
          immunity int(10) unsigned NOT NULL,
          group_id int(10) unsigned NOT NULL,
          receivedOn datetime NULL,
          duration int(32) unsigned NULL,
          expiresAt datetime NULL,
          pausedSince datetime NULL
        );"
  }

let private databaseConfiguration host password : DatabaseConnectionInformation =
  {
    host = host
    user = "test"
    password = password
    database = "testDB"
    port = 3306
  }

let private environmentVar name =
  let var = System.Environment.GetEnvironmentVariable name
  if System.String.IsNullOrEmpty var then
    None
  else
    Some var

[<EntryPoint>]
let main args =
  let dbHost =
    environmentVar "MYSQL_HOST"
    |> Option.defaultValue "127.0.0.1"

  let dbPassword =
    environmentVar "MYSQL_PASSWORD"
    |> Option.defaultValue ""

  let dbConf =
    databaseConfiguration dbHost dbPassword

  setup dbConf
  |> Async.RunSynchronously

  runTestsWithCLIArgs
    []
    args
    (tests dbConf)
