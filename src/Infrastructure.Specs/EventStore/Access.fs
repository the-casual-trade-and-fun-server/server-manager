module Infrastructure.Specs.EventStore.Access

open Expecto.Tests
open Expecto.Expect
open Infrastructure.EventStore.Access
open Infrastructure.SqlOperations
open Infrastructure.Configuration
open Domain.Specs.Members.TestHelpers

let private tearDown databaseConfiguration =
  async {
    use! connection = openConnection databaseConfiguration
    do!
      connection
      |> executeSql "DELETE FROM EventStore"
  }

let private testEventStore databaseConfiguration testCase = async {
  try
    match startEventStore databaseConfiguration with
    | Ok (eventStore, observer) ->
        testCase eventStore observer

    | Error err ->
        failtest err

  finally
    tearDown databaseConfiguration |> Async.RunSynchronously
}

let private testEventStoreWithoutDatabase testCase = async {
  let dbConfigToNonAvailableHost : DatabaseConnectionInformation =
    {
      host = "127.0.0.0"
      port = 3000
      database = "NotAvailable"
      user = "nonExisting"
      password = "wrong"
    }

  let observer = new Event<Domain.Members.Events.Events>()

  let eventStore = initEventStore dbConfigToNonAvailableHost observer.Trigger []
  testCase eventStore observer.Publish
}

let private failOnError maybeError =
  match maybeError with
  | Some err -> failtest err
  | None -> ()

let private equalAsResult actual expected message =
  try
    equal actual expected message
    Ok []

  with e ->
    Error e.Message

let tests databaseConfiguration =
  [
    testCaseAsync "effect - single event"
      <| testEventStore databaseConfiguration (fun eventStore _ ->
        let testUser = steamId "test"

        eventStore
        |> effect (fun _ -> [ testUser |> wasAddedAsMemberWithRank ownerRank ])
        |> failOnError

        let storedEvents =
          eventStore
          |> getEvents

        equal
          storedEvents
          [ testUser |> wasAddedAsMemberWithRank ownerRank ]
          "should equal"
      )

    testCaseAsync "effect - multiple events"
      <| testEventStore databaseConfiguration (fun eventStore _ ->
        let testUser = steamId "test"

        let eventsToSave =
          [
            testUser |> wasAddedAsMemberWithRank ownerRank
            testUser |> rankWasSettedTo adminRank
            testUser |> wasRemovedFromMembers
          ]

        eventStore
        |> effect (fun _ -> eventsToSave)
        |> failOnError

        let storedEvents =
          eventStore
          |> getEvents

        equal
          storedEvents
          [
            testUser |> wasAddedAsMemberWithRank ownerRank
            testUser |> rankWasSettedTo adminRank
            testUser |> wasRemovedFromMembers
          ]
          "should equal"
      )

    testCaseAsync "multiple effects - single event"
      <| testEventStore databaseConfiguration (fun eventStore _ ->
        let firstUser = steamId "test1"
        let secondUser = steamId "test2"

        eventStore
        |> effect (fun _ -> [ firstUser |> wasAddedAsMemberWithRank adminRank ])
        |> failOnError

        eventStore
        |> effect (fun _ -> [ secondUser |> wasAddedAsMemberWithRank mapperRank ])
        |> failOnError

        let storedEvents =
          eventStore
          |> getEvents

        equal
          storedEvents
          [
            firstUser |> wasAddedAsMemberWithRank adminRank
            secondUser |> wasAddedAsMemberWithRank mapperRank
          ]
          "should equal"
      )

    testCaseAsync "multiple effects - multiple events"
      <| testEventStore databaseConfiguration (fun eventStore _ ->
        let firstUser = steamId "test1"
        let secondUser = steamId "test2"

        let eventsForFirstUser =
          [
            firstUser |> wasAddedAsMemberWithRank moderatorRank
            firstUser |> rankWasSettedTo adminRank
            firstUser |> wasRemovedFromMembers
          ]

        eventStore
        |> effect (fun _ -> eventsForFirstUser)
        |> failOnError

        let startDate = date 20 01 2018
        let duration = durationFromDays 5
        let extraDuration = durationFromDays 10
        let eventsForSecondUser =
          [
            secondUser |> receivedVIP startDate duration
            secondUser |> durationWasExtendedBy extraDuration
          ]

        eventStore
        |> effect (fun _ -> eventsForSecondUser)
        |> failOnError

        let storedEvents =
          eventStore
          |> getEvents

        equal
          storedEvents
          [
            firstUser |> wasAddedAsMemberWithRank moderatorRank
            firstUser |> rankWasSettedTo adminRank
            firstUser |> wasRemovedFromMembers
            secondUser |> receivedVIP startDate duration
            secondUser |> durationWasExtendedBy extraDuration
          ]
          "should be equal"
      )

    testCaseAsync "history in effects is correct"
      <| testEventStore databaseConfiguration (fun eventStore _ ->
        let testUser = steamId "test"

        let events =
          [ testUser |> wasAddedAsMemberWithRank ownerRank ]

        eventStore
        |> effect (fun _ -> events)
        |> failOnError

        let mutable historyFromEffect = None

        eventStore
        |> effect (fun history -> historyFromEffect <- Some history; [])
        |> failOnError

        equal historyFromEffect (Some events) "history is not correct"
      )

    testCaseAsync "observer will be called after storing events"
      <| testEventStore databaseConfiguration (fun eventStore observer ->
        let testUser = steamId "test"

        let events =
          [ testUser |> wasAddedAsMemberWithRank ownerRank ]

        let mutable callbackCalled = false

        use subscription =
          observer
          |> Observable.subscribe (fun _ -> callbackCalled <- true)

        eventStore
        |> effect (fun _ -> events)
        |> failOnError

        isTrue callbackCalled "Callback should be called after storing"
      )

    testCaseAsync "observer will be called after storing events - history is correct"
      <| testEventStore databaseConfiguration (fun eventStore observer ->
        let testUser = steamId "test"

        let events =
          [ testUser |> wasAddedAsMemberWithRank ownerRank ]

        let mutable historyFromCallback = None

        use subscription =
          observer
          |> Observable.subscribe (fun history -> historyFromCallback <- Some history)

        eventStore
        |> effect (fun _ -> events)
        |> failOnError

        equal historyFromCallback (Some events) "history from observer is wrong"
      )

    testCaseAsync "effect without SQL connection fails"
      <| testEventStoreWithoutDatabase (fun eventStore _ ->
        let testUser = steamId "test"

        let events =
          [ testUser |> wasAddedAsMemberWithRank ownerRank ]

        eventStore
        |> effect (fun _ -> events)
        |> Expecto.Flip.Expect.isSome "effect should return error"
      )

    testCaseAsync "effect without SQL connection - events are not in history"
      <| testEventStoreWithoutDatabase (fun eventStore _ ->
        let testUser = steamId "test"

        let events =
          [ testUser |> wasAddedAsMemberWithRank ownerRank ]

        eventStore
        |> effect (fun _ -> events)
        |> ignore

        eventStore
        |> getEvents
        |> (fun history -> equal history [] "non stored events should not be in history")
      )

    testCaseAsync "effect without SQL connection - callback won't be called"
      <| testEventStoreWithoutDatabase (fun eventStore observer ->
        let testUser = steamId "test"

        let events =
          [ testUser |> wasAddedAsMemberWithRank ownerRank ]

        let mutable callbackCalled = false

        use subscription =
          observer
          |> Observable.subscribe (fun _ -> System.Console.WriteLine "Yes"; callbackCalled <- true)

        eventStore
        |> effect (fun _ -> events)
        |> ignore

        isFalse callbackCalled "callback should not be called when storing failed"
      )

    testCaseAsync "tryEffect returns error"
      <| testEventStore databaseConfiguration (fun eventStore _ ->
        let errorMessage = "fail on purpose"
        eventStore
        |> tryEffect (fun _ -> Error errorMessage)
        |> fun result -> equal result (Some errorMessage) "error should be returned"
      )

    testCaseAsync "history in tryEffect is correct"
      <| testEventStore databaseConfiguration (fun eventStore _ ->
        let testUser = steamId "test"

        let events =
          [ testUser |> wasAddedAsMemberWithRank ownerRank ]

        eventStore
        |> tryEffect (fun _ -> Ok events)
        |> failOnError

        eventStore
        |> tryEffect (fun history -> equalAsResult history events "history is not correct")
        |> failOnError
      )

    testCaseAsync "observer will be called after storing events from tryEffect"
      <| testEventStore databaseConfiguration (fun eventStore observer ->
        let testUser = steamId "test"

        let events =
          [ testUser |> wasAddedAsMemberWithRank ownerRank ]

        let mutable callbackCalled = false

        use subscription =
          observer
          |> Observable.subscribe (fun _ -> callbackCalled <- true)

        eventStore
        |> tryEffect (fun _ -> Ok events)
        |> failOnError

        isTrue callbackCalled "Callback should be called after storing"
      )

    testCaseAsync "observer will be called after storing events from tryEffect - history is correct"
      <| testEventStore databaseConfiguration (fun eventStore observer ->
        let testUser = steamId "test"

        let events =
          [ testUser |> wasAddedAsMemberWithRank ownerRank ]

        let mutable historyFromCallback = None

        use subscription =
          observer
          |> Observable.subscribe (fun history -> historyFromCallback <- Some history)

        eventStore
        |> tryEffect (fun _ -> Ok events)
        |> failOnError

        equal historyFromCallback (Some events) "history from observer is wrong"
      )

    testCaseAsync "tryEffect without SQL connection fails"
      <| testEventStoreWithoutDatabase (fun eventStore _ ->
        let testUser = steamId "test"

        let events =
          [ testUser |> wasAddedAsMemberWithRank ownerRank ]

        eventStore
        |> tryEffect (fun _ -> Ok events)
        |> Expecto.Flip.Expect.isSome "effect should return error"
      )

    testCaseAsync "tryEffect without SQL connection - events are not in history"
      <| testEventStoreWithoutDatabase (fun eventStore _ ->
        let testUser = steamId "test"

        let events =
          [ testUser |> wasAddedAsMemberWithRank ownerRank ]

        eventStore
        |> tryEffect (fun _ -> Ok events)
        |> ignore

        eventStore
        |> getEvents
        |> (fun history -> equal history [] "non stored events should not be in history")
      )

    testCaseAsync "tryEffect without SQL connection - callback won't be called"
      <| testEventStoreWithoutDatabase (fun eventStore observer ->
        let testUser = steamId "test"

        let events =
          [ testUser |> wasAddedAsMemberWithRank ownerRank ]

        let mutable callbackCalled = false

        use subscription =
          observer
          |> Observable.subscribe (fun _ -> System.Console.WriteLine "Yes"; callbackCalled <- true)

        eventStore
        |> tryEffect (fun _ -> Ok events)
        |> ignore

        isFalse callbackCalled "callback should not be called when storing failed"
      )
  ]
  |> testList "Access"
  |> testSequenced