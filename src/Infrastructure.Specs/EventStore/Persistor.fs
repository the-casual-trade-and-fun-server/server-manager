module Infrastructure.Specs.EventStore.Persistor

open Expecto
open Domain.Specs.Members.TestHelpers
open Infrastructure.SqlOperations
open Infrastructure.EventStore.Persistor
open Expecto.Flip.Expect

let private tearDown databaseConfiguration =
  async {
    use! connection = openConnection databaseConfiguration
    do!
      connection
      |> executeSql "DELETE FROM EventStore"
  }

let private testEventStorePersistor databaseConfiguration testCase = async {
  try
    testCase databaseConfiguration

  finally
    tearDown databaseConfiguration |> Async.RunSynchronously
}

let private failOnError maybeError =
  match maybeError with
  | Some err -> failtest err
  | None -> ()

let singleEventStoredTests databaseConfiguration =
  let testUser = steamId "STEAM_0:0:177249907"
  let testTarget = steamId "STEAM_0:1:184923123"
  let duration = durationFromDays 20
  let startDate = date 20 05 2018
  [
    ("member was added", testUser |> wasAddedAsMemberWithRank ownerRank)
    ("rank was setted", testUser |> rankWasSettedTo mapperRank)
    ("removed form members", testUser |> wasRemovedFromMembers)
    ("recieved VIP", testUser |> receivedVIP startDate duration)
    ("removed due to rule breaking", testUser |> wasRemovedDueToRuleBreaking)
    ("duration was corrected", testUser |> durationWasCorrectedTo duration)
    ("duration was extended", testUser |> durationWasExtendedBy duration)
    ("VIP transferred time", testUser |> transferredTimeTo testTarget duration)
    ("VIP paused", testUser |> pausedVIPOn startDate)
    ("VIP resumed", testUser |> resumedVIPOn startDate)
  ]
  |> List.map (fun (testName, event) ->
    testCaseAsync testName
      <| testEventStorePersistor databaseConfiguration (fun dbConfig ->
        let events =
          [ event ]

        store dbConfig events
        |> failOnError

        getAllEvents dbConfig
        |> function
        | Ok history ->
            equal "should equal" history events

        | Error err ->
            failtest err
      )
    )
  |> testList "Store single event"
  |> testSequenced

let tests databaseConfiguration =
  [
    singleEventStoredTests databaseConfiguration
  ]
  |> testList "Persistor"
  |> testSequenced