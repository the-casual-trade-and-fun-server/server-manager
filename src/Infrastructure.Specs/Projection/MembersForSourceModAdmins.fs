module Infrastructure.Specs.Projection.MembersForSourceModAdmins

open Expecto
open Expecto.Flip
open Domain.Specs.Members.TestHelpers
open Domain.Members.Types
open Domain.ACL.MembersForSourceModAdmins
open Infrastructure.SqlOperations

let private given = id

let private utcDate day month year hours minutes =
  System.DateTime(year, month, day, hours, minutes, 0, System.DateTimeKind.Utc)

let private teardown databaseConfiguration =
  async {
    use! connection = openConnection databaseConfiguration
    do! connection |> executeSql "TRUNCATE members"
  }

type private MemberInDatabase =
  {
    User : string
    Immunity : uint32
    Group : uint32
    ReceivedOn : System.DateTime option
    Duration : uint32 option
    ExpirationDate : System.DateTime option
    PausedSince : System.DateTime option
  }

type private TestResult =
  {
    Admins : MemberInDatabase list
    GroupLinks : (int * int) list
  }

let private configuredAdminGroups : Infrastructure.Configuration.SourceModAdminGroups =
  {
    Owner = 1u
    Admin = 2u
    Moderator = 3u
    Mapper = 4u
    VIP = 5u
  }

let private dateTimeOrNone index (reader : System.Data.Common.DbDataReader) =
  if reader.IsDBNull index then None else reader.GetDateTime index |> Some

let private readSourceModAdmin (reader : System.Data.Common.DbDataReader) : MemberInDatabase =
  {
    User = reader.GetString 0
    Immunity = reader.GetInt32 1 |> uint32
    Group = reader.GetInt32 2 |> uint32
    ReceivedOn = dateTimeOrNone 3 reader
    Duration = if reader.IsDBNull 4 then None else reader.GetInt32 4 |> uint32 |> Some
    ExpirationDate = dateTimeOrNone 5 reader
    PausedSince = dateTimeOrNone 6 reader
  }

let private whenProject databaseConfiguration events =
  async {
    try
      use! connection = openConnection databaseConfiguration

      let! error =
        projectMembersIntoDatabase
          connection
          configuredAdminGroups
          events
      error |> Option.iter failtest

      return!
        connection |> queryData "SELECT identity, immunity, group_id, receivedOn, duration, expiresAt, pausedSince FROM members ORDER BY id DESC" readSourceModAdmin

    finally
      teardown databaseConfiguration |> Async.RunSynchronously
  }

let private previousProjection databaseConfiguration events =
  async {
    use! connection = openConnection databaseConfiguration
    let! error =
      projectMembersIntoDatabase
        connection
        configuredAdminGroups
        events
    error |> Option.iter failtest
  }

let private asyncDo (func : 'a -> unit) (a : Async<'a>) : Async<'a> =
  async {
    let! result = a
    result |> func
    return result
  }

let private expectAdmins admins result =
  result |> asyncDo (Expect.equal "Admins should equal" admins) |> Async.Ignore

let private ownerImmunity = 99u
let private adminImmunity = 20u
let private moderatorImmunity = 10u
let private mapperImmunity = 0u
let private vipImmunity = 0u

let private testUser = steamId "STEAM_0:0:123"
let private secondUser = steamId "STEAM_0:0:321"

let tests databaseConfiguration =
  [
    testCaseAsync "user as member with rank 'owner'" (
      given [ testUser |> wasAddedAsMemberWithRank ownerRank ]
      |> whenProject databaseConfiguration
      |> expectAdmins
        [ { User = steamIdString testUser
            Immunity = ownerImmunity
            Group = configuredAdminGroups.Owner
            ReceivedOn = None
            Duration = None
            ExpirationDate = None
            PausedSince = None
          }
        ]
    )
    testCaseAsync "user as member with rank 'admin'" (
      given [ testUser |> wasAddedAsMemberWithRank adminRank ]
      |> whenProject databaseConfiguration
      |> expectAdmins
        [ { User = steamIdString testUser
            Immunity = adminImmunity
            Group = configuredAdminGroups.Admin
            ReceivedOn = None
            Duration = None
            ExpirationDate = None
            PausedSince = None
          }
        ]
    )
    testCaseAsync "user as member with rank 'moderator'" (
      given [ testUser |> wasAddedAsMemberWithRank moderatorRank ]
      |> whenProject databaseConfiguration
      |> expectAdmins
        [ { User = steamIdString testUser
            Immunity = moderatorImmunity
            Group = configuredAdminGroups.Moderator
            ReceivedOn = None
            Duration = None
            ExpirationDate = None
            PausedSince = None
          }
        ]
    )
    testCaseAsync "user as member with rank 'mapper'" (
      given [ testUser |> wasAddedAsMemberWithRank mapperRank ]
      |> whenProject databaseConfiguration
      |> expectAdmins
        [ { User = steamIdString testUser
            Immunity = mapperImmunity
            Group = configuredAdminGroups.Mapper
            ReceivedOn = None
            Duration = None
            ExpirationDate = None
            PausedSince = None
          }
        ]
    )
    testCaseAsync "user as member with rank 'permanent VIP'" (
      given [ testUser |> wasAddedAsMemberWithRank permanentVIPRank ]
      |> whenProject databaseConfiguration
      |> expectAdmins
        [ { User = steamIdString testUser
            Immunity = vipImmunity
            Group = configuredAdminGroups.VIP
            ReceivedOn = None
            Duration = None
            ExpirationDate = None
            PausedSince = None
          }
        ]
    )
    testCaseAsync "member got new rank" <| async {
      do!
        given [ testUser |> wasAddedAsMemberWithRank moderatorRank ]
        |> previousProjection databaseConfiguration

      return!
        given [ testUser |> rankWasSettedTo adminRank ]
        |> whenProject databaseConfiguration
        |> expectAdmins
          [ { User = steamIdString testUser
              Immunity = adminImmunity
              Group = configuredAdminGroups.Admin
              ReceivedOn = None
              Duration = None
              ExpirationDate = None
              PausedSince = None
            }
          ]
    }
    testCaseAsync "user is not listed anymore after removed as member" <| async {
      do!
        given [ testUser |> wasAddedAsMemberWithRank adminRank ]
        |> previousProjection databaseConfiguration

      return!
        given [ testUser |> wasRemovedFromMembers ]
        |> whenProject databaseConfiguration
        |> expectAdmins []
    }

    testCaseAsync "user as vip with expiration date" (
      given [ testUser |> receivedVIP (utcDate 10 01 2019 18 24) (durationFromDays 10) ]
      |> whenProject databaseConfiguration
      |> expectAdmins
        [ { User = steamIdString testUser
            Immunity = vipImmunity
            Group = configuredAdminGroups.VIP
            ReceivedOn = Some (utcDate 10 01 2019 18 24)
            Duration = Some (durationFromDays 10)
            ExpirationDate = Some (utcDate 20 01 2019 18 24)
            PausedSince = None
          }
        ]
    )
    testCaseAsync "user received vip after expiration without expired event" (
      given
        [ testUser |> receivedVIP (utcDate 10 01 2019 18 24) (durationFromDays 10)
          testUser |> receivedVIP (utcDate 05 02 2019 12 00) (durationFromDays 15)
        ]
      |> whenProject databaseConfiguration
      |> expectAdmins
        [ { User = steamIdString testUser
            Immunity = vipImmunity
            Group = configuredAdminGroups.VIP
            ReceivedOn = Some (utcDate 05 02 2019 12 00)
            Duration = Some (durationFromDays 15)
            ExpirationDate = Some (utcDate 20 02 2019 12 00)
            PausedSince = None
          }
        ]
    )
    testCaseAsync "vip time got extended" <| async {
      do!
        given [ testUser |> receivedVIP (utcDate 01 01 2019 15 45) (durationFromDays 5) ]
        |> previousProjection databaseConfiguration

      return!
        given [ testUser |> durationWasExtendedBy (durationFromDays 5) ]
        |> whenProject databaseConfiguration
        |> expectAdmins
          [ { User = steamIdString testUser
              Immunity = vipImmunity
              Group = configuredAdminGroups.VIP
              ReceivedOn = Some (utcDate 01 01 2019 15 45)
              Duration = Some (durationFromDays 10)
              ExpirationDate = Some (utcDate 11 01 2019 15 45)
              PausedSince = None
            }
          ]
    }
    testCaseAsync "vip time was corrected" <| async {
      do!
        given [ testUser |> receivedVIP (utcDate 01 01 2019 15 45) (durationFromDays 99) ]
        |> previousProjection databaseConfiguration

      return!
        given [ testUser |> durationWasCorrectedTo (durationFromDays 10) ]
        |> whenProject databaseConfiguration
        |> expectAdmins
          [ { User = steamIdString testUser
              Immunity = vipImmunity
              Group = configuredAdminGroups.VIP
              ReceivedOn = Some (utcDate 01 01 2019 15 45)
              Duration = Some (durationFromDays 10)
              ExpirationDate = Some (utcDate 11 01 2019 15 45)
              PausedSince = None
            }
          ]
    }
    testCaseAsync "vip paused" <| async {
      do!
        given [ testUser |> receivedVIP (utcDate 01 01 2019 10 00) (durationFromDays 20) ]
        |> previousProjection databaseConfiguration

      return!
        given [ testUser |> pausedVIPOn (utcDate 03 01 2019 12 15) ]
        |> whenProject databaseConfiguration
        |> expectAdmins
          [ { User = steamIdString testUser
              Immunity = vipImmunity
              Group = configuredAdminGroups.VIP
              ReceivedOn = Some (utcDate 01 01 2019 10 00)
              Duration = Some (durationFromDays 20)
              ExpirationDate = None
              PausedSince = Some (utcDate 03 01 2019 12 15)
            }
          ]
    }
    testCaseAsync "vip resumed" <| async {
      do!
        given
          [ testUser |> receivedVIP (utcDate 01 01 2019 10 30) (durationFromDays 20)
            testUser |> pausedVIPOn (utcDate 02 01 2019 10 30)
          ]
        |> previousProjection databaseConfiguration

      return!
        given [ testUser |> resumedVIPOn (utcDate 12 01 2019 10 30) ]
        |> whenProject databaseConfiguration
        |> expectAdmins
          [ { User = steamIdString testUser
              Immunity = vipImmunity
              Group = configuredAdminGroups.VIP
              ReceivedOn = Some (utcDate 01 01 2019 10 30)
              Duration = Some (durationFromDays 30)
              ExpirationDate = Some (utcDate 31 01 2019 10 30)
              PausedSince = None
            }
          ]
    }
    testCaseAsync "vip transferred time" <| async {
      do!
        given [ testUser |> receivedVIP (utcDate 01 01 2019 12 00) (durationFromDays 30) ]
        |> previousProjection databaseConfiguration

      return!
        given
          [ testUser |> transferredTimeTo secondUser (durationFromDays 10)
            secondUser |> receivedVIP (utcDate 05 01 2019 10 00) (durationFromDays 10)
          ]
          |> whenProject databaseConfiguration
          |> expectAdmins
            [ { User = steamIdString testUser
                Immunity = vipImmunity
                Group = configuredAdminGroups.VIP
                ReceivedOn = Some (utcDate 01 01 2019 12 00)
                Duration = Some (durationFromDays 20)
                ExpirationDate = Some (utcDate 21 01 2019 12 00)
                PausedSince = None
              }
              { User = steamIdString secondUser
                Immunity = vipImmunity
                Group = configuredAdminGroups.VIP
                ReceivedOn = Some (utcDate 05 01 2019 10 00)
                Duration = Some (durationFromDays 10)
                ExpirationDate = Some (utcDate 15 01 2019 10 00)
                PausedSince = None
              }
          ]
    }
    testCaseAsync "vip expired" <| async {
      do!
        given [ testUser |> receivedVIP (utcDate 01 01 2019 12 00) (durationFromDays 30) ]
        |> previousProjection databaseConfiguration

      return!
        given [ testUser |> hasExpired ]
        |> whenProject databaseConfiguration
        |> expectAdmins []
    }
    testCaseAsync "received vip after expiration" <| async {
      do!
        given
          [ testUser |> receivedVIP (utcDate 01 01 2019 09 45) (durationFromDays 2)
            testUser |> hasExpired
          ]
        |> previousProjection databaseConfiguration

      return!
        given [ testUser |> receivedVIP (utcDate 05 01 2019 20 15) (durationFromDays 15) ]
        |> whenProject databaseConfiguration
        |> expectAdmins
          [ { User = steamIdString testUser
              Immunity = vipImmunity
              Group = configuredAdminGroups.VIP
              ReceivedOn = Some (utcDate 05 01 2019 20 15)
              Duration = Some (durationFromDays 15)
              ExpirationDate = Some (utcDate 20 01 2019 20 15)
              PausedSince = None
            }
          ]
    }
    testCaseAsync "vip removed due to rule breaking" <| async {
      do!
        given [ testUser |> receivedVIP (utcDate 01 01 2019 20 22) (durationFromDays 10) ]
        |> previousProjection databaseConfiguration

      return!
        given [ testUser |> wasRemovedDueToRuleBreaking ]
        |> whenProject databaseConfiguration
        |> expectAdmins []
    }

    testCaseAsync "nothing happens if no event is given" <| async {
      do!
        given [ testUser |> wasAddedAsMemberWithRank ownerRank ]
        |> previousProjection databaseConfiguration

      return!
        given []
        |> whenProject databaseConfiguration
        |> expectAdmins
          [ { User = steamIdString testUser
              Immunity = ownerImmunity
              Group = configuredAdminGroups.Owner
              ReceivedOn = None
              Duration = None
              ExpirationDate = None
              PausedSince = None
            }
          ]
    }
  ]
  |> testList "SourceMod Admins"
  |> testSequenced