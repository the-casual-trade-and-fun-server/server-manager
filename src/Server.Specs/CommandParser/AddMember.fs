module Server.Specs.CommandParser.AddMember

open Expecto
open TestHelpers
open Domain.Members.Commands

let private asAddMemberCommand = Server.RequestHandler.Members.asAddMemberCommand

let private addMember user rank : AddMember =
  {
    user = steamId user
    rank = rank
  }

let tests =
  [
    test "AddMember parsed with rank 'owner'" {
      requestWithFormData
        [
          "user", Some "STEAM_0:0:61241"
          "rank", Some owner
        ]
      |> asAddMemberCommand
      |> Then (addMember "STEAM_0:0:61241" ownerRank)
    }

    test "AddMember parsed with rank 'admin'" {
      requestWithFormData
        [
          "user", Some "STEAM_0:0:61241"
          "rank", Some admin
        ]
      |> asAddMemberCommand
      |> Then (addMember "STEAM_0:0:61241" adminRank)
    }

    test "AddMember parsed with rank 'moderator'" {
      requestWithFormData
        [
          "user", Some "STEAM_0:0:61241"
          "rank", Some moderator
        ]
      |> asAddMemberCommand
      |> Then (addMember "STEAM_0:0:61241" moderatorRank)
    }

    test "AddMember parsed with rank 'mapper'" {
      requestWithFormData
        [
          "user", Some "STEAM_0:0:61241"
          "rank", Some mapper
        ]
      |> asAddMemberCommand
      |> Then (addMember "STEAM_0:0:61241" mapperRank)
    }

    test "AddMember parsed with rank 'permanentVIP'" {
      requestWithFormData
        [
          "user", Some "STEAM_0:0:61241"
          "rank", Some permanentVIP
        ]
      |> asAddMemberCommand
      |> Then (addMember "STEAM_0:0:61241" permanentVIPRank)
    }

    test "fails when rank is not valid" {
      requestWithFormData
        [
          "user", Some "STEAM_0:0:61241"
          "rank", Some "wrongRank"
        ]
      |> asAddMemberCommand
      |> ThenFailWith (wrongFormat "Rank 'wrongRank' does not exists")
    }

    test "fails when parameter 'user' is missing" {
      requestWithFormData
        [
          "rank", Some permanentVIP
        ]
      |> asAddMemberCommand
      |> ThenFailWith (missing "user")
    }

    test "fails when parameter 'user' is empty" {
      requestWithFormData
        [
          "user", None
          "rank", Some permanentVIP
        ]
      |> asAddMemberCommand
      |> ThenFailWith (missing "user")
    }

    test "fails when parameter 'rank' is missing" {
      requestWithFormData
        [
          "user", Some "STEAM_0:0:61241"
        ]
      |> asAddMemberCommand
      |> ThenFailWith (missing "rank")
    }

    test "fails when parameter 'rank' is empty" {
      requestWithFormData
        [
          "user", Some "STEAM_0:0:61241"
          "rank", None
        ]
      |> asAddMemberCommand
      |> ThenFailWith (missing "rank")
    }
  ]
  |> testList "Add Member"