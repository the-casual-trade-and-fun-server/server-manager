module Server.Specs.CommandParser.SetRank

open Expecto
open TestHelpers
open Domain.Members.Commands

let private asSetRankOfMemberCommand = Server.RequestHandler.Members.asSetRankOfMemberCommand

let private setRankOfMemberTo user rank : SetRankOfMemberTo =
  {
    user = steamId user
    newRank = rank
  }

let tests =
  [
    test "SetRankOfMemberTo parsed with rank 'owner'" {
      requestWithFormData
        [
          "user", Some "STEAM_0:1:81252"
          "newRank", Some owner
        ]
      |> asSetRankOfMemberCommand
      |> Then (setRankOfMemberTo "STEAM_0:1:81252" ownerRank)
    }

    test "SetRankOfMemberTo parsed with rank 'admin'" {
      requestWithFormData
        [
          "user", Some "STEAM_0:1:81252"
          "newRank", Some admin
        ]
      |> asSetRankOfMemberCommand
      |> Then (setRankOfMemberTo "STEAM_0:1:81252" adminRank)
    }

    test "SetRankOfMemberTo parsed with rank 'moderator'" {
      requestWithFormData
        [
          "user", Some "STEAM_0:1:81252"
          "newRank", Some moderator
        ]
      |> asSetRankOfMemberCommand
      |> Then (setRankOfMemberTo "STEAM_0:1:81252" moderatorRank)
    }

    test "SetRankOfMemberTo parsed with rank 'mapper'" {
      requestWithFormData
        [
          "user", Some "STEAM_0:1:81252"
          "newRank", Some mapper
        ]
      |> asSetRankOfMemberCommand
      |> Then (setRankOfMemberTo "STEAM_0:1:81252" mapperRank)
    }

    test "SetRankOfMemberTo parsed with rank 'permanentVIP'" {
      requestWithFormData
        [
          "user", Some "STEAM_0:1:81252"
          "newRank", Some permanentVIP
        ]
      |> asSetRankOfMemberCommand
      |> Then (setRankOfMemberTo "STEAM_0:1:81252" permanentVIPRank)
    }

    test "fails when rank is wrong" {
      requestWithFormData
        [
          "user", Some "STEAM_0:1:81252"
          "newRank", Some "not valid"
        ]
      |> asSetRankOfMemberCommand
      |> ThenFailWith (wrongFormat "Rank 'not valid' does not exists")
    }

    test "fails when parameter 'user' is missing" {
      requestWithFormData
        [
          "newRank", Some owner
        ]
      |> asSetRankOfMemberCommand
      |> ThenFailWith (missing "user")
    }

    test "fails when parameter 'user' is empty" {
      requestWithFormData
        [
          "user", None
          "newRank", Some owner
        ]
      |> asSetRankOfMemberCommand
      |> ThenFailWith (missing "user")
    }

    test "fails when parameter 'newRank' is missing" {
      requestWithFormData
        [
          "user", Some "STEAM_0:1:81252"
        ]
      |> asSetRankOfMemberCommand
      |> ThenFailWith (missing "newRank")
    }

    test "fails when parameter 'newRank' is empty" {
      requestWithFormData
        [
          "user", Some "STEAM_0:1:81252"
          "newRank", None
        ]
      |> asSetRankOfMemberCommand
      |> ThenFailWith (missing "newRank")
    }
  ]
  |> testList "SetRank"