module Server.Specs.CommandParser.UseNoJoinsound

open Expecto
open Domain.ACL.UserSettings
open TestHelpers

let private asUseNoJoinsoundCommand = Server.RequestHandler.UserSettings.asUseNoJoinsoundCommand

let private useNoJoinsound user : UseNoJoinsoundCommand =
  { User = steamId user }

let tests =
  [
    test "parse" {
      requestWithFormData [ ("user", Some "STEAM_0:0:123") ]
      |> asUseNoJoinsoundCommand
      |> Then (useNoJoinsound "STEAM_0:0:123")
    }

    test "fail when user is empty" {
      requestWithFormData [ ("user", None) ]
      |> asUseNoJoinsoundCommand
      |> ThenFailWith (missing "user")
    }

    test "fail when user is missing" {
      requestWithFormData []
      |> asUseNoJoinsoundCommand
      |> ThenFailWith (missing "user")
    }
  ]
  |> testList "parse 'use no joinsound'"