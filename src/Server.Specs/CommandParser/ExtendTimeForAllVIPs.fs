module Server.Specs.CommandParser.ExtendTimeForAllVIPs

open Expecto
open TestHelpers
open Domain.Members.Commands

let private asExtendTimeForAllVIPsCommand =
  Server.RequestHandler.VIPs.asExtendTimeForAllVIPsCommand

let private extendTimeForAllVIPs date duration : ExtendTimeForAllVIPs =
  {
    date = date
    duration = duration
  }

let tests =
  [
    test "parsed and date converted to UTC" {
      requestWithFormData
        [
          "date", Some (dateAsTimestamp 15 11 2019 12 15)
          "duration", Some "10"
        ]
      |> asExtendTimeForAllVIPsCommand
      |> Then (extendTimeForAllVIPs (utcDate 15 11 2019 12 15) (durationInDays 10))
    }

    test "fails due to missing date" {
      requestWithFormData
        [
          "duration", Some "10"
        ]
      |> asExtendTimeForAllVIPsCommand
      |> ThenFailWith (missing "date")
    }

    test "fails due to empty date" {
      requestWithFormData
        [
          "date", None
          "duration", Some "10"
        ]
      |> asExtendTimeForAllVIPsCommand
      |> ThenFailWith (missing "date")
    }

    test "fails due to missing duration" {
      requestWithFormData
        [
          "date", Some (dateAsTimestamp 15 11 2019 12 15)
        ]
      |> asExtendTimeForAllVIPsCommand
      |> ThenFailWith (missing "duration")
    }

    test "fails due to empty duration" {
      requestWithFormData
        [
          "date", Some (dateAsTimestamp 15 11 2019 12 15)
          "duration", None
        ]
      |> asExtendTimeForAllVIPsCommand
      |> ThenFailWith (missing "duration")
    }
  ]
  |> testList "Parse 'extend time of all VIPs'"