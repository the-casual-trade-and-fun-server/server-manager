module Server.Specs.CommandParser.CorrectDuration

open Expecto
open TestHelpers
open Domain.Members.Commands

let private asCorrectDuration = Server.RequestHandler.VIPs.asCorrectVIPDurationCommand

let private correctDurationCommand user duration : CorrectDurationOfVIP =
  {
    user = steamId user
    newDuration = duration
  }

let tests =
  [
    test "CorrectDuration parsed" {
      requestWithFormData
        [
          "user", Some "STEAM_0:1:47124124"
          "newDuration", Some "72000"
        ]
      |> asCorrectDuration
      |> Then (correctDurationCommand "STEAM_0:1:47124124" 72000u)
    }

    test "fails when parameter 'user' is missing" {
      requestWithFormData
        [
          "newDuration", Some "72000"
        ]
      |> asCorrectDuration
      |> ThenFailWith (missing "user")
    }

    test "fails when parameter 'user' is empty" {
      requestWithFormData
        [
          "user", None
          "newDuration", Some "72000"
        ]
      |> asCorrectDuration
      |> ThenFailWith (missing "user")
    }

    test "fails when parameter 'newDuration' is missing" {
      requestWithFormData
        [
          "user", Some "STEAM_0:1:47124124"
        ]
      |> asCorrectDuration
      |> ThenFailWith (missing "newDuration")
    }

    test "fails when parameter 'newDuration' is empty" {
      requestWithFormData
        [
          "user", Some "STEAM_0:1:47124124"
          "newDuration", None
        ]
      |> asCorrectDuration
      |> ThenFailWith (missing "newDuration")
    }
  ]
  |> testList "Correct duration"