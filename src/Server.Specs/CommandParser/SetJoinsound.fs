module Server.Specs.CommandParser.SetJoinsound

open Expecto
open Domain.ACL.UserSettings
open TestHelpers

let private asSetJoinsoundCommand = Server.RequestHandler.UserSettings.asSetJoinsosundCommand

let private setJoinsound user sound : SetJoinsoundCommand =
  { User = steamId user
    Sound = sound
  }

let tests =
  [
    test "parse" {
      requestWithFormData
        [
          "user", Some "STEAM_0:0:123"
          "sound", Some "test.mp3"
        ]
      |> asSetJoinsoundCommand
      |> Then (setJoinsound "STEAM_0:0:123" "test.mp3")
    }

    test "fail when user is empty" {
      requestWithFormData
        [
          "user", None
          "sound", Some "test.mp3"
        ]
      |> asSetJoinsoundCommand
      |> ThenFailWith (missing "user")
    }

    test "fail when user is missing" {
      requestWithFormData
        [
          "sound", Some "test.mp3"
        ]
      |> asSetJoinsoundCommand
      |> ThenFailWith (missing "user")
    }

    test "fail when sound is empty" {
      requestWithFormData
        [
          "user", Some "STEAM_0:0:123"
          "sound", None
        ]
      |> asSetJoinsoundCommand
      |> ThenFailWith (missing "sound")
    }

    test "fail when sound is missing" {
      requestWithFormData
        [
          "user", Some "STEAM_0:0:123"
        ]
      |> asSetJoinsoundCommand
      |> ThenFailWith (missing "sound")
    }
  ]
  |> testList "parse 'set joinsound'"