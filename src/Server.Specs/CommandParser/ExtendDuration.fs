module Server.Specs.CommandParser.ExtendDuration

open Expecto
open TestHelpers
open Domain.Members.Commands

let asExtendDuration = Server.RequestHandler.VIPs.asExtendVIPDurationCommand

let extendDuration user date duration : ExtendDurationOfVIP =
  {
    user = steamId user
    dateOfExtension = date
    additionalDuration = duration
  }

let tests =
  [
    test "ExtendDuration parsed, date converted to UTC" {
      requestWithFormData
        [
          "user", Some "STEAM_0:0:61481248"
          "dateOfExtension", Some (dateAsTimestamp 20 05 2019 14 45)
          "additionalDuration", Some "50000"
        ]
      |> asExtendDuration
      |> Then (extendDuration "STEAM_0:0:61481248" (utcDate 20 05 2019 14 45) 50000u)
    }

    test "fails when parameter 'user' is missing" {
      requestWithFormData
        [
          "dateOfExtension", Some (dateAsTimestamp 20 05 2019 14 45)
          "additionalDuration", Some "50000"
        ]
      |> asExtendDuration
      |> ThenFailWith (missing "user")
    }

    test "fails when parameter 'user' is empty" {
      requestWithFormData
        [
          "user", None
          "dateOfExtension", Some (dateAsTimestamp 20 05 2019 14 45)
          "additionalDuration", Some "50000"
        ]
      |> asExtendDuration
      |> ThenFailWith (missing "user")
    }

    test "fails when parameter 'dateOfExtension' is missing" {
      requestWithFormData
        [
          "user", Some "STEAM_0:0:61481248"
          "additionalDuration", Some "50000"
        ]
      |> asExtendDuration
      |> ThenFailWith (missing "dateOfExtension")
    }

    test "fails when parameter 'dateOfExtension' is empty" {
      requestWithFormData
        [
          "user", Some "STEAM_0:0:61481248"
          "dateOfExtension", None
          "additionalDuration", Some "50000"
        ]
      |> asExtendDuration
      |> ThenFailWith (missing "dateOfExtension")
    }

    test "fails when parameter 'additionalDuration' is missing" {
      requestWithFormData
        [
          "user", Some "STEAM_0:0:61481248"
          "dateOfExtension", Some (dateAsTimestamp 29 12 2018 18 30)
        ]
      |> asExtendDuration
      |> ThenFailWith (missing "additionalDuration")
    }

    test "fails when parameter 'additionalDuration' is empty" {
      requestWithFormData
        [
          "user", Some "STEAM_0:0:61481248"
          "dateOfExtension", Some (dateAsTimestamp 29 12 2018 18 30)
          "additionalDuration", None
        ]
      |> asExtendDuration
      |> ThenFailWith (missing "additionalDuration")
    }
  ]
  |> testList "Extend duration"