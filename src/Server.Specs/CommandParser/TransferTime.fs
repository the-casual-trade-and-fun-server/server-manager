module Server.Specs.CommandParser.TransferTime

open Expecto
open TestHelpers
open Domain.Members.Commands

let private asTransferTimeCommand = Server.RequestHandler.VIPs.asTransferTimeCommand

let private transferTimeCommand user target date duration =
  {
    user = steamId user
    target = steamId target
    date = date
    duration = duration
  }

let tests =
  [
    test "Command `TransferTime` from request, date is converted to UTC" {
      requestWithFormData
        [
          "user", Some "STEAM_0:1:824812412"
          "target", Some "STEAM_0:1:51248212"
          "date", Some (dateAsTimestamp 01 01 2019 18 00)
          "duration", Some "29"
        ]
      |> asTransferTimeCommand
      |> Then (transferTimeCommand "STEAM_0:1:824812412" "STEAM_0:1:51248212" (utcDate 01 01 2019 18 00) (durationInDays 29))
    }

    test "Command `TransferTime` from request fails due to missing 'user'" {
      requestWithFormData
        [
          "target", Some "STEAM_0:1:51248212"
          "date", Some (dateAsTimestamp 01 01 2019 18 00)
          "duration", Some "13"
        ]
      |> asTransferTimeCommand
      |> ThenFailWith (missing "user")
    }

    test "Command `TransferTime` from request fails due to 'user' without argument" {
      requestWithFormData
        [
          "user", None
          "target", Some "STEAM_0:1:51248212"
          "date", Some (dateAsTimestamp 01 01 2019 18 00)
          "duration", Some "50"
        ]
      |> asTransferTimeCommand
      |> ThenFailWith (missing "user")
    }

    test "Command `TransferTime` from request fails due to missing 'target'" {
      requestWithFormData
        [
          "user", Some "STEAM_0:1:51248212"
          "date", Some (dateAsTimestamp 01 01 2019 18 00)
          "duration", Some "90"
        ]
      |> asTransferTimeCommand
      |> ThenFailWith (missing "target")
    }

    test "Command `TransferTime` from request fails due to missing 'date'" {
      requestWithFormData
        [
          "user", Some "STEAM_0:1:824812412"
          "target", Some "STEAM_0:1:51248212"
          "duration", Some "120"
        ]
      |> asTransferTimeCommand
      |> ThenFailWith (missing "date")
    }

    test "Command `TransferTime` from request fails due to missing 'duration'" {
      requestWithFormData
        [
          "user", Some "STEAM_0:1:824812412"
          "target", Some "STEAM_0:1:51248212"
          "date", Some (dateAsTimestamp 01 01 2019 18 00)
        ]
      |> asTransferTimeCommand
      |> ThenFailWith (missing "duration")
    }
  ]
  |> testList "Parse transferTime"