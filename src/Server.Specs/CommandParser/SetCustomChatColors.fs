module Server.Specs.CommandParser.SetCustomChatColor

open Expecto
open Domain.ACL.UserSettings
open TestHelpers

let private asSetCustomChatColorsCommand = Server.RequestHandler.UserSettings.asSetCustomChatColorsCommand

let private setCustomChatColor user colors : SetCustomChatColorsCommand =
  { User = steamId user
    Colors = colors
  }

let private colors tag name chat : CustomChatColors =
  { Tag = Some tag
    Name = Some name
    Chat = Some chat
  }

let private customColors : CustomChatColors =
  { Tag = None
    Name = None
    Chat = None
  }
let private withTagColor color customChatColors : CustomChatColors =
  { customChatColors with Tag = Some color }
let private withNameColor color customChatColors : CustomChatColors =
  { customChatColors with Name = Some color }
let private withChatColor color customChatColors : CustomChatColors =
  { customChatColors with Chat = Some color }

let tests =
  [
    test "parsed with colors" {
      requestWithFormData
        [
          "user", Some "STEAM_0:1:81252"
          "tag", Some "#bbff33"
          "name", Some "#002200"
          "chat", Some "#aa22ee"
        ]
      |> asSetCustomChatColorsCommand
      |> Then (setCustomChatColor "STEAM_0:1:81252" (colors "#bbff33" "#002200" "#aa22ee"))
    }

    test "parsed with with tag color" {
      requestWithFormData
        [
          "user", Some "STEAM_0:1:81252"
          "tag", Some "#bbff33"
          "name", None
          "chat", None
        ]
      |> asSetCustomChatColorsCommand
      |> Then (setCustomChatColor "STEAM_0:1:81252" (customColors |> withTagColor "#bbff33"))
    }

    test "parsed with with name color" {
      requestWithFormData
        [
          "user", Some "STEAM_0:1:81252"
          "tag", None
          "name", Some "#002200"
          "chat", None
        ]
      |> asSetCustomChatColorsCommand
      |> Then (setCustomChatColor "STEAM_0:1:81252" (customColors |> withNameColor "#002200"))
    }

    test "parsed with with chat color" {
      requestWithFormData
        [
          "user", Some "STEAM_0:1:81252"
          "tag", None
          "name", None
          "chat", Some "#aa22ee"
        ]
      |> asSetCustomChatColorsCommand
      |> Then (setCustomChatColor "STEAM_0:1:81252" (customColors |> withChatColor "#aa22ee"))
    }

    test "fails when 'tag' is missing" {
      requestWithFormData
        [
          "user", Some "STEAM_0:1:81252"
          "name", None
          "chat", None
        ]
      |> asSetCustomChatColorsCommand
      |> ThenFailWith (missing "tag")
    }

    test "fails when 'name' is missing" {
      requestWithFormData
        [
          "user", Some "STEAM_0:1:81252"
          "tag", None
          "chat", None
        ]
      |> asSetCustomChatColorsCommand
      |> ThenFailWith (missing "name")
    }

    test "fails when 'chat' is missing" {
      requestWithFormData
        [
          "user", Some "STEAM_0:1:81252"
          "tag", None
          "name", None
        ]
      |> asSetCustomChatColorsCommand
      |> ThenFailWith (missing "chat")
    }
  ]
  |>
  testList "Parse command 'SetCustomChatColor'"