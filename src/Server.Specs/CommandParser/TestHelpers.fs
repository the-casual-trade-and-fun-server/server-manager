module Server.Specs.CommandParser.TestHelpers

open Expecto
open Domain.Members.Types

let steamId user = SteamId user

let owner = "owner"
let admin = "admin"
let moderator = "moderator"
let mapper = "mapper"
let permanentVIP = "permanent-vip"

let ownerRank = Rank.Owner
let adminRank = Rank.Admin
let moderatorRank = Rank.Moderator
let mapperRank = Rank.Mapper
let permanentVIPRank = Rank.PermanentVIP

let requestWithFormData : (string * string option) list -> Map<string, string option> = Map.ofList

let utcDate day month year hours minutes =
  System.DateTime(year, month, day, hours, minutes, 0, System.DateTimeKind.Utc)

let date day month year hours minutes =
  System.DateTime(year, month, day, hours, minutes, 0, System.DateTimeKind.Local)

let utc (date : System.DateTime) =
  date.ToUniversalTime()

let dateAsTimestamp day month year hours minutes =
  System.DateTimeOffset(year, month, day, hours, minutes, 0, System.TimeSpan.Zero).ToUnixTimeSeconds() |> string

let durationInDays days =
  days |> uint16

let missing argument =
  Server.RequestHandler.Utils.Missing argument
let wrongFormat info =
  Server.RequestHandler.Utils.WrongFormat info

let Then expected actual =
  match actual with
  | Ok command ->
      Expect.equal command expected "command parsed wrong"

  | Error err ->
      failtestf "Command parsing should succeed but failed (%A)" err

let ThenFailWith expected actual =
  match actual with
  | Ok _ ->
      failtest "Should failed but succeeded"

  | Error err ->
      Expect.equal err expected "wrong error"