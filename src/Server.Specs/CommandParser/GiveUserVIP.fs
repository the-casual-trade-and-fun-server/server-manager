module Server.Specs.CommandParser.GiveUserVIP

open Expecto
open TestHelpers
open Domain.Members.Commands

let private asGiveUserVIPCommand = Server.RequestHandler.VIPs.asGiveUserVIPCommand

let private giveUserVIP user date duration : GiveUserVIP =
  {
    user = steamId user
    date = date
    duration = duration
  }

let tests =
  [
    test "GiveUserVIP parsed, and converts datetime to UTC" {
      requestWithFormData
        [
          "user", Some "STEAM_0:0:12582231"
          "date", Some (dateAsTimestamp 01 01 2019 18 00)
          "duration", Some "84000"
        ]
      |> asGiveUserVIPCommand
      |> Then (giveUserVIP "STEAM_0:0:12582231" (utcDate 01 01 2019 18 00) 84000u)
    }

    test "fails when parameter 'user' is missing" {
      requestWithFormData
        [
          "date", Some (dateAsTimestamp 01 01 2019 18 00)
          "duration", Some "84000"
        ]
      |> asGiveUserVIPCommand
      |> ThenFailWith (missing "user")
    }

    test "fails when parameter 'user' is empty" {
      requestWithFormData
        [
          "user", None
          "date", Some (dateAsTimestamp 01 01 2019 18 00)
          "duration", Some "84000"
        ]
      |> asGiveUserVIPCommand
      |> ThenFailWith (missing "user")
    }

    test "fails when parameter 'date' is missing" {
      requestWithFormData
        [
          "user", Some "STEAM_0:0:12582231"
          "duration", Some "84000"
        ]
      |> asGiveUserVIPCommand
      |> ThenFailWith (missing "date")
    }

    test "fails when parameter 'date' is empty" {
      requestWithFormData
        [
          "user", Some "STEAM_0:0:12582231"
          "date", None
          "duration", Some "84000"
        ]
      |> asGiveUserVIPCommand
      |> ThenFailWith (missing "date")
    }

    test "fails when parameter 'duration' is missing" {
      requestWithFormData
        [
          "user", Some "STEAM_0:0:12582231"
          "date", Some (dateAsTimestamp 01 01 2019 18 00)
        ]
      |> asGiveUserVIPCommand
      |> ThenFailWith (missing "duration")
    }

    test "fails when parameter 'duration' is empty" {
      requestWithFormData
        [
          "user", Some "STEAM_0:0:12582231"
          "date", Some (dateAsTimestamp 01 01 2019 18 00)
          "duration", None
        ]
      |> asGiveUserVIPCommand
      |> ThenFailWith (missing "duration")
    }
  ]
  |> testList "Give user VIP"