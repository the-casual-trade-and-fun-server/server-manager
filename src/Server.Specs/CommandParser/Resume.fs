module Server.Specs.CommandParser.Resume

open Expecto
open TestHelpers
open Domain.Members.Commands

let private asResumeVIP = Server.RequestHandler.VIPs.asResumeVIPCommand

let private resume user date : ResumeVIP =
  {
    user = steamId user
    resumeDate = date
  }

let tests =
  [
    test "ResumeVIP parsed, date converted to UTC" {
      requestWithFormData
        [
          "user", Some "STEAM_0:0:89123213"
          "resumeDate", Some (dateAsTimestamp 23 11 2019 09 10)
        ]
      |> asResumeVIP
      |> Then (resume "STEAM_0:0:89123213" (utcDate 23 11 2019 09 10))
    }

    test "fails when parameter 'user' is missing" {
      requestWithFormData
        [
          "resumeDate", Some (dateAsTimestamp 23 11 2019 09 10)
        ]
      |> asResumeVIP
      |> ThenFailWith (missing "user")
    }

    test "fails when parameter 'user' is empty" {
      requestWithFormData
        [
          "user", None
          "resumeDate", Some (dateAsTimestamp 23 11 2019 09 10)
        ]
      |> asResumeVIP
      |> ThenFailWith (missing "user")
    }

    test "fails when parameter 'resumeDate' is missing" {
      requestWithFormData
        [
          "user", Some "STEAM_0:0:9084724"
        ]
      |> asResumeVIP
      |> ThenFailWith (missing "resumeDate")
    }

    test "fails when parameter 'resumeDate' is empty" {
      requestWithFormData
        [
          "user", Some "STEAM_0:0:9084724"
          "resumeDate", None
        ]
      |> asResumeVIP
      |> ThenFailWith (missing "resumeDate")
    }
  ]
  |> testList "Resume VIP"