module Server.Specs.CommandParser.RemoveMember

open Expecto
open TestHelpers
open Domain.Members.Commands

let private asRemoveMemberCommand = Server.RequestHandler.Members.asRemoveMemberCommand

let private removeMember user : RemoveMember =
  { user = steamId user }

let tests =
  [
    test "RemoveMember parsed" {
      requestWithFormData [ "user", Some "STEAM_0:0:71241824" ]
      |> asRemoveMemberCommand
      |> Then (removeMember "STEAM_0:0:71241824")
    }

    test "fails when parameter 'user' is missing" {
      requestWithFormData []
      |> asRemoveMemberCommand
      |> ThenFailWith (missing "user")
    }

    test "fails when parameter 'user' is empty" {
      requestWithFormData [ "user", None ]
      |> asRemoveMemberCommand
      |> ThenFailWith (missing "user")
    }
  ]
  |> testList "Remove member"