module Server.Specs.CommandParser.RemoveDueToRuleBreaking

open Expecto
open TestHelpers
open Domain.Members.Commands

let private asRemoveDueToRuleBreaking = Server.RequestHandler.VIPs.asRemoveVIPDueToRuleBreakingCommand

let private removeDueToRuleBreaking user : RemoveVIPDueToRuleBreaking =
  { user = steamId user }

let tests =
  [
    test "RemoveDueToRuleBreaking parsed" {
      requestWithFormData [ "user", Some "STEAM_0:0:51258825" ]
      |> asRemoveDueToRuleBreaking
      |> Then (removeDueToRuleBreaking "STEAM_0:0:51258825")
    }

    test "fails when parameter 'user' is missing" {
      requestWithFormData []
      |> asRemoveDueToRuleBreaking
      |> ThenFailWith (missing "user")
    }

    test "fails when parameter 'user' is empty" {
      requestWithFormData [ "user", None ]
      |> asRemoveDueToRuleBreaking
      |> ThenFailWith (missing "user")
    }
  ]
  |> testList "Remove due to rule breaking"