module Server.Specs.CommandParser.Pause

open Expecto
open TestHelpers
open Domain.Members.Commands

let private asPauseCommand = Server.RequestHandler.VIPs.asPauseVIPCommand

let private pause user date : PauseVIP =
  {
    user = steamId user
    pauseDate = date
  }

let tests =
  [
    test "PauseVIP parsed, date converted to UTC" {
      requestWithFormData
        [
          "user", Some "STEAM_0:0:15725151"
          "pauseDate", Some (dateAsTimestamp 14 03 2019 18 39)
        ]
      |> asPauseCommand
      |> Then (pause "STEAM_0:0:15725151" (utcDate 14 03 2019 18 39))
    }

    test "fails when parameter 'user' is missing" {
      requestWithFormData
        [
          "pauseDate", Some (dateAsTimestamp 14 03 2019 18 39)
        ]
      |> asPauseCommand
      |> ThenFailWith (missing "user")
    }

    test "fails when parameter 'user' is empty" {
      requestWithFormData
        [
          "user", None
          "pauseDate", Some (dateAsTimestamp 14 03 2019 18 39)
        ]
      |> asPauseCommand
      |> ThenFailWith (missing "user")
    }

    test "fails when parameter 'pauseDate' is missing" {
      requestWithFormData
        [
          "user", Some "STEAM_0:0:1858125"
        ]
      |> asPauseCommand
      |> ThenFailWith (missing "pauseDate")
    }

    test "fails when parameter 'pauseDate' is empty" {
      requestWithFormData
        [
          "user", Some "STEAM_0:0:1858125"
          "pauseDate", None
        ]
      |> asPauseCommand
      |> ThenFailWith (missing "pauseDate")
    }
  ]
  |> testList "Pause VIP"