module Server.Specs.Main

open Expecto

let tests =
  testList "Server"
    [ CommandParser.AddMember.tests
      CommandParser.SetRank.tests
      CommandParser.RemoveMember.tests
      CommandParser.GiveUserVIP.tests
      CommandParser.CorrectDuration.tests
      CommandParser.ExtendDuration.tests
      CommandParser.ExtendTimeForAllVIPs.tests
      CommandParser.TransferTime.tests
      CommandParser.RemoveDueToRuleBreaking.tests
      CommandParser.Pause.tests
      CommandParser.Resume.tests
      CommandParser.SetCustomChatColor.tests
      CommandParser.SetJoinsound.tests
      CommandParser.UseNoJoinsound.tests
    ]

[<EntryPoint>]
let main args =
  runTestsWithCLIArgs
    []
    args
    tests
