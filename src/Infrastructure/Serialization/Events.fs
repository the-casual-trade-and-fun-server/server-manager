module Infrastructure.Serialization.Events

open Domain.Members.Events
open Domain.Members.Types
open Newtonsoft.Json

type SteamIdDTO = string
type RankDTO = string
type DateDTO = System.DateTime
type VIPDurationDTO = uint32

type EventDTO =
  | DTOMemberWasAdded of SteamIdDTO * RankDTO
  | DTOMemberGotNewRank of SteamIdDTO * RankDTO
  | DTOMemberWasRemoved of SteamIdDTO
  | DTOUserRecievedVIP of SteamIdDTO * DateDTO * VIPDurationDTO
  | DTOVIPDurationWasCorrected of SteamIdDTO * VIPDurationDTO
  | DTOVIPDurationWasExtended of SteamIdDTO * VIPDurationDTO
  | DTOVIPTransferredTimeTo of SteamIdDTO * SteamIdDTO * VIPDurationDTO
  | DTOVIPWasRemovedDueToRuleBreaking of SteamIdDTO
  | DTOVIPWasPaused of SteamIdDTO * DateDTO
  | DTOVIPWasResumed of SteamIdDTO * DateDTO
  | DTOVIPHasExpired of SteamIdDTO

let private rankSerialized (rank : Rank) =
  match rank with
  | Owner -> "owner"
  | Admin -> "admin"
  | Moderator -> "moderator"
  | Mapper -> "mapper"
  | PermanentVIP -> "permanentVIP"

let private eventAsDTO (event : Event) : EventDTO =
  match event with
  | MemberWasAdded (SteamId id, rank) ->
      DTOMemberWasAdded (id, rank |> rankSerialized)

  | MemberGotNewRank (SteamId id, rank) ->
      DTOMemberGotNewRank (id, rank |> rankSerialized)

  | MemberWasRemoved (SteamId id) ->
      DTOMemberWasRemoved id

  | UserRecievedVIP (SteamId id, date, duration) ->
      DTOUserRecievedVIP (id, date, duration)

  | VIPDurationWasCorrected (SteamId id, duration) ->
      DTOVIPDurationWasCorrected (id, duration)

  | VIPDurationWasExtended (SteamId id, duration) ->
      DTOVIPDurationWasExtended (id, duration)

  | VIPTransferredTimeTo (SteamId user, SteamId target, duration) ->
      DTOVIPTransferredTimeTo (user, target, duration)

  | VIPWasRemovedDueToRuleBreaking (SteamId id) ->
      DTOVIPWasRemovedDueToRuleBreaking id

  | VIPWasPaused (SteamId id, date) ->
      DTOVIPWasPaused (id, date)

  | VIPWasResumed (SteamId id, date) ->
      DTOVIPWasResumed (id, date)

  | VIPHasExpired (SteamId id) ->
      DTOVIPHasExpired id

let eventSerialized (event : Event) : string =
  event
  |> eventAsDTO
  |> JsonConvert.SerializeObject

let private rankDeserialized rank : Result<Rank, string> =
  match rank with
  | "owner" -> Ok Owner
  | "admin" -> Ok Admin
  | "moderator" -> Ok Moderator
  | "mapper" -> Ok Mapper
  | "permanentVIP" -> Ok PermanentVIP
  | unknown -> unknown |> sprintf "unknown rank: %s" |> Error

let private steamIdDeserialized id : SteamId =
  SteamId id

let private eventFromDTO (dto : EventDTO) : Result<Event, string> =
  match dto with
  | DTOMemberWasAdded (id, rank) ->
      rank
      |> rankDeserialized
      |> Result.map (fun rank -> MemberWasAdded (id |> steamIdDeserialized, rank))

  | DTOMemberGotNewRank (id, rank) ->
      rank
      |> rankDeserialized
      |> Result.map (fun rank -> MemberGotNewRank (id |> steamIdDeserialized, rank))

  | DTOMemberWasRemoved id ->
      id
      |> steamIdDeserialized
      |> MemberWasRemoved
      |> Ok

  | DTOUserRecievedVIP (id, date, duration) ->
      (id |> steamIdDeserialized, date, duration)
      |> UserRecievedVIP
      |> Ok

  | DTOVIPDurationWasCorrected (id, duration) ->
      (id |> steamIdDeserialized, duration)
      |> VIPDurationWasCorrected
      |> Ok

  | DTOVIPDurationWasExtended (id, duration) ->
      (id |> steamIdDeserialized, duration)
      |> VIPDurationWasExtended
      |> Ok

  | DTOVIPTransferredTimeTo (user, target, duration) ->
      (user |> steamIdDeserialized, target |> steamIdDeserialized, duration)
      |> VIPTransferredTimeTo
      |> Ok

  | DTOVIPWasRemovedDueToRuleBreaking id ->
      id
      |> steamIdDeserialized
      |> VIPWasRemovedDueToRuleBreaking
      |> Ok

  | DTOVIPWasPaused (id, date) ->
      (id |> steamIdDeserialized, date)
      |> VIPWasPaused
      |> Ok

  | DTOVIPWasResumed (id, date) ->
      (id |> steamIdDeserialized, date)
      |> VIPWasResumed
      |> Ok

  | DTOVIPHasExpired (id) ->
      VIPHasExpired (steamIdDeserialized id)
      |> Ok

let eventDezerialized (event : string) : Result<Event, string> =
  try
    event
    |> JsonConvert.DeserializeObject<EventDTO>
    |> eventFromDTO

  with e ->
    Error e.Message