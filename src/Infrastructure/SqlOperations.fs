module Infrastructure.SqlOperations

open Infrastructure.Configuration
open MySqlConnector

let private asSqlConnectionString (config : DatabaseConnectionInformation) : string =
  sprintf "Server=%s;Port=%i;User ID=%s;Password=%s;Database=%s;SslMode=None;AllowPublicKeyRetrieval=True"
    config.host
    config.port
    config.user
    config.password
    config.database

let openConnection (config : DatabaseConnectionInformation) = async {
  let connection = new MySqlConnection(config |> asSqlConnectionString)
  do! connection.OpenAsync() |> Async.AwaitTask
  return connection
}

let executeSqlWithArgs (cmd : string) (args : MySqlParameterCollection -> unit) (connection : MySqlConnection) = async {
  use sqlCmd = connection.CreateCommand()
  sqlCmd.CommandText <- cmd
  args sqlCmd.Parameters |> ignore

  do! sqlCmd.ExecuteNonQueryAsync() |> Async.AwaitTask |> Async.Ignore
}

let executeSql (cmd : string) (connection : MySqlConnection) = async {
  use sqlCmd = connection.CreateCommand()
  sqlCmd.CommandText <- cmd

  do! sqlCmd.ExecuteNonQueryAsync() |> Async.AwaitTask |> Async.Ignore
}

let rec private readData mapper data (reader : System.Data.Common.DbDataReader) = async {
  let! hasRow = reader.ReadAsync() |> Async.AwaitTask
  if hasRow then
    return! readData
      mapper
      ((reader |> mapper) :: data)
      reader
  else
    return data
}

let queryData (query : string) mapper (connection : MySqlConnection) = async {
  use sqlCmd = connection.CreateCommand()
  sqlCmd.CommandText <- query
  use! reader = sqlCmd.ExecuteReaderAsync() |> Async.AwaitTask
  return! readData mapper [] reader
}

let queryDataWithArgs (query : string) (args : MySqlParameterCollection -> unit) mapper (connection : MySqlConnection) = async {
  use sqlCmd = connection.CreateCommand()
  sqlCmd.CommandText <- query
  args sqlCmd.Parameters |> ignore
  use! reader = sqlCmd.ExecuteReaderAsync() |> Async.AwaitTask
  return! readData mapper [] reader
}