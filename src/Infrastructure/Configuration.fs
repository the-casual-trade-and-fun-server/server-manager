module Infrastructure.Configuration

type DatabaseConnectionInformation =
  {
    host : string
    port : int
    user : string
    database : string
    password : string
  }

type SourceModAdminGroups =
  {
    Owner : uint32
    Admin : uint32
    Moderator : uint32
    Mapper : uint32
    VIP : uint32
  }