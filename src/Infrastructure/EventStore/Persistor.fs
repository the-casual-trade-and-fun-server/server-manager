module Infrastructure.EventStore.Persistor

open Infrastructure.Configuration
open Infrastructure.SqlOperations
open Infrastructure.Serialization.Events
open Domain.Members.Events

let store (config : DatabaseConnectionInformation) (events : Events) : string option =
  try
    let recordDate =
      System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")
    let sql =
      events
      |> List.map eventSerialized
      |> List.mapi (fun index event -> sprintf "('%s', %i, '%s')" recordDate index event)
      |> fun events -> System.String.Join (',', events)
      |> sprintf "INSERT INTO EventStore (recordDate, recordOrder, event) VALUES %s"

    async {
      use! connection = openConnection config
      do! executeSql sql connection
    }
    |> Async.RunSynchronously
    None

  with ex ->
    sprintf "Failed to store events: %s" ex.Message |> Some

let private eventsFromEventStore config =
  async {
    use! connection = openConnection config
    return! queryData
      "SELECT event FROM EventStore ORDER BY recordDate DESC, recordOrder"
      (fun r -> r.GetString 0)
      connection
  }

let private collectEvent (result : Result<Events, string list>) (event : Result<Event, string>) =
  match event with
  | Ok event ->
      result
      |> Result.map (fun events -> events @ [event])

  | Error err ->
      match result with
      | Ok _ ->
          Error [err]

      | Error errors ->
          Error (errors @ [err])

let private collectEvents (events : Result<Event, string> list) : Result<Events, string list> =
  events
  |> List.fold
      collectEvent
      (Ok [])

let getAllEvents config : Result<Events, string> =
  try
    eventsFromEventStore config
    |> Async.RunSynchronously
    |> List.map eventDezerialized
    |> collectEvents
    |> Result.mapError (fun errors -> System.String.Join(",", errors))

  with ex ->
    Error ex.Message