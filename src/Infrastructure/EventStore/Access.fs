module Infrastructure.EventStore.Access

open Persistor
open Domain.Members.Events

type Msg =
  private
  | GetEvents of AsyncReplyChannel<Events>
  | Effect of AsyncReplyChannel<string option> * (Events -> Events)
  | TryEffect of AsyncReplyChannel<string option> * (Events -> Result<Events, string>)

let initEventStore config subscriptions history =
  MailboxProcessor.Start(fun inbox ->
    let rec loop history = async {
      let! msg = inbox.Receive()
      match msg with
      | GetEvents channel ->
          channel.Reply history
          return! loop history

      | Effect (channel, handler) ->
          match handler history with
          | [] ->
              channel.Reply None
              return! loop history

          | events ->
              match events |> store config with
              | Some err ->
                  Some err |> channel.Reply
                  return! loop history

              | None ->
                  try
                    subscriptions events
                  with ex ->
                    printfn "[ERROR] @Subscriptions: %s\n%s" ex.Message ex.StackTrace
                  channel.Reply None
                  return! loop (history @ events)

      | TryEffect (channel, handler) ->
          match handler history with
          | Ok [] ->
              channel.Reply None
              return! loop history

          | Ok events ->
              match events |> store config with
              | Some _ as error ->
                  error |> channel.Reply
                  return! loop history

              | None ->
                  try
                    subscriptions events
                  with ex ->
                    printfn "[ERROR] @Subscriptions: %s\n%s" ex.Message ex.StackTrace
                  channel.Reply None
                  return! loop (history @ events)

          | Error err ->
              err |> Some |> channel.Reply
              return! loop history
    }

    loop history
  )

let startEventStore config : Result<MailboxProcessor<Msg> * IEvent<Events>, string> =
  let observer = new Event<Events>()

  getAllEvents config
  |> Result.map (initEventStore config observer.Trigger)
  |> Result.map (fun store -> (store, observer.Publish))

let getEvents (inbox : MailboxProcessor<Msg>) : Events =
  inbox.PostAndReply GetEvents

let effect (handler : Events -> Events) (inbox : MailboxProcessor<Msg>) : string option =
  inbox.PostAndReply (fun channel -> (channel, handler) |> Effect)

let tryEffect (handler : Events -> Result<Events, string>) (inbox : MailboxProcessor<Msg>) : string option =
  inbox.PostAndReply (fun channel -> (channel, handler) |> TryEffect)