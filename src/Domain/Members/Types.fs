module Domain.Members.Types

type SteamId = SteamId of string

type Rank =
  | Owner
  | Admin
  | Moderator
  | Mapper
  | PermanentVIP

type VIPDuration = uint32
type VIPDurationInDays = uint16

type Date = System.DateTime

type VIPState =
  {
    startDate : Date
    duration : VIPDuration
  }

type VIP =
  | No
  | Member
  | Is of VIPState
  | Expired
  | Paused of VIPState*Date*VIPDuration
  | Blocked

let steamIdString (SteamId id) = id

let asMinutes (days : VIPDurationInDays) : VIPDuration =
  (days |> uint32) * 1440u

let isNotUTC (date : Date) =
  date.Kind <> System.DateTimeKind.Utc

let expirationDate state =
  state.duration
  |> float
  |> state.startDate.AddMinutes

let isExpired date state =
  state
  |> expirationDate
  |> (fun a -> a <= date)

let isVIPSince date state =
  state.startDate <= date