module Domain.Members.AllVIPsProjection

open Domain.Members.Types
open Domain.Members.Events

type VIPInfo =
  {
    id : SteamId
    date : Date
    duration : uint32
    pausedSince : Date option
    durationFromPauses : uint32
    countOfPauses : int
  }

type private Pause =
  | NotPaused
  | Paused of Date*(uint32 list)
  | Resumed of (uint32 list)

type private VIPFoldState =
  {
    id : SteamId
    date : Date
    duration : uint32
    pauses : Pause
  }

let private updateVIP key updater vips =
  vips
  |> Map.tryFind key
  |> Option.map updater
  |> Option.map (fun vip -> Map.add key vip vips)
  |> Option.defaultValue vips

let private setPause date vip =
  match vip.pauses with
  | NotPaused ->
      { vip with pauses = Paused (date, []) }

  | Resumed pauses ->
      { vip with pauses = Paused (date, pauses) }

  | Paused _ ->
      vip

let private resume date vip =
  match vip.pauses with
  | Paused (pauseDate, pauses) ->
      let duration =
        (date - pauseDate).TotalMinutes
        |> uint32
      { vip with pauses = Resumed (duration::pauses) }

  | _ ->
      vip

let private apply state event : Map<SteamId, VIPFoldState> =
  match event with
  | UserRecievedVIP (steamId, date, duration) ->
      state
      |> Map.add
        steamId
        {
          id = steamId
          date = date
          duration = duration
          pauses = NotPaused
        }

  | VIPHasExpired id
  | VIPWasRemovedDueToRuleBreaking id ->
      state |> Map.remove id

  | VIPDurationWasCorrected (id, duration) ->
      updateVIP
        id
        (fun vip -> { vip with duration = duration })
        state

  | VIPDurationWasExtended (id, duration) ->
      updateVIP
        id
        (fun vip -> { vip with duration = vip.duration + duration })
        state

  | VIPTransferredTimeTo (id, _, duration) ->
      updateVIP
        id
        (fun vip -> { vip with duration = vip.duration - duration })
        state

  | VIPWasPaused (id, date) ->
      updateVIP
        id
        (setPause date)
        state

  | VIPWasResumed (id, date) ->
      updateVIP
        id
        (resume date)
        state

  | _ -> state

let private timeHasStartedSince date vip =
  date >= vip.date

let private totalDurationFromPauses pauses =
  pauses |> List.sum

let private isAfter date dateToTest =
  dateToTest > date

let addMinutesTo (date : Date) (minutes : uint32) : Date =
  minutes |> float |> date.AddMinutes

let private isNotExpiredOn date vip =
  match vip.pauses with
  | NotPaused ->
      vip.duration
      |> addMinutesTo vip.date
      |> isAfter date

  | Resumed pauses ->
      vip.duration
      |> (+) (totalDurationFromPauses pauses)
      |> addMinutesTo vip.date
      |> isAfter date

  | Paused _ ->
      true

let private asVIPInfo dateToView (vip : VIPFoldState) : VIPInfo =
  let info =
    {
      id = vip.id
      date = vip.date
      duration = vip.duration
      pausedSince = None
      durationFromPauses = 0u
      countOfPauses = 0
    }

  match vip.pauses with
  | NotPaused ->
      info

  | Resumed pauses ->
      { info with
          durationFromPauses = pauses |> List.sum
          countOfPauses = pauses |> List.length
      }

  | Paused (pauseDate, previousPauses) ->
      { info with
          pausedSince = if dateToView >= pauseDate then Some pauseDate else None
          durationFromPauses = previousPauses |> List.sum
          countOfPauses = previousPauses |> List.length
      }

let allVIPs dateToView history : VIPInfo array =
  history
  |> List.fold apply Map.empty
  |> Map.toArray
  |> Array.map (fun (_,vip) -> vip)
  |> Array.filter (timeHasStartedSince dateToView)
  |> Array.filter (isNotExpiredOn dateToView)
  |> Array.map (asVIPInfo dateToView)