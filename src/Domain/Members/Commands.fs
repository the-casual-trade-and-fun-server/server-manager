module Domain.Members.Commands

open Types
open Events

type AddMember =
  {
    user : SteamId
    rank : Rank
  }

type AddMemberError =
  | AlreadyMemberWithAnotherRank
  | IsAlreadyAdded

type SetRankOfMemberTo =
  {
    user : SteamId
    newRank : Rank
  }

type SetRankOfMemberError =
  | NotAMember
  | RankAlreadySetted

type RemoveMember =
  {
    user : SteamId
  }

type RemoveMemberError =
  | NotAMember

// -- VIPs --

type GiveUserVIP =
  {
    user : SteamId
    date : Date
    duration : VIPDuration
  }

type GiveUserVIPError =
  | UserIsAlreadyVIP
  | UserIsCurrentlyPaused
  | UserIsARuleBreaker
  | UserIsAMember
  | TimezoneIsNotUTC
  | DurationOfZero

type CorrectDurationOfVIP =
  {
    user : SteamId
    newDuration : VIPDuration
  }

type CorrectDurationOfVIPError =
  | UserIsNotAVIP
  | NoDifferenceInDuration
  | DurationOfZero

type ExtendDurationOfVIP =
  {
    user : SteamId
    dateOfExtension : Date
    additionalDuration : VIPDuration
  }

type ExtendDurationOfVIPError =
  | UserIsNotAVIP
  | VIPIsExpired
  | TimezoneIsNotUTC
  | DurationOfZero

type ExtendTimeForAllVIPs =
  {
    date : Date
    duration : VIPDurationInDays
  }

type ExtendTimeForAllVIPsError =
  | DurationOfZero

type TransferTime =
  {
    user : SteamId
    target : SteamId
    date : Date
    duration : VIPDurationInDays
  }

type TransferTimeError =
  | UserIsNotAVIP
  | VIPIsExpired
  | DurationOfZero
  | TimezoneIsNotUTC
  | DurationIsNotAvailable

type RemoveVIPDueToRuleBreaking =
  {
    user : SteamId
  }

type RemoveVIPDueToRuleBreakingError =
  | UserIsNotAVIP

type PauseVIP =
  {
    user : SteamId
    pauseDate : Date
  }

type PauseVIPError =
  | UserIsNotAVIP
  | PauseBeforeStartDate
  | PauseAfterExpiration
  | AlreadyPaused
  | TimezoneIsNotUTC

type ResumeVIP =
  {
    user : SteamId
    resumeDate : Date
  }

type ResumeVIPError =
  | IsNotPaused
  | TimezoneIsNotUTC

type RemoveExpiredVips =
  { date : Date }

type CommandResult<'error> =
  Result<Events, 'error>