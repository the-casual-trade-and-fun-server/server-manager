module Domain.Members.Behavior

open Types
open Commands
open Events
open Projections

let handleAddMember (command : AddMember) (history : Events) : CommandResult<AddMemberError> =
  match history |> projectMember command.user with
  | Some rank ->
      if rank = command.rank then
        Error IsAlreadyAdded
      else
        Error AlreadyMemberWithAnotherRank

  | None ->
      Ok [ MemberWasAdded (command.user, command.rank) ]

let handleSetRankOfMember (command : SetRankOfMemberTo) (history : Events) : CommandResult<SetRankOfMemberError> =
  match history |> projectMember command.user with
  | Some rank ->
      if command.newRank <> rank then
        Ok [ MemberGotNewRank (command.user, command.newRank) ]
      else
        Error RankAlreadySetted

  | None ->
      Error SetRankOfMemberError.NotAMember

let handleRemoveMember (command : RemoveMember) (history : Events) : CommandResult<RemoveMemberError> =
  match history |> projectMember command.user with
  | Some _ ->
      Ok [ MemberWasRemoved command.user ]

  | None ->
      Error RemoveMemberError.NotAMember

// -- VIPS --

let handleGiveUserVIP (command : GiveUserVIP) (history : Events) : CommandResult<GiveUserVIPError> =
  if command.date |> isNotUTC then
    Error GiveUserVIPError.TimezoneIsNotUTC

  elif command.duration = 0u then
    Error GiveUserVIPError.DurationOfZero

  else
    match projectVIP command.user history with
    | No
    | Expired ->
        Ok [ Event.UserRecievedVIP (command.user, command.date, command.duration) ]

    | Is _ ->
        Error GiveUserVIPError.UserIsAlreadyVIP

    | Paused _ ->
        Error GiveUserVIPError.UserIsCurrentlyPaused

    | VIP.Member ->
        Error GiveUserVIPError.UserIsAMember

    | Blocked ->
        Error GiveUserVIPError.UserIsARuleBreaker

let handleCorrectDurationOfVIP (command : CorrectDurationOfVIP) (history : Events) : CommandResult<CorrectDurationOfVIPError> =
  if command.newDuration = 0u then
    Error CorrectDurationOfVIPError.DurationOfZero

  else
    history
    |> durationOfVIP command.user
    |> Option.map (fun duration ->
      if duration <> command.newDuration then
        Ok [ VIPDurationWasCorrected (command.user, command.newDuration) ]
      else
        Error CorrectDurationOfVIPError.NoDifferenceInDuration)
    |> Option.defaultValue (Error CorrectDurationOfVIPError.UserIsNotAVIP)

let private remainingDurationOf (date : Date) vip =
  match vip with
  | Paused (_, _, duration) ->
      Some duration

  | Is state ->
      let duration = ((state |> expirationDate) - date).TotalMinutes

      if duration < 0. then
        Some 0u
      else
        duration |> uint32 |> Some

  | Expired ->
      Some 0u

  | _ ->
      None

let handleExtendDurationOfVIP (command : ExtendDurationOfVIP) (history : Events) : CommandResult<ExtendDurationOfVIPError> =
  if command.dateOfExtension |> isNotUTC then
    Error ExtendDurationOfVIPError.TimezoneIsNotUTC

  elif command.additionalDuration = 0u then
    Error ExtendDurationOfVIPError.DurationOfZero

  else
    match projectVIP command.user history with
    | Is _
    | Paused _ ->
        Ok [ VIPDurationWasExtended (command.user, command.additionalDuration) ]

    | Expired ->
        Error ExtendDurationOfVIPError.VIPIsExpired

    | No
    | VIP.Member
    | Blocked ->
        Error ExtendDurationOfVIPError.UserIsNotAVIP

let private extendTimeBy duration vip : Event =
  Event.VIPDurationWasExtended (vip, duration)

let private timeCanBeExtended date (vip : VIP) : bool =
  match vip with
  | Is vip
  | Paused (vip, _, _) ->
      vip.startDate <= date

  | _ ->
      false

let handleExtendTimeForAllVIPs (command : ExtendTimeForAllVIPs) (history : Events) : CommandResult<ExtendTimeForAllVIPsError> =
  if command.duration = (uint16 0) then
    Error ExtendTimeForAllVIPsError.DurationOfZero

  else
    history
    |> projectVIPs
    |> Map.toSeq
    |> Seq.filter (fun (_, vip) -> vip |> timeCanBeExtended command.date)
    |> Seq.map (fun (vip, _) -> vip |> extendTimeBy (asMinutes command.duration))
    |> Seq.toList
    |> Ok

let private hasNotStarted date vip =
  match vip with
  | Paused (state, _, _)
  | Is state -> state.startDate > date
  | _ -> false

let private isVIP date vip =
  vip
  |> remainingDurationOf date
  |> Option.exists (fun d -> d > 0u)

let handleTransferTime (command : TransferTime) (history : Events) : CommandResult<TransferTimeError> =
  if command.date |> isNotUTC then
    Error TransferTimeError.TimezoneIsNotUTC

  elif command.duration = 0us then
    Error TransferTimeError.DurationOfZero

  else
    let vips = projectVIPs history
    let vip =
      vips
      |> Map.tryFind command.user
      |> Option.defaultValue No

    match remainingDurationOf command.date vip with
    | Some duration ->
        if duration = 0u then
          Error TransferTimeError.VIPIsExpired

        elif vip |> hasNotStarted command.date then
          Error TransferTimeError.UserIsNotAVIP

        elif duration < (asMinutes command.duration) then
          Error TransferTimeError.DurationIsNotAvailable

        else
          let target =
            vips
            |> Map.tryFind command.target
            |> Option.defaultValue No
          let targetEvent =
            if target |> isVIP command.date then
              VIPDurationWasExtended (command.target, asMinutes command.duration)
            else
              UserRecievedVIP (command.target, command.date, asMinutes command.duration)

          Ok
            [
              VIPTransferredTimeTo (command.user, command.target, asMinutes command.duration)
              targetEvent
            ]

    | None ->
        Error TransferTimeError.UserIsNotAVIP

let handleRemoveVIPDueToRuleBreaking (command : RemoveVIPDueToRuleBreaking) (history : Events) : CommandResult<RemoveVIPDueToRuleBreakingError> =
  if history |> wasVIPBefore command.user then
    Ok [ VIPWasRemovedDueToRuleBreaking command.user ]

  else
    Error RemoveVIPDueToRuleBreakingError.UserIsNotAVIP

let handlePauseVIP (command : PauseVIP) (history : Events) : CommandResult<PauseVIPError> =
  if command.pauseDate |> isNotUTC then
    Error PauseVIPError.TimezoneIsNotUTC

  else
    match projectVIP command.user history with
    | Is state ->
        if command.pauseDate < state.startDate then
          Error PauseVIPError.PauseBeforeStartDate

        else
          Ok [ VIPWasPaused (command.user, command.pauseDate) ]

    | Expired ->
        Error PauseVIPError.PauseAfterExpiration

    | Paused _ ->
        Error PauseVIPError.AlreadyPaused

    | _ ->
        Error PauseVIPError.UserIsNotAVIP

let handleResumeVIP (command : ResumeVIP) (history : Events) : CommandResult<ResumeVIPError> =
  if command.resumeDate |> isNotUTC then
    Error ResumeVIPError.TimezoneIsNotUTC

  else
    history
    |> pausedSince command.user
    |> Option.filter ((>) command.resumeDate)
    |> Option.map (fun _ -> Ok [ VIPWasResumed (command.user, command.resumeDate) ])
    |> Option.defaultValue (Error ResumeVIPError.IsNotPaused)

type VipInfo =
  { date : Date
    duration : VIPDuration
    pausedSince : Date option
  }

let private updateTmpVip (vip : SteamId) (updater : VipInfo -> VipInfo) (vips : Map<SteamId, VipInfo>) =
  vips.TryFind vip
  |> Option.map updater
  |> Option.map (fun v -> vips |> Map.add vip v)
  |> Option.defaultValue vips

let private applyExpiredVip (state : Map<SteamId, VipInfo>) event =
  match event with
  | UserRecievedVIP (user, date, duration) ->
      state |> Map.add user ({ date = date; duration = duration; pausedSince = None })

  | VIPDurationWasCorrected (user, duration) ->
      state |> updateTmpVip user (fun v -> { v with duration = duration })

  | VIPDurationWasExtended (user, duration) ->
      state |> updateTmpVip user (fun v -> { v with duration = v.duration + duration })

  | VIPTransferredTimeTo (user, _, duration) ->
      state |> updateTmpVip user (fun v -> { v with duration = v.duration - duration })

  | VIPWasPaused (user, date) ->
      state |> updateTmpVip user (fun v -> { v with pausedSince = Some date })

  | VIPWasResumed (user, date) ->
      state |> updateTmpVip user (fun v -> { v with pausedSince = None ; duration = v.pausedSince |> Option.map (fun pauseDate -> (pauseDate |> durationTo date) + v.duration) |> Option.defaultValue v.duration })

  | VIPHasExpired user
  | VIPWasRemovedDueToRuleBreaking user ->
      state |> Map.remove user

  | _ ->
      state

let private projectExpiredVips date history =
  history
  |> List.fold applyExpiredVip Map.empty
  |> Map.toSeq
  |> Seq.filter (fun (_, vip) -> vip.pausedSince |> Option.isNone)
  |> Seq.filter (fun (_, vip) -> vip.duration |> float |> vip.date.AddMinutes <= date)
  |> Seq.map (fun (user, _) -> user)

let handleRemoveExpiredVips (command : RemoveExpiredVips) (history : Events) : Events =
  history
  |> projectExpiredVips command.date
  |> Seq.map VIPHasExpired
  |> Seq.toList