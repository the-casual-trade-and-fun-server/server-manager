module Domain.Members.RoleOfProjection

open Types
open Projections
open AllVIPsProjection
open Events

type Role =
  | ServerOwner
  | TeamMember
  | VIP
  | PausedVIP
  | Player

let private vipAsRole (vip : VIPInfo) =
  if vip.pausedSince.IsSome then
    PausedVIP
  else
    VIP

let private vipOfUser dateOfView (steamId : SteamId) (history : Events) : Role option =
  history
  |> allVIPs dateOfView
  |> Array.tryFind (fun vip -> vip.id = steamId)
  |> Option.map vipAsRole

let private rankAsRole (rank : Rank) =
  match rank with
  | Owner -> ServerOwner
  | Admin | Moderator -> TeamMember
  | PermanentVIP -> VIP
  | Mapper -> Player

let roleOf (steamId : SteamId) (dateOfView : Date) (history : Events) : Role =
  history
  |> projectMembers
  |> Map.tryFind steamId
  |> Option.map rankAsRole
  |> Option.orElse (vipOfUser dateOfView steamId history)
  |> Option.defaultValue Player