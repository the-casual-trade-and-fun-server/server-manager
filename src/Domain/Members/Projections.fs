module Domain.Members.Projections

open Events
open Types

let private applyMember members event =
  match event with
  | MemberWasAdded (user, rank)
  | MemberGotNewRank (user, rank) ->
      members
      |> Map.add user rank

  | MemberWasRemoved user ->
      members
      |> Map.remove user

  | _ ->
      members

let projectMembers history =
  List.fold
    applyMember
    Map.empty
    history

let projectMember user history =
  projectMembers history
  |> Map.tryFind user

let private updateVIP user mapper vips =
  vips |> Map.tryFind user
  |> Option.bind (function | Is state -> Some state | Paused (state, _, _) -> Some state | _ -> None)
  |> Option.map mapper
  |> Option.map (fun vip -> vips |> Map.add user vip)
  |> Option.defaultValue vips

let private substractDuration value (duration : VIPDuration) =
  duration - value

let durationTo (toDate : Date) (from : Date) : VIPDuration =
  toDate - from
  |> fun span -> span.TotalMinutes
  |> uint32

let private applyVIPs (vips : Map<SteamId, VIP>) event : Map<SteamId, VIP> =
  match event with
  | MemberWasAdded (user, _) ->
      vips |> Map.add user VIP.Member

  | UserRecievedVIP (user, date, duration) ->
      vips |> Map.add user (Is { startDate = date ; duration = duration })

  | MemberWasRemoved user ->
      vips |> Map.remove user

  | VIPDurationWasCorrected (user, newDuration) ->
      vips |> updateVIP user (fun state -> Is { state with duration = newDuration })

  | VIPDurationWasExtended (user, extraDuration) ->
      vips |> updateVIP user (fun state -> Is { state with duration = state.duration + extraDuration })

  | VIPTransferredTimeTo (user, _, duration) ->
      vips
      |> Map.tryFind user
      |> Option.bind
          (function
            | Is state -> Some (Is { state with duration = state.duration - duration })
            | Paused (state, date, pauseduration) -> Some (Paused (state, date, pauseduration - duration))
            | _ -> None)
      |> Option.map (fun v -> vips |> Map.add user v)
      |> Option.defaultValue vips

  | VIPWasRemovedDueToRuleBreaking user ->
      vips |> Map.add user Blocked

  | VIPWasPaused (user, pauseDate) ->
      vips
      |> updateVIP
        user
        (fun state ->
          state.duration
          |> substractDuration (state.startDate |> durationTo pauseDate)
          |> fun duration -> Paused (state, pauseDate, duration))

  | VIPWasResumed (user, resumeDate) ->
      vips |> Map.tryFind user
      |> Option.bind (function | Paused (_, _, duration) -> Some duration | _ -> None)
      |> Option.map (fun duration -> Is { startDate = resumeDate ; duration = duration })
      |> Option.map (fun vip -> vips |> Map.add user vip)
      |> Option.defaultValue vips

  | VIPHasExpired user ->
      vips |> Map.add user Expired

  | _ ->
      vips

let projectVIPs history =
  List.fold
    applyVIPs
    Map.empty
    history

let projectVIP user history =
  projectVIPs history
  |> Map.tryFind user
  |> Option.defaultValue No

let wasVIPBefore user history =
  history |> List.exists (function | UserRecievedVIP (id, _, _) -> id = user | _ -> false)

let private applyDurationOfVIP user duration event =
  match event with
  | UserRecievedVIP (id, _, initialDuration) when id = user ->
      Some initialDuration

  | VIPDurationWasCorrected (id, newDuration) when id = user ->
      Some newDuration

  | VIPDurationWasExtended (id, additionalDuration) when id = user ->
      duration |> Option.map ((+) additionalDuration)

  | VIPWasRemovedDueToRuleBreaking id when id = user ->
      None

  | _ -> duration

let durationOfVIP user history =
  history
  |> List.fold
    (applyDurationOfVIP user)
    None

let private applyPauseDate user pauseDate event =
  match event with
  | VIPWasPaused (id, date) when id = user ->
      Some date

  | VIPWasResumed (id, _) when id = user ->
      None

  | _ ->
      pauseDate

let pausedSince user history : Date option =
  history
  |> List.fold
    (applyPauseDate user)
    None