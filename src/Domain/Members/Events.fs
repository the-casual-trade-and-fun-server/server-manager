module Domain.Members.Events

open Types

type Event =
  | MemberWasAdded of SteamId * Rank
  | MemberGotNewRank of SteamId * Rank
  | MemberWasRemoved of SteamId
  | UserRecievedVIP of SteamId * Date * VIPDuration
  | VIPDurationWasCorrected of SteamId * VIPDuration
  | VIPDurationWasExtended of SteamId * VIPDuration
  | VIPTransferredTimeTo of user:SteamId * target:SteamId * VIPDuration
  | VIPWasRemovedDueToRuleBreaking of SteamId
  | VIPWasPaused of SteamId * Date
  | VIPWasResumed of SteamId * Date
  | VIPHasExpired of SteamId

type Events =
  Event list