# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [Unreleased]
## [0.18.0] - 2025-02-15
### Changed
- Update to .NET 9
- Updated dependencies
- Publish as self-contained single file

## [0.17.2] - 2023-07-24
### Fixed
- Update CoreRCON to be able to send A2S_INFO to Server again

## [0.17.1] - 2020-12-04
### Fixed
- Transfer time after transferring time during pause and resume will work now

## [0.17.0] - 2020-04-19
### Changed
- Updated dependencies

### Fixed
- Only reload admin cache and user settings after tables have been updated


## [0.16.0] - 2020-03-24
### Changed
- Switched from Suave to Giraffe + ASP.NET Core
- Updated dependencies

## [0.15.0] - 2020-01-19
### Changed
- Target framework is now `netcoreapp3.1`

## [0.14.2] - 2020-01-01
### Changed
- Retrieving the server status now uses a timeout of 10 seconds instead of 1

## [0.14.1] - 2020-01-01
### Added
- Server status now contains the server address

## [0.14.0] - 2019-12-17
### Added
- server status queries
- Notifications about paused and resumed VIPs.
- User settings of paused VIPs are stored and restored after resuming.

### Changed
- Expired VIPs are now event based
- Table `members` for SourceMod admins is now a readmodel.
    - Admin cache reload will happen after table content was modified
    - Faster performance since table is not truncated and build up after each event. Only diffs are gonna be applied.

### Fixed
- Admin cache and custom chat colors reloading on each event.
- Deadlock when gameserver is not available over RCON by using a timeout.
- Expired VIPs would be notified as "removed due to rule breaking"

## [0.13.1] - 2019-12-02
### Changed
- Try up to 5 times to send Discord notifications

### Fixed
- Expending or correcting duration of VIP during pause
- Removing user settings when remembering settings failed
- Close SQL connection after use

### Removed
- Unneeded dependencie (FsUnit & Expecto.FsCheck)

## [0.13.0] - 2019-11-29
### Added
- Reload custom chat colors on game server when needed
- Reload admins on gameserver when needed. See #9
- Unhandeld errors from event subscriptions are now logged.

## [0.12.0] - 2019-11-24
### Added
- Query for joinsound of user
- Query for defined joinsounds
- Query for custom chat colors of user
- Set custom chat colors
- Set joinsound
- Command to use no joinsound
- Remember joinsound and custom chat colors of expired VIPs and restore if user gets VIP again
- Remove user settings when VIP was removed due to rule breaking
- Notify via Discord when VIP was removed due to rule breaking

### Changed
- Remove user settings explicitly on VIP expiration
- Target framework is now `netcoreapp2.2`
- simplified dependencies
- Argument parsing errors are now typed

## [0.11.0] - 2019-10-15
### Added
- Command to extend time for all VIPs

### Removed
- UI to interact with the server. New UI is the casual-trade-and-fun-website

## [0.10.0] - 2019-09-25
### Changed
- Members are now serialized as plain objects
- Upadted paket
- Updated dependencies

## [0.9.1] - 2019-07-10
### Changed
- Update dependencies

### Fixed
- [AllVIPs] Return paused VIP again which would be expired on query date

## [0.9.0] - 2019-07-10
### Changed
- `transferTime` will now take an unix timestamp as date
- `extendDuration` will now take an unix timestamp as date
- `resumeVIP` will no take an unix timestamp as date
- `pauseVIP` will no take an unix timestamp as date

### Fixed
- Wrong error for duration of zero at `extendDuration`

## [0.8.2] - 2019-06-25
### Changed
- Datetime of vips will now be transfered as unix timestamp

## [0.8.1] - 2019-06-03
### Changed
- update dependencies

## [0.8.0] - 2019-06-01
### Added
- Query to get role of an user

### Changed
- `giveUserVIP` will now take an unix timestamp as date

## [0.7.0] - 2019-05-01
### Added
- Discord notification of added VIPs
- Discord notification of expired VIPs

### Changed
- Timer for running database projection changed to check of expired VIPs
- Config `hoursBetweenDatabaseProjection` renamed to `hoursBetweenCheckOfExpiredVIPs`
- Subscriptions from EventStore only contains new events as payload

## [0.6.1] - 2019-04-28
### Added
- Option to set ip binding and port

## [0.6.0] - 2019-04-19
### Added
- Transfer time from VIP to user/VIP. See #14
- Tests for covering parsing of all commands
- Config to set hours between database projection to remove expired VIP

### Changed
- Converter will create a backup table of `sm_admins` and `sm_admins_groups`
- Table `sm_admins` and `sm_admins_groups` is replaced with an SQL-View whichs get its data from the `members` table. See #26
- Breaking: Datetimes of HTTP Requests will only accept in format "dd.MM.yyyy HH:mm". For example "21.02.2019 13:55"
- Optimized handling of "correct VIP duration"
- Optimized handling of "extend VIP duration"
- Optimized handling of "remove VIP due to rule breaking"
- Optimized handling of "resume VIP"
- Resuming VIP will only fail when VIP is not paused

### Fixed
- Correcting VIP duration to the current duration of the VIP will now fail
- Correcting VIP duration of a removed VIP will now fail

## [0.5.0] - 2019-03-23
### Changed
- Resuming VIP will fail now when resuming before a pause has started. See #12
- Resuming VIP will fail now when user is not paused. See #32
- Giving user VIP will fail if user is VIP but paused. See #31
- Adding a member twice will fail now instead of doing nothing. See #21
- Adding VIP, correcting or adding duration of 0 is not allowed. See #25
- Correcting VIP duration to the same as the current one will fail. See #24
- Set rank of a member will fail if rank is already set. See #23
- Removing member will fail if user is not a member. See #22
- getAllVIPs will show paused VIPs with pause date. See #33
- All projects now targeting .NET Core 2.1

### Fixed
- getAllVIPs will show time again
- Remove VIP due to rule breaking works now for paused VIP. See #29
- Correct duration of VIP works now for paused VIP. See #29
- Extend duration of VIP works now for paused VIP. See #29

## [0.4.0] - 2019-03-17
### Added
- Projections of active VIPs & all VIPs are now covered with tests

### Changed
- Internal datetimes are now stored in UTC timezone. See !6
  - The SourceMod Admin convertert converts all datetime to UTC
  - Datetimes from API endpoints are parsed as localtime if no timezone information are given and are converted to UTC.
- Name of SourceMod admins are now something like "projected member 1" instead of "projectedAdmin 1"
- `Effect` and `TryEffect` fails when the events can not be stored. See #20
- Projection of all VIPs gives now information about duration from pauses. See #27
- Updated dependencies

### Deprecated
- Datetimes send to API endpoints will be parsed as UTC in the future

### Fixed
- Provide default value of `flags` and `inherit_order` at SourceMod admins projection. See #16
- Projection timer for VIP expiration will not fail if the time is over `Int32.MaxValue`. See #17
- VIPs which are added at a later time are not listed in SourceMod Admins anymore. See #19

## [0.3.0] - 2019-02-14
### Added
- Converter program, that converts all existing SourceMod admins and VIPs to the new event store. See #15
- Simple UI. See #3

## [0.2.0] - 2019-02-11
### Added
- POST requests for commands are secured by an access key

## [0.1.0] - 2018-11-27
- Initial release
