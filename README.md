## Requirements
In order to use the server manager **ASP.NET Core Runtime 3.1** has to be installed on the host system.

## Configuration
The configuration is stored in the `config.json` file next to the server. It has the following structure:

```json
{
  "ip" : "127.0.0.1",
  "port" : 8080,
  "accessKey" : "secretAPIKey",
  "hoursBetweenCheckOfExpiredVIPs": 3.5,
  "database" : {
    "host" : "127.0.0.1",
    "port" : 3306,
    "user" : "mysql",
    "database" : "ServerManager",
    "password" : "mySuperSecretPassword"
  },
  "adminGroups" : {
    "Owner" : 1,
    "Admin" : 2,
    "Moderator" : 3,
    "Mapper" : 4,
    "VIP" : 5
  },
  "discord" : {
      "webhook" : "webhook endpoint",
      "mention" : "@here"
  },
  "gameserver" : {
      "ip" : "127.0.0.1",
      "port" : 27015,
      "rconPassword" : "rconPassWD"
  }
}
```

### Fields
#### ip
The IP address the server is running on. For external access use the IP address of the host instead of localhost.

#### port
The port the server is listening on.

#### accessKey
The API is secured from unauthorized usage by checking POST requests for an access key. Here is the key defined.

#### hoursBetweenCheckOfExpiredVIPs
The amount of hours between a check of expired VIPs with the effects:
- projection of the tables `joinsounds` and `cccm_users` to remove expired VIPs
- sending a notification to discord via webhook

#### database
An object which contains the connection info of the database server.

> Warning! Only MySQL compatible SQL server are supported

#### adminGroups
An object which contains the ids of the sourcemod admin groups equivalent to the member ranks.

#### discord
##### webhook
The url to the discord webhook endpoint.

##### mention
Discord mention each message will contains at the start.

#### gameserver
##### ip
The IP address where the gameserver is running.

##### port
The port of the gameserver.

##### rconPassword
The password for the rcon.